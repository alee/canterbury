/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/process-info.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/apparmor.h>
#include <sys/types.h>

#include "canterbury/bundle-internal.h"
#include "canterbury/enumtypes.h"
#include "canterbury/errors.h"
#include "canterbury/messages-internal.h"

#include "canterbury/platform/paths.h"

#define DBUS_SERVICE_DBUS "org.freedesktop.DBus"
#define DBUS_PATH_DBUS "/org/freedesktop/DBus"
#define DBUS_INTERFACE_DBUS "org.freedesktop.DBus"

struct _CbyProcessInfo {
    GObject parent;
};

/* If it is not true that uid_t == guint == guint32 on some platform,
 * then we need to be more careful with the :user-id property. */
G_STATIC_ASSERT (sizeof (uid_t) == sizeof (guint));
G_STATIC_ASSERT (sizeof (uid_t) == sizeof (guint32));

typedef struct _CbyProcessInfoPrivate {
    GError *init_error;

    gchar *apparmor_label;
    gchar *bundle_id;
    /* lazily initialized */
    gchar *persistence_path;
    /* lazily initialized */
    gchar *downloads_path;

    guint user_id;
    CbyProcessType process_type;
} CbyProcessInfoPrivate;

typedef enum {
    PROP_APPARMOR_LABEL = 1,
    PROP_BUNDLE_ID,
    PROP_PROCESS_TYPE,
    PROP_USER_ID,
    N_PROPERTIES
} Property;

static GParamSpec *property_specs[N_PROPERTIES] = { NULL };

/**
 * SECTION:process-info.h
 * @title: CbyProcessInfo
 * @short_description: An object holding information about a process
 *
 * The #CbyProcessInfo object holds information about a process, such as
 * its security context, at a particular point in time. It can either
 * represent the process in which this API is used, or another process
 * visible via inter-process communication.
 *
 * When using #CbyProcessInfo objects to make authorization decisions,
 * construct a new #CbyProcessInfo object for each request that needs to be
 * authorized. For example, for D-Bus method calls, use
 * cby_process_info_new_for_dbus_invocation_async() once for each method call.
 * This ensures that the credentials used to authorize each method call
 * are the same as those of the process responsible for the D-Bus connection.
 *
 * The properties of a #CbyProcessInfo object do not change after it has
 * been created, even if the process for which it was created somehow changes
 * its identity via privileged mechanisms such as `aa_change_profile()` or
 * `setuid()`. As a result, the #GObject::notify signal is never emitted.
 *
 * There is deliberately no constructor that takes a process ID as argument,
 * because that would be susceptible to time-of-check/time-of-use
 * vulnerabilities: in some cases it is possible for a process to
 * send a request, then `exec()` a more privileged process before the
 * request is processed.
 *
 * Similarly, there is deliberately no property for the process ID, because
 * using the process ID to look up process attributes in `/proc` is
 * susceptible to time-of-check/time-of-use vulnerabilities.
 *
 * Since: 0.4.0
 */

static void initable_iface_init (GInitableIface *initable_iface);

G_DEFINE_TYPE_WITH_CODE (CbyProcessInfo,
                         cby_process_info,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                initable_iface_init);
                         G_ADD_PRIVATE (CbyProcessInfo))

static void
cby_process_info_init (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);

  priv->process_type = CBY_PROCESS_TYPE_UNKNOWN;
  priv->user_id = CBY_PROCESS_INFO_NO_USER_ID;
}

static gboolean
cby_process_info_initable_init (GInitable *initable,
                                GCancellable *cancellable,
                                GError **error)
{
  CbyProcessInfoPrivate *priv =
      cby_process_info_get_instance_private (CBY_PROCESS_INFO (initable));

  if (priv->init_error == NULL)
    return TRUE;

  g_set_error_literal (error, priv->init_error->domain, priv->init_error->code,
                       priv->init_error->message);
  return FALSE;
}

static void
cby_process_info_constructed (GObject *object)
{
  CbyProcessInfoPrivate *priv =
      cby_process_info_get_instance_private (CBY_PROCESS_INFO (object));
  CbyProcessType type = CBY_PROCESS_TYPE_UNKNOWN;
  gchar *bundle_id = NULL;

  if (priv->apparmor_label == NULL)
    {
      type = CBY_PROCESS_TYPE_UNKNOWN;
    }
  else if (strcmp (priv->apparmor_label, "unconfined") == 0)
    {
      /* Some platform processes, such as systemd, are said to be
       * unconfined: AppArmor does not apply any particular
       * restrictions to them. */
      type = CBY_PROCESS_TYPE_PLATFORM;
    }
  else if (g_path_is_absolute (priv->apparmor_label))
    {
      type = _cby_parse_installation_path (priv->apparmor_label, &bundle_id);
    }

  if (type != CBY_PROCESS_TYPE_UNKNOWN)
    {
      if (priv->process_type != type &&
          priv->process_type != CBY_PROCESS_TYPE_UNKNOWN)
        {
          g_set_error (&priv->init_error, CBY_ERROR,
                       CBY_ERROR_INVALID_ARGUMENT,
                       "AppArmor label \"%s\" is inconsistent with "
                       "specified process type %d",
                       priv->apparmor_label, priv->process_type);
          goto clear;
        }
      else
        {
          priv->process_type = type;
        }
    }

  if (bundle_id != NULL)
    {
      if (priv->bundle_id != NULL &&
          g_strcmp0 (priv->bundle_id, bundle_id) != 0)
        {
          g_set_error (&priv->init_error, CBY_ERROR,
                       CBY_ERROR_INVALID_ARGUMENT,
                       "AppArmor label \"%s\" is inconsistent with "
                       "specified bundle ID \"%s\"",
                       priv->apparmor_label, priv->bundle_id);
          goto clear;
        }
      else
        {
          g_free (priv->bundle_id);
          priv->bundle_id = bundle_id;
        }
    }

  if (priv->bundle_id != NULL &&
      priv->process_type != CBY_PROCESS_TYPE_STORE_BUNDLE &&
      priv->process_type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
    {
      g_set_error (&priv->init_error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                   "Bundle ID \"%s\" is inconsistent with specified process "
                   "type \"%d\"",
                   priv->bundle_id, priv->process_type);
      goto clear;
    }

  return;

clear:
  g_assert (priv->init_error != NULL);
  /* If the information we were given is not internally consistent,
   * forget it */
  g_clear_pointer (&priv->apparmor_label, g_free);
  g_clear_pointer (&priv->bundle_id, g_free);
  priv->process_type = CBY_PROCESS_TYPE_UNKNOWN;
  priv->user_id = CBY_PROCESS_INFO_NO_USER_ID;
}

static void
cby_process_info_get_property (GObject *object,
    guint prop_id,
    GValue *value,
    GParamSpec *pspec)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (
      CBY_PROCESS_INFO (object));

  switch ((Property) prop_id)
    {
      case PROP_APPARMOR_LABEL:
        g_value_set_string (value, priv->apparmor_label);
        break;

      case PROP_BUNDLE_ID:
        g_value_set_string (value, priv->bundle_id);
        break;

      case PROP_PROCESS_TYPE:
        g_value_set_enum (value, priv->process_type);
        break;

      case PROP_USER_ID:
        g_value_set_uint (value, priv->user_id);
        break;

      case N_PROPERTIES:
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
cby_process_info_set_property (GObject *object,
    guint prop_id,
    const GValue *value,
    GParamSpec *pspec)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (
      CBY_PROCESS_INFO (object));

  switch ((Property) prop_id)
    {
      case PROP_APPARMOR_LABEL:
        /* construct-only */
        g_assert (priv->apparmor_label == NULL);
        priv->apparmor_label = g_value_dup_string (value);
        break;

      case PROP_USER_ID:
        priv->user_id = g_value_get_uint (value);
        break;

      case PROP_BUNDLE_ID:
        /* construct-only */
        g_assert (priv->bundle_id == NULL);
        priv->bundle_id = g_value_dup_string (value);
        break;

      case PROP_PROCESS_TYPE:
        priv->process_type = g_value_get_enum (value);
        break;

      case N_PROPERTIES:
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
cby_process_info_finalize (GObject *object)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (
      CBY_PROCESS_INFO (object));

  g_free (priv->apparmor_label);
  g_free (priv->bundle_id);
  g_clear_error (&priv->init_error);

  G_OBJECT_CLASS (cby_process_info_parent_class)->finalize (object);
}

static void
cby_process_info_class_init (CbyProcessInfoClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  /**
   * CbyProcessInfo:apparmor-label:
   *
   * The AppArmor label of the process, for example
   * `unconfined`, `/usr/bin/org.apertis.SomeService1` or
   * `/Applications/com.example.SomeApp/bin/someapp`.
   * %NULL if unknown or not applicable.
   *
   * Since: 0.4.0
   */
  property_specs[PROP_APPARMOR_LABEL] = g_param_spec_string ("apparmor-label",
      "AppArmor label", "AppArmor label, or NULL if unknown",
      NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyProcessInfo:bundle-id:
   *
   * The application bundle ID of the process, for example
   * `com.example.SomeApp`. %NULL if unknown or not applicable,
   * for example because the process is part of the platform.
   *
   * Since: 0.4.0
   */
  property_specs[PROP_BUNDLE_ID] = g_param_spec_string (
      "bundle-id", "Bundle ID", "Application bundle ID, or NULL if unknown",
      NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyProcessInfo:user-id:
   *
   * The Unix user ID (`uid_t`) of the process, for example
   * 0 for processes that run as `root`, or
   * %CBY_PROCESS_INFO_NO_USER_ID if the user ID is unknown.
   *
   * Since: 0.4.0
   */
  property_specs[PROP_USER_ID] = g_param_spec_uint ("user-id",
      "User ID", "The Unix uid_t of the process, or "
      "CBY_PROCESS_INFO_NO_USER_ID if unknown",
      0, G_MAXUINT32, CBY_PROCESS_INFO_NO_USER_ID,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyProcessInfo:process-type:
   *
   * Whether the process is part of the platform, a built-in application
   * bundle, a store application bundle, or an unknown process.
   *
   * Since: 0.4.0
   */
  property_specs[PROP_PROCESS_TYPE] = g_param_spec_enum (
      "process-type", "Process type",
      "Whether the process is in an application bundle", CBY_TYPE_PROCESS_TYPE,
      CBY_PROCESS_TYPE_UNKNOWN, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  object_class->constructed = cby_process_info_constructed;
  object_class->get_property = cby_process_info_get_property;
  object_class->set_property = cby_process_info_set_property;
  object_class->finalize = cby_process_info_finalize;
  g_object_class_install_properties (object_class, N_PROPERTIES,
      property_specs);
}

/**
 * cby_process_info_get_self:
 *
 * Return information about the current process. This returns a pointer
 * to a single shared instance of #CbyProcessInfo.
 *
 * If the current process has changed its identity using privileged mechanisms
 * such as `setuid()` or `aa_change_profile()`, the result of this function
 * reflects the process's identity at the time this function was first called.
 *
 * Returns: (transfer none): a #CbyProcessInfo, which must not be freed
 *
 * Since: 0.4.0
 */
CbyProcessInfo *
cby_process_info_get_self (void)
{
  static CbyProcessInfo *init_once = NULL;

  if (g_once_init_enter (&init_once))
    {
      CbyProcessInfo *self;
      char *label = NULL;
      /* mode is part of label and must not be freed */
      char *mode = NULL;
      GError *error = NULL;

      if (G_UNLIKELY (aa_getcon (&label, &mode) < 0))
        {
          /* should never happen on Apertis */
          WARNING ("aa_getcon: %s", g_strerror (errno));
          label = NULL;
        }

      self =
          g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                          "apparmor-label", label, "user-id", getuid (), NULL);
      free (label);

      if (self == NULL)
        {
          /* init can only fail if we provided inconsistent information,
           * which _cby_parse_installation_path should never do */
          CRITICAL ("unable to make CbyProcessInfo for AppArmor label \"%s\" "
                    "and user %u: %s",
                    label, getuid (), error->message);
          g_error_free (error);
        }

      g_once_init_leave (&init_once, self);
    }

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (init_once), NULL);
  return init_once;
}

static void
process_info_get_connection_credentials_cb (GObject *source_object,
    GAsyncResult *res,
    gpointer user_data)
{
  GDBusConnection *connection = G_DBUS_CONNECTION (source_object);
  GTask *task = user_data;
  GVariant *results;
  GError *error = NULL;

  results = g_dbus_connection_call_finish (connection, res, &error);

  if (results == NULL)
    {
      g_prefix_error (&error,
          "Unable to determine method caller credentials: ");
      g_task_return_error (task, error);
    }
  else
    {
      GVariantIter *iter;
      const gchar *key;
      GVariant *value;
      guint user_id = CBY_PROCESS_INFO_NO_USER_ID;
      gchar *apparmor_label = NULL;
      CbyProcessInfo *self;

      g_variant_get (results, "(a{sv})", &iter);

      /* Reference:
       * http://dbus.freedesktop.org/doc/dbus-specification.html#bus-messages-get-connection-credentials
       */
      while (g_variant_iter_loop (iter, "{sv}", &key, &value))
        {
          if (g_strcmp0 (key, "UnixUserID") == 0)
            {
              G_STATIC_ASSERT (G_MAXUINT32 <= G_MAXUINT);

              if (G_UNLIKELY (g_variant_classify (value) !=
                    G_VARIANT_CLASS_UINT32))
                {
                  WARNING ("UnixUserID from D-Bus has unexpected type %s",
                      g_variant_get_type_string (value));
                  continue;
                }

              user_id = g_variant_get_uint32 (value);
            }
          else if (g_strcmp0 (key, "LinuxSecurityLabel") == 0)
            {
              const gchar *context;
              gchar *label;
              char *mode;

              if (G_UNLIKELY (!g_variant_is_of_type (value,
                      G_VARIANT_TYPE_BYTESTRING)))
                {
                  WARNING ("LinuxSecurityLabel from D-Bus has unexpected "
                      "type %s", g_variant_get_type_string (value));
                  continue;
                }

              context = g_variant_get_bytestring (value);

              if (G_UNLIKELY (context == NULL))
                {
                  WARNING ("LinuxSecurityLabel from D-Bus not \\0-terminated");
                  continue;
                }

              label = g_strdup (context);

              if (aa_splitcon (label, &mode) != label)
                {
                  WARNING ("Cannot split security label from D-Bus: \"%s\" "
                      "(%s)", context, g_strerror (errno));
                  g_free (label);
                  continue;
                }

              /* transfer ownership */
              apparmor_label = label;
            }
        }

      g_variant_iter_free (iter);

      self = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                             "apparmor-label", apparmor_label, "user-id",
                             user_id, NULL);

      if (self == NULL)
        {
          /* init can only fail if we provided inconsistent information,
           * which parsing an AppArmor context should never do */
          CRITICAL ("unable to make CbyProcessInfo for AppArmor label \"%s\" "
                    "and user ID %u: %s",
                    apparmor_label, user_id, error->message);
          g_error_free (error);
        }
      else
        {
          g_task_return_pointer (task, self, g_object_unref);
        }
    }

  g_object_unref (task);
}

/**
 * cby_process_info_new_for_dbus_invocation_async:
 * @invocation: object representing a D-Bus method call
 * @cancellable: (nullable): may be used to cancel object creation
 * @callback: called when process information is available
 * @user_data: (nullable): arbitrary user data for @callback
 *
 * Retrieve process information for the process that called the method
 * represented by @invocation (the sender of the method call message).
 * This is an asynchronous function using the #GAsyncResult pattern.
 *
 * If the sender of the method call message has changed its identity
 * using privileged mechanisms such as `setuid()` or `aa_change_profile()`,
 * its D-Bus credentials (and hence the result of this function) reflect
 * its identity at the time that it opened the D-Bus connection.
 *
 * Since: 0.4.0
 */
void
cby_process_info_new_for_dbus_invocation_async (GDBusMethodInvocation *invocation,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data)
{
  g_return_if_fail (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  cby_process_info_new_for_dbus_sender_async (
      g_dbus_method_invocation_get_sender (invocation),
      g_dbus_method_invocation_get_connection (invocation),
      cancellable, callback, user_data);
}

/**
 * cby_process_info_new_for_dbus_invocation_finish:
 * @res: the result of cby_process_info_new_for_dbus_invocation_async()
 * @error: used to report an error
 *
 * Interpret the result of cby_process_info_new_for_dbus_invocation_async().
 * If retrieving information about the method caller failed, set @error and
 * return %NULL.
 *
 * Returns: (transfer full) (nullable): information about the method caller,
 *  free with g_object_unref()
 *
 * Since: 0.4.0
 */
CbyProcessInfo *
cby_process_info_new_for_dbus_invocation_finish (GAsyncResult *res,
    GError **error)
{
  g_return_val_if_fail (G_IS_ASYNC_RESULT (res), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  return cby_process_info_new_for_dbus_sender_finish (res, error);
}

/**
 * cby_process_info_new_for_dbus_sender_async:
 * @sender: object representing a D-Bus method call
 * @connection: D-Bus connection giving access to @sender
 * @cancellable: (nullable): may be used to cancel object creation
 * @callback: called when process information is available
 * @user_data: (nullable): arbitrary user data for @callback
 *
 * Retrieve process information for the process that called the method
 * represented by @sender (the sender of the method call message).
 * This is an asynchronous function using the #GAsyncResult pattern.
 *
 * If the sender of the method call message has changed its identity
 * using privileged mechanisms such as `setuid()` or `aa_change_profile()`,
 * its D-Bus credentials (and hence the result of this function) reflect
 * its identity at the time that it opened the D-Bus connection.
 *
 * Since: 0.10.0
 */
void
cby_process_info_new_for_dbus_sender_async (const gchar *sender,
    GDBusConnection *connection,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data)
{
  GTask *task;

  g_return_if_fail (g_dbus_is_unique_name (sender));
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_source_tag (task, cby_process_info_new_for_dbus_sender_async);

  g_dbus_connection_call (connection, DBUS_SERVICE_DBUS, DBUS_PATH_DBUS,
      DBUS_INTERFACE_DBUS, "GetConnectionCredentials",
      g_variant_new ("(s)", sender),
      G_VARIANT_TYPE ("(a{sv})"),
      G_DBUS_CALL_FLAGS_NONE,
      -1,
      cancellable,
      process_info_get_connection_credentials_cb, task);
}

/**
 * cby_process_info_new_for_dbus_sender_finish:
 * @res: the result of cby_process_info_new_for_dbus_sender_async()
 * @error: used to report an error
 *
 * Interpret the result of cby_process_info_new_for_dbus_sender_async().
 * If retrieving information about the method caller failed, set @error and
 * return %NULL.
 *
 * Returns: (transfer full) (nullable): information about the method caller,
 *  free with g_object_unref()
 *
 * Since: 0.10.0
 */
CbyProcessInfo *
cby_process_info_new_for_dbus_sender_finish (GAsyncResult *res,
    GError **error)
{
  g_return_val_if_fail (g_async_result_is_tagged (res,
        cby_process_info_new_for_dbus_sender_async), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  return g_task_propagate_pointer (G_TASK (res), error);
}

/**
 * cby_process_info_get_apparmor_label:
 * @self: a process information object
 *
 * Return the AppArmor label of the process, or %NULL if unknown.
 *
 * Returns: (nullable) (transfer none): the #CbyProcessInfo:apparmor-label
 *
 * Since: 0.4.0
 */
const gchar *
cby_process_info_get_apparmor_label (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), NULL);

  return priv->apparmor_label;
}

/**
 * cby_process_info_get_bundle_id:
 * @self: a process information object
 *
 * Return the application bundle ID of the process, or %NULL if unknown
 * or not applicable (for example because the process is part of the
 * platform).
 *
 * Returns: (nullable) (transfer none): the #CbyProcessInfo:bundle-id
 *
 * Since: 0.4.0
 */
const gchar *
cby_process_info_get_bundle_id (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), NULL);

  return priv->bundle_id;
}

/**
 * cby_process_info_get_persistence_path:
 * @self: a process information object
 *
 * Return a directory where the process would write data.
 * The result is private to a particular combination of user ID
 * (cby_process_info_get_user_id ())
 * and application bundle ID (cby_process_info_get_bundle_id()).
 * If the process information does not contain both of those, %NULL
 * is returned instead.
 *
 * Returns: (nullable) (transfer none): the #CbyProcessInfo:persistence-path
 *
 * Since: 0.6.0
 */
const gchar *
cby_process_info_get_persistence_path (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);
  gchar *path;

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), NULL);

  /* We use an atomic pointer per instance, so that
   * cby_process_info_get_self() can be shared between threads. */
  path = g_atomic_pointer_get (&priv->persistence_path);

  if (path != NULL)
    return path;

  if (priv->bundle_id != NULL && priv->user_id != CBY_PROCESS_INFO_NO_USER_ID)
    {
      path = g_strdup_printf ("%s/%s/users/%d", CBY_PATH_PREFIX_VARIABLE_DATA,
                              priv->bundle_id, priv->user_id);

      /* ((p->pp == NULL) ? (p->pp = path, TRUE) : FALSE), but atomic */
      if (G_LIKELY (g_atomic_pointer_compare_and_exchange (
              &priv->persistence_path, NULL, path)))
        {
          /* path was stored in priv->persistence_path, do not free */
        }
      else
        {
          /* oops, we did some duplicate work */
          g_free (path);
          path = g_atomic_pointer_get (&priv->persistence_path);
          g_assert (path != NULL);
        }
    }

  return path;
}

/**
 * cby_process_info_get_downloads_path:
 * @self: a process information object
 *
 * Return a download directory where the process would download data.
 * For store or built-in app-bundles, the result is private to a
 * particular combination of user ID (cby_process_info_get_user_id ())
 * and application bundle ID (cby_process_info_get_bundle_id()).
 * For platform components, the result is private to a particular
 * combination of user ID and AppArmor label.
 * If the process information does not contain all the necessary
 * information to compute a suitable download path, %NULL
 * is returned instead.
 *
 * Returns: (nullable) (transfer none): an absolute path, or %NULL
 *
 * Since: 0.1706.9
 */
const gchar *
cby_process_info_get_downloads_path (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);
  gchar *path;
  g_autofree gchar *base_name = NULL;
  const gchar *download_dir = NULL;

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), NULL);

  /* We use an atomic pointer per instance, so that
   * cby_process_info_get_self() can be shared between threads. */
  path = g_atomic_pointer_get (&priv->downloads_path);

  if (path != NULL)
    return path;

  switch (priv->process_type)
    {
    case CBY_PROCESS_TYPE_UNKNOWN:
      return NULL;
      break;

    case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
    case CBY_PROCESS_TYPE_STORE_BUNDLE:
      path = g_build_filename (cby_process_info_get_persistence_path (self),
                               "downloads", NULL);
      break;

    case CBY_PROCESS_TYPE_PLATFORM:

      if (priv->apparmor_label == NULL ||
          !g_path_is_absolute (priv->apparmor_label))
        return NULL;

      base_name = g_path_get_basename (priv->apparmor_label);

      download_dir = g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD);
      if (download_dir != NULL)
        {
          path = g_build_filename (download_dir, base_name, NULL);
        }
      else
        {
          path = g_build_filename (g_get_home_dir (), "Downloads", base_name,
                                   NULL);
        }
      break;

    default:
      return NULL;
    }

  if (G_LIKELY (g_atomic_pointer_compare_and_exchange (
          &priv->downloads_path, NULL, path)))
    {
      /* path was stored in priv->downloads_path, do not free */
    }
  else
    {
      /* oops, we did some duplicate work */
      g_free (path);
      path = g_atomic_pointer_get (&priv->downloads_path);
      g_assert (path != NULL);
    }

  return path;
}

/**
 * cby_process_info_get_process_type:
 * @self: a process information object
 *
 * Return the type of the process.
 *
 * Returns: the value of #CbyProcessInfo:process-type
 *
 * Since: 0.4.0
 */
CbyProcessType
cby_process_info_get_process_type (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), CBY_PROCESS_TYPE_UNKNOWN);

  return priv->process_type;
}

/**
 * cby_process_info_get_user_id:
 * @self: a process information object
 *
 * Return the Unix user ID of the process, or %CBY_PROCESS_INFO_NO_USER_ID
 * if unknown.
 *
 * Returns: the value of #CbyProcessInfo:user-id
 *
 * Since: 0.4.0
 */
guint
cby_process_info_get_user_id (CbyProcessInfo *self)
{
  CbyProcessInfoPrivate *priv = cby_process_info_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_PROCESS_INFO (self), CBY_PROCESS_INFO_NO_USER_ID);

  return priv->user_id;
}

/**
 * cby_process_info_new_for_apparmor_context:
 * @context: an AppArmor context, such as `/usr/bin/foo//child (complain)`
 *
 * Return information about a process identified by its AppArmor
 * [confinement context](http://manpages.ubuntu.com/manpages/wily/en/man2/aa_getcon.2.html#contenttoc2).
 *
 * If the AppArmor context was obtained from a process ID via
 * `aa_getprocattr_raw()` or by reading `/proc/$pid`, it should not be
 * used to make authorization decisions. This is because `exec()` or
 * process ID reuse can result in the same numeric process ID being used
 * by a differently-privileged process.
 *
 * The *confinement context* consists of a *label*, a space, and a
 * *confinement mode* in parentheses. For example, `/proc/self/current/attr`
 * contains a confinement context. An optional trailing newline is allowed
 * and ignored in @context.
 *
 * As a special case, `unconfined` is also a valid confinement context;
 * it does not have a confinement mode.
 *
 * If you have a string containing only a label, use
 * cby_process_info_new_for_apparmor_label() instead.
 *
 * Anything not implied by the context, such as cby_process_info_get_user_id(),
 * will not be provided by the returned object.
 *
 * If the @context cannot be parsed successfully, the returned process
 * information object will have no information,
 * its process type will be %CBY_PROCESS_TYPE_UNKNOWN, and
 * its cby_process_info_get_apparmor_label() method will return %NULL.
 *
 * Returns: (transfer full): a newly created process information object
 *
 * Since: 0.4.0
 */
CbyProcessInfo *
cby_process_info_new_for_apparmor_context (const gchar *context)
{
  gchar *label;
  CbyProcessInfo *self = NULL;
  /* part of @label, do not free */
  char *mode;
  GError *error = NULL;

  g_return_val_if_fail (context != NULL, NULL);

  label = g_strdup (context);

  if (aa_splitcon (label, &mode) != label)
    g_clear_pointer (&label, g_free);

  self = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error, "apparmor-label",
                         label, NULL);
  g_free (label);

  if (self == NULL)
    {
      /* init can only fail if we provided inconsistent information,
       * which parsing an AppArmor context should never do */
      CRITICAL (
          "unable to make CbyProcessInfo for AppArmor context \"%s\": %s",
          context, error->message);
      g_error_free (error);
    }

  return self;
}

/**
 * cby_process_info_new_for_apparmor_label:
 * @label: an AppArmor label, such as `/usr/bin/foo//child`
 *
 * Return information about a process identified by its AppArmor label.
 * The *label* consists of one or more *profiles* separated by `//`.
 *
 * If the AppArmor context was obtained from a process ID via
 * `aa_gettaskcon()`, `aa_getprocattr()`, `aa_getprocattr_raw()` or by reading
 * `/proc/$pid`, it should not be used to make authorization decisions. This
 * is because `exec()` or process ID reuse can result in the same numeric
 * process ID being used by a differently-privileged process.
 *
 * If you have a string which may contain a *confinement mode* such as
 * `(complain)`, then that is a *confinement context*, which should not
 * be passed to this function. Use
 * cby_process_info_new_for_apparmor_context() instead.
 *
 * Anything not implied by the context, such as cby_process_info_get_user_id(),
 * will not be provided by the returned object.
 *
 * Returns: (transfer full): a newly created process information object
 *
 * Since: 0.4.0
 */
CbyProcessInfo *
cby_process_info_new_for_apparmor_label (const gchar *label)
{
  return cby_process_info_new_for_apparmor_label_and_user (label,
                                                           CBY_PROCESS_INFO_NO_USER_ID);
}

/**
 * cby_process_info_new_for_apparmor_label_and_user:
 * @label: an AppArmor label, such as `/usr/bin/foo//child`
 * @user_id: a numeric Unix user ID, or %CBY_PROCESS_INFO_NO_USER_ID
 *
 * Return information about a process identified by its AppArmor label.
 * The *label* consists of one or more *profiles* separated by `//`.
 *
 * If the AppArmor context was obtained from a process ID via
 * `aa_gettaskcon()`, `aa_getprocattr()`, `aa_getprocattr_raw()` or by reading
 * `/proc/$pid`, it should not be used to make authorization decisions. This
 * is because `exec()` or process ID reuse can result in the same numeric
 * process ID being used by a differently-privileged process.
 *
 * If you have a string which may contain a *confinement mode* such as
 * `(complain)`, then that is a *confinement context*, which should not
 * be passed to this function. Use
 * cby_process_info_new_for_apparmor_context() instead.
 *
 * Returns: (transfer full): a newly created process information object
 *
 * Since: 0.1706.9
 */
CbyProcessInfo *
cby_process_info_new_for_apparmor_label_and_user (const gchar *label,
                                                  guint user_id)
{
  GError *error = NULL;
  CbyProcessInfo *self;

  g_return_val_if_fail (label != NULL, NULL);

  /* This is consistent with libapparmor: "" is not an AppArmor label */
  if (label[0] == '\0')
    label = NULL;

  self = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error, "apparmor-label",
                         label, "user-id", user_id, NULL);

  if (self == NULL)
    {
      /* init can only fail if we provided inconsistent information,
       * which parsing an AppArmor label should never do */
      CRITICAL ("unable to make CbyProcessInfo for AppArmor label \"%s\": %s",
                label, error->message);
      g_error_free (error);
    }

  return self;
}

/**
 * cby_process_info_new_for_path_and_user:
 * @path: an absolute path in an app-bundle
 * @user_id: a numeric Unix user ID, or %CBY_PROCESS_INFO_NO_USER_ID
 *
 * Return information about the processes that would be started by the
 * given app-bundle.
 *
 * @path should either be the absolute path to
 * `/Applications/${bundle_id}` or `/usr/Applications/${bundle_id}`,
 * or some executable or directory below one of those hierarchies.
 *
 * Returns: (transfer full): a newly created process information object
 *
 * Since: 0.6.0
 */
CbyProcessInfo *
cby_process_info_new_for_path_and_user (const gchar *path, guint user_id)
{
  CbyProcessInfo *self;
  CbyProcessType process_type;
  gchar *bundle_id = NULL;
  GError *error = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (g_path_is_absolute (path), NULL);

  process_type = _cby_parse_installation_path (path, &bundle_id);

  self = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
      "process-type", process_type,
      "bundle-id", bundle_id,
      "user-id", user_id,
      NULL);
  g_free (bundle_id);

  if (self == NULL)
    {
      /* init can only fail if we provided inconsistent information,
       * which _cby_parse_installation_path should never do */
      CRITICAL ("unable to make CbyProcessInfo for path \"%s\" and "
                "user %u: %s",
                path, user_id, error->message);
      g_error_free (error);
    }

  return self;
}

static void
initable_iface_init (GInitableIface *initable_iface)
{
  initable_iface->init = cby_process_info_initable_init;
}

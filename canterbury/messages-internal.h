/*<private_header>*/
/* vim: set sts=2 sw=2 et : */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_MESSAGES_INTERNAL_H__
#define __CANTERBURY_MESSAGES_INTERNAL_H__

#include <glib.h>

#define ERROR(format, ...) g_error ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define MESSAGE(format, ...) g_message ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define INFO(format, ...) g_info ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

#endif

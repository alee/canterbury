/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_SRC_ENTRY_POINT_INDEX_H
#define CANTERBURY_SRC_ENTRY_POINT_INDEX_H

#include <glib.h>

#include "canterbury/platform/component-index.h"
#include "entry-point.h"

G_BEGIN_DECLS

#define CBY_TYPE_ENTRY_POINT_INDEX (cby_entry_point_index_get_type ())
G_DECLARE_FINAL_TYPE (CbyEntryPointIndex, cby_entry_point_index, CBY, ENTRY_POINT_INDEX, GObject)

CbyEntryPointIndex *cby_entry_point_index_new (CbyComponentIndex *component_index);

GPtrArray *cby_entry_point_index_get_entry_points (CbyEntryPointIndex *self);

CbyEntryPoint *cby_entry_point_index_get_by_id (CbyEntryPointIndex *self,
                                                const gchar *id);

G_END_DECLS

#endif

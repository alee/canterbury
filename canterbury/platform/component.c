/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/platform/component.h"

#include "canterbury/bundle.h"
#include "canterbury/enumtypes.h"
#include "canterbury/errors.h"
#include "canterbury/platform/component-internal.h"
#include "canterbury/platform/enumtypes.h"
#include "canterbury/process-info.h"

/**
 * SECTION:platform/component.h
 * @title: CbyComponent
 * @short_description: Object representing a software component
 *
 * Object representing a software component: either a
 * [built-in
 * app-bundle](https://wiki.apertis.org/Glossary#built-in-application-bundle),
 * a [store
 * app-bundle](https://wiki.apertis.org/Glossary#store-application-bundle)
 * or part of the platform. The term "component" is used in the same sense
 * here as in
 * [AppStream](https://www.freedesktop.org/software/appstream/docs/).
 *
 * #CbyComponent is a #GInitable object, so it is an error to use its APIs
 * when g_initable_init() has not completed successfully.
 * The convenience constructors cby_component_new_for_appstream_app()
 * and g_initable_new() do this automatically, and will not return an object
 * that cannot be used.
 *
 * Since: 0.11.0
 */

struct _CbyComponent
{
  GObject parent;
  CbyProcessType type;
  AsApp *app;

  /* populated lazily */
  gchar **categories;

  GError *init_error;
};

typedef enum
{
  PROP_APPSTREAM_APP = 1,
  PROP_BUNDLE_ID,
  PROP_COMPONENT_TYPE,
  PROP_ID,
} Property;

static void initable_iface_init (GInitableIface *initable_iface);

static GParamSpec *property_specs[PROP_ID + 1] = { NULL };

G_DEFINE_TYPE_WITH_CODE (CbyComponent,
                         cby_component,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                initable_iface_init))

/**
 * cby_component_new_for_appstream_app:
 * @type: the type of bundle
 * @app: the AppStream app or component
 *
 * Construct a new software component information object.
 * @app must not be modified while the returned object exists.
 *
 * Returns: (transfer full): a new #CbyComponent
 *
 * Since: 0.11.0
 */
CbyComponent *
cby_component_new_for_appstream_app (CbyProcessType type,
                                     AsApp *app,
                                     GError **error)
{
  g_return_val_if_fail ((type == CBY_PROCESS_TYPE_STORE_BUNDLE ||
                         type == CBY_PROCESS_TYPE_BUILT_IN_BUNDLE ||
                         type == CBY_PROCESS_TYPE_PLATFORM),
                        NULL);
  g_return_val_if_fail (AS_IS_APP (app), NULL);

  return g_initable_new (CBY_TYPE_COMPONENT, NULL, error,
                         "appstream-app", app,
                         "component-type", type,
                         NULL);
}

static void
cby_component_init (CbyComponent *self)
{
}

static void
cby_component_constructed (GObject *object)
{
  CbyComponent *self = CBY_COMPONENT (object);
  const gchar *id;
  const gchar *other_id;

  /* We check for validity before returning the new object, so that we don't
   * have to track "is it initialized yet?" separately. */

  if (!AS_IS_APP (self->app))
    {
      g_set_error (&self->init_error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                   "Required \"appstream-app\" property not provided");
      return;
    }

  id = as_app_get_id (self->app);

  switch (self->type)
    {
    case CBY_PROCESS_TYPE_STORE_BUNDLE:
    case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
      if (!cby_is_bundle_id (id))
        {
          g_set_error (&self->init_error, CBY_ERROR,
                       CBY_ERROR_INVALID_ARGUMENT,
                       "\"%s\" is not a valid bundle ID", id);
          return;
        }

      other_id =
          as_app_get_metadata_item (self->app, CBY_APPSTREAM_X_BUNDLE_ID);

      if (g_strcmp0 (id, other_id) != 0)
        {
          g_set_error (&self->init_error, CBY_ERROR,
                       CBY_ERROR_INVALID_ARGUMENT,
                       "%s \"%s\" does not match \"%s\"",
                       CBY_APPSTREAM_X_BUNDLE_ID, other_id, id);
          return;
        }
      break;

    case CBY_PROCESS_TYPE_PLATFORM:
      break;

    case CBY_PROCESS_TYPE_UNKNOWN:
    default:
      {
        g_set_error (&self->init_error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                     "Invalid \"component-type\": %d", self->type);
        return;
      }
    }
}

static gboolean
cby_component_initable_init (GInitable *initable,
                             GCancellable *cancellable,
                             GError **error)
{
  CbyComponent *self = CBY_COMPONENT (initable);

  /* We did the checks at construct time; just pass on the result. */

  if (self->init_error == NULL)
    return TRUE;

  g_set_error_literal (error, self->init_error->domain, self->init_error->code,
                       self->init_error->message);
  return FALSE;
}

static void
cby_component_get_property (GObject *object,
                            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec)
{
  CbyComponent *self = CBY_COMPONENT (object);

  g_return_if_fail (self->init_error == NULL);

  switch ((Property) prop_id)
    {
    case PROP_APPSTREAM_APP:
      g_value_set_object (value, self->app);
      break;

    case PROP_BUNDLE_ID:
      g_value_set_string (value, cby_component_get_bundle_id (self));
      break;

    case PROP_COMPONENT_TYPE:
      g_value_set_enum (value, self->type);
      break;

    case PROP_ID:
      g_value_set_string (value, cby_component_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_component_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
  CbyComponent *self = CBY_COMPONENT (object);

  g_return_if_fail (self->init_error == NULL);

  switch ((Property) prop_id)
    {
    case PROP_APPSTREAM_APP:
      /* construct-only */
      g_assert (self->app == NULL);
      self->app = g_value_dup_object (value);
      break;

    case PROP_COMPONENT_TYPE:
      self->type = g_value_get_enum (value);
      break;

    case PROP_BUNDLE_ID:
    case PROP_ID:
    /* read-only, fall through */
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_component_dispose (GObject *object)
{
  CbyComponent *self = CBY_COMPONENT (object);

  g_clear_object (&self->app);

  G_OBJECT_CLASS (cby_component_parent_class)->dispose (object);
}

static void
cby_component_class_init (CbyComponentClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed = cby_component_constructed;
  object_class->get_property = cby_component_get_property;
  object_class->set_property = cby_component_set_property;
  object_class->dispose = cby_component_dispose;

  /**
   * CbyComponent:appstream-app:
   *
   * The #AsApp containing metadata for the component.
   * This is never %NULL unless g_initable_init() failed.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_APPSTREAM_APP] = g_param_spec_object (
      "appstream-app", "AppStream app",
      "Complete metadata for the application bundle", AS_TYPE_APP,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyComponent:bundle-id:
   *
   * The application bundle ID of the bundle, for example
   * `com.example.SomeApp`, or %NULL if the component is not a bundle.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_BUNDLE_ID] =
      g_param_spec_string ("bundle-id", "Bundle ID", "Application bundle ID",
                           NULL, G_PARAM_READABLE);

  /**
   * CbyComponent:component-type:
   *
   * Whether this is a built-in application bundle, a store
   * application bundle or a platform component. This is never
   * %CBY_PROCESS_TYPE_UNKNOWN unless g_initable_init() failed.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_COMPONENT_TYPE] =
      g_param_spec_enum ("component-type", "Component type",
                         "Whether this is a store or built-in application "
                         "bundle, or part of the platform",
                         CBY_TYPE_PROCESS_TYPE, CBY_PROCESS_TYPE_UNKNOWN,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyComponent:id:
   *
   * The application bundle ID of an application bundle, for example
   * `com.example.SomeApp`, or some other ID for platform components, for
   * example `thunar.desktop`. This is never %NULL unless g_initable_init()
   * failed.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_ID] = g_param_spec_string (
      "id", "ID", "Software component ID", NULL, G_PARAM_READABLE);

  g_object_class_install_properties (
      object_class, G_N_ELEMENTS (property_specs), property_specs);
}

/**
 * cby_component_get_component_type:
 * @self: a software component
 *
 * Return the type of the bundle. This is never %CBY_PROCESS_TYPE_UNKNOWN
 * unless g_initable_init() failed.
 *
 * Returns: (transfer none): the value of #CbyComponent:component-type
 *
 * Since: 0.11.0
 */
CbyProcessType
cby_component_get_component_type (CbyComponent *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT (self), CBY_PROCESS_TYPE_UNKNOWN);
  g_return_val_if_fail (self->init_error == NULL, CBY_PROCESS_TYPE_UNKNOWN);

  return self->type;
}

/**
 * cby_component_get_bundle_id:
 * @self: a software component
 *
 * Return the identifier of the bundle, for example `com.example.Calculator`,
 * or %NULL if this is not an app-bundle.
 *
 * Returns: (nullable) (transfer none): the value of #CbyComponent:bundle-id
 *
 * Since: 0.11.0
 */
const gchar *
cby_component_get_bundle_id (CbyComponent *self)
{
  const gchar *id;

  g_return_val_if_fail (CBY_IS_COMPONENT (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);

  if (self->type != CBY_PROCESS_TYPE_STORE_BUNDLE &&
      self->type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
    return NULL;

  id = as_app_get_id (self->app);
  g_return_val_if_fail (cby_is_bundle_id (id), NULL);
  return id;
}

/**
 * cby_component_get_id:
 * @self: a software component
 *
 * Return the identifier of the bundle, for example `com.example.Calculator`.
 * This is always non-%NULL unless g_initable_init() failed, but it is not
 * necessarily a syntactically valid bundle ID in the case of
 * platform components.
 *
 * Returns: the value of #CbyComponent:id
 *
 * Since: 0.11.0
 */
const gchar *
cby_component_get_id (CbyComponent *self)
{
  const gchar *id;

  g_return_val_if_fail (CBY_IS_COMPONENT (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);

  id = as_app_get_id (self->app);

  if (self->type == CBY_PROCESS_TYPE_STORE_BUNDLE ||
      self->type == CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
    g_return_val_if_fail (cby_is_bundle_id (id), NULL);

  return id;
}

/**
 * cby_component_get_appstream_app:
 * @self: a software component
 *
 * Return the bundle's complete metadata.
 *
 * The returned #AsApp is owned by @self and must not be freed or
 * modified. This is always non-%NULL unless g_initable_init() failed.
 *
 * Prefer to use a convenience function like cby_component_get_id(),
 * if one exists.
 *
 * Returns: (transfer none): an #AsApp for read-only access
 *
 * Since: 0.11.0
 */
AsApp *
cby_component_get_appstream_app (CbyComponent *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);

  g_return_val_if_fail (AS_IS_APP (self->app), NULL);
  return self->app;
}

static int
indirect_strcmp (gconstpointer pa, gconstpointer pb)
{
  const gchar *const *a = pa;
  const gchar *const *b = pb;

  return g_strcmp0 (*a, *b);
}

/**
 * cby_component_get_categories:
 * @self: a software component
 *
 * Return the machine-readable categories for the bundle. These can either
 * be a *main category*, *additional category* or *reserved category* as
 * documented by the
 * [freedesktop.org Desktop Menu
 * specification](https://specifications.freedesktop.org/menu-spec/latest/apa.html),
 * a category specific to Apertis in the form `X-Apertis-Something` as
 * documented
 * [on the Apertis wiki](https://wiki.apertis.org/Application_Bundle_Metadata),
 * or a vendor-specific category in the form `X-MyVendor-Something` as
 * defined by that vendor.
 *
 * For example, the music player `org.apertis.Frampton` is expected to
 * have at least `Audio`, `AudioVideo` and `Music` among its categories.
 *
 * User interfaces that display categories, for example an app-store, are
 * expected to convert these standardized category names into whatever
 * visual presentation is desired by the UI designer. For example,
 * `org.apertis.Frampton` displayed in an English-language locale might
 * be shown in a category labelled "Audio", "Multimedia" or "M U S I C",
 * depending on the UI designer's preference.
 *
 * Returns: (transfer none) (array zero-terminated=1) (element-type utf8):
 *  a sorted list of categories
 *
 * Since: 0.11.0
 */
const gchar *const *
cby_component_get_categories (CbyComponent *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);

  if (self->categories == NULL)
    {
      GPtrArray *categories = as_app_get_categories (self->app);
      GPtrArray *copy = NULL;

      if (categories == NULL)
        {
          copy = g_ptr_array_sized_new (1);
          g_ptr_array_add (copy, NULL);
        }
      else
        {
          guint i;

          copy = g_ptr_array_sized_new (categories->len + 1);

          for (i = 0; i < categories->len; i++)
            g_ptr_array_add (copy,
                             g_strdup (g_ptr_array_index (categories, i)));

          g_ptr_array_sort (copy, indirect_strcmp);
          g_ptr_array_add (copy, NULL);
        }

      self->categories = (gchar **) g_ptr_array_free (copy, FALSE);
    }

  return (const gchar *const *) self->categories;
}

/**
 * cby_component_get_settings_schema_id:
 * @self: a #CbyComponent
 *
 * Return the ID of the GSettings schema for the bundle’s user-visible
 * preferences, if the bundle provides any preferences; %NULL otherwise. This
 * can be passed to the system preferences application to be displayed, for
 * example.
 *
 * This ID will typically be the same as the bundle ID, as GSettings and bundle
 * naming schemes are similar. However, callers must not assume this is always
 * the case: the ID might be prefixed by the bundle ID; or might not contain
 * the bundle ID at all, if the component is a built-in or platform component.
 *
 * Note that bundles may ship and use additional GSettings schemas which are
 * not returned by this function; they will not appear in the user interface.
 *
 * Returns: (nullable): settings schema ID, or %NULL if the bundle has no
 *    preferences
 * Since: 0.12.0
 */
const gchar *
cby_component_get_settings_schema_id (CbyComponent *self)
{
  const gchar *schema_id;

  g_return_val_if_fail (CBY_IS_COMPONENT (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);

  /* We can’t validate the schema ID, other than whether it’s a non-empty
   * string, as GSettings doesn’t do any validation. */
  schema_id = as_app_get_metadata_item (self->app,
                                        CBY_APPSTREAM_X_SETTINGS_SCHEMA);
  g_return_val_if_fail (schema_id == NULL || *schema_id != '\0', NULL);

  return schema_id;
}

static void
initable_iface_init (GInitableIface *initable_iface)
{
  initable_iface->init = cby_component_initable_init;
}

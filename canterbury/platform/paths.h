/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_PLATFORM_PATHS_H
#define CANTERBURY_PLATFORM_PATHS_H

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury-platform.h> can be included directly"
#endif

#include <glib.h>

G_BEGIN_DECLS

/**
 * SECTION:platform/paths.h
 * @title: Platform path utilities
 * @short_description: Platform path related utility functions and enumerations
 */

/**
 * CbyPrepareAppBundleFlags:
 * @CBY_PREPARE_APP_BUNDLE_FLAGS_NONE: No special behaviour
 *
 * Flags affecting how system services prepare an app bundle for use.
 * There are no flags yet; this enum is for future expansion.
 *
 * Since: 0.5.0
 */
typedef enum /*< flags >*/
{
  CBY_PREPARE_APP_BUNDLE_FLAGS_NONE = 0
} CbyPrepareAppBundleFlags;

/**
 * CBY_PATH_PREFIX_STORE_BUNDLE:
 *
 * The absolute path of the directory containing static data for
 * [store application bundles](https://wiki.apertis.org/Glossary#store-application-bundle).
 * Individual application bundles each have a subdirectory named according
 * to the the bundle ID.
 *
 * Since: 0.5.0
 */
#define CBY_PATH_PREFIX_STORE_BUNDLE "/Applications"

/**
 * CBY_PATH_PREFIX_BUILT_IN_BUNDLE:
 *
 * The absolute path of the directory containing static data for
 * [built-in application bundles](https://wiki.apertis.org/Glossary#built-in-application-bundle).
 * Individual application bundles each have a subdirectory named according
 * to the the bundle ID.
 *
 * Since: 0.5.0
 */
#define CBY_PATH_PREFIX_BUILT_IN_BUNDLE "/usr/Applications"

/**
 * CBY_PATH_PREFIX_VARIABLE_DATA:
 *
 * The absolute path of the directory containing variable data for all
 * [application bundles](https://wiki.apertis.org/Glossary#application-bundle).
 * Individual application bundles each have a subdirectory named according
 * to the the bundle ID.
 *
 * Since: 0.5.0
 */
#define CBY_PATH_PREFIX_VARIABLE_DATA "/var/Applications"

/**
 * CBY_PATH_SYSTEM_EXTENSIONS:
 *
 * The absolute path of the directory containing application bundles'
 * entry points, icons and other
 * [system extensions](https://docs.apertis.org/latest/applications.html#system-extensions).
 *
 * Since: 0.1612.1
 */
#define CBY_PATH_SYSTEM_EXTENSIONS "/var/lib/apertis_extensions"

G_END_DECLS

#endif

/*<private_header>*/
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_PLATFORM_COMPONENT_INDEX_INTERNAL_H__
#define __CANTERBURY_PLATFORM_COMPONENT_INDEX_INTERNAL_H__

#include "canterbury/platform/component-index.h"

/* Built-in app bundles and the platform have a very high priority. */
#define CBY_APPSTREAM_PRIORITY_PLATFORM (1000)

/* Installed store app bundles have a low priority, lower than what's
 * available from an app store (which would normally be the default
 * priority, 0). */
#define CBY_APPSTREAM_PRIORITY_INSTALLED_STORE (-1)

/* Cached location for built-in app-bundles and the platform. */
#define CBY_APPSTREAM_CACHE_PLATFORM                                          \
  "/var/cache/app-info/xmls/apertis-platform.xml.gz"

/* Cached location for installed store app bundles.
 * There will be a separate location for available store app bundles,
 * later. */
#define CBY_APPSTREAM_CACHE_INSTALLED_STORE                                   \
  "/var/cache/app-info/xmls/apertis-installed-store.xml.gz"

/* Mainly exposed here for tests' benefit */
static inline CbyComponentIndex *
_cby_component_index_new_full (CbyComponentIndexFlags flags,
                               const gchar *platform_cache,
                               const gchar *store_cache,
                               GError **error)
{
  return g_initable_new (CBY_TYPE_COMPONENT_INDEX, NULL, error,
                         "flags", flags,
                         "platform-cache-path", platform_cache,
                         "store-cache-path", store_cache,
                         NULL);
}

#endif

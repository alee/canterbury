/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_SRC_ENTRY_POINT_H
#define CANTERBURY_SRC_ENTRY_POINT_H

#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define CBY_TYPE_ENTRY_POINT (cby_entry_point_get_type ())
G_DECLARE_FINAL_TYPE (CbyEntryPoint, cby_entry_point, CBY, ENTRY_POINT, GObject)

gboolean cby_entry_point_should_show (CbyEntryPoint *self);

const gchar *cby_entry_point_get_id (CbyEntryPoint *self);

const gchar *cby_entry_point_get_executable (CbyEntryPoint *self);

const gchar *cby_entry_point_get_display_name (CbyEntryPoint *self);

gchar *cby_entry_point_get_string (CbyEntryPoint *self, const gchar *key);

GIcon *cby_entry_point_get_icon (CbyEntryPoint *self);

const gchar *const *cby_entry_point_get_supported_types (CbyEntryPoint *self);

void cby_entry_point_activate_async (CbyEntryPoint *self,
                                     GVariant *platform_data,
                                     GCancellable *cancellable,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);
gboolean cby_entry_point_activate_finish (CbyEntryPoint *self,
                                          GAsyncResult *result,
                                          GError **error);

void cby_entry_point_open_uris_async (CbyEntryPoint *self,
                                      const gchar * const *uris,
                                      GVariant *platform_data,
                                      GCancellable *cancellable,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data);
gboolean cby_entry_point_open_uris_finish (CbyEntryPoint *self,
                                           GAsyncResult *result,
                                           GError **error);

void cby_entry_point_open_uri_async (CbyEntryPoint *self,
                                     const gchar *uri,
                                     GVariant *platform_data,
                                     GCancellable *cancellable,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);
gboolean cby_entry_point_open_uri_finish (CbyEntryPoint *self,
                                          GAsyncResult *result,
                                          GError **error);

G_END_DECLS

#endif

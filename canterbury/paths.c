/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/paths.h"

#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

#include "canterbury/bundle.h"
#include "canterbury/messages-internal.h"
#include "canterbury/process-info.h"

/**
 * SECTION:paths.h
 * @title: General path utilities
 * @short_description: General path related utility functions and enumerations
 */

/**
 * cby_get_persistence_path:
 * @error: used to raise an error on failure
 *
 * Return a directory where this process can write data.
 * The result is private to a particular combination of user ID (`getuid()`)
 * and application bundle ID (cby_get_bundle_id()).
 *
 * If the bundle ID cannot be determined, or the persistence directory
 * does not exist and cannot be created, this function sets the error
 * and returns %NULL.
 *
 * Returns: (transfer none): an absolute path, or %NULL on error
 */
const gchar *
cby_get_persistence_path (GError **error)
{
  CbyProcessInfo *me;
  const gchar *path;

  me = cby_process_info_get_self ();
  g_return_val_if_fail (CBY_IS_PROCESS_INFO (me), NULL);

  path = cby_process_info_get_persistence_path (me);

  if (path == NULL)
    {
      g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                           "Not part of an app bundle");
      return NULL;
    }

  /* TODO: some centralized component like Ribchester should create
   * the directories before an ordinary app is launched (and
   * ordinary apps shouldn't have the permissions to do so)
   * at which point we can stop trying to create them
   */
  if (!g_file_test (path, G_FILE_TEST_IS_DIR) &&
      g_mkdir_with_parents (path, 0700) != 0)
    {
      int saved_errno = errno;

      g_set_error_literal (error, G_IO_ERROR,
                           g_io_error_from_errno (saved_errno),
                           g_strerror (saved_errno));
      path = NULL;
    }

  return path;
}

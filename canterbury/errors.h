/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_ERRORS_H
#define CANTERBURY_ERRORS_H

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury.h> can be included directly"
#endif

#include <glib.h>

G_BEGIN_DECLS

/**
 * CBY_ERROR:
 *
 * Error domain for errors encountered by the Canterbury application
 * framework. Error codes in this domain are members of the #CbyError
 * enumeration.
 */
#define CBY_ERROR (cby_error_quark ())

/**
 * CbyError:
 * @CBY_ERROR_FAILED: A generic error: "something went wrong"
 * @CBY_ERROR_INVALID_ARGUMENT: Input to a function was invalid or inconsistent
 * @CBY_ERROR_ACCESS_DENIED: Caller is not authorised to launch the given
 *    application (Since: 0.12.2)
 * @CBY_ERROR_BUSY: Canterbury is currently launching an application and cannot
 *    launch another at the same time (Since: 0.12.2)
 * @CBY_ERROR_LAUNCH_FAILED: Unknown error when executing the process while
 *    launching an application (Since: 0.12.2)
 * @CBY_ERROR_INVALID_ENTRY_POINT: Either the entry point id or the bundle id for
 *    the entry point is invalid (Since: 0.1706.0)
 * @CBY_ERROR_NOT_OUR_ENTRY_POINT: The process type of the entry point could not
 *    be identified (Since: 0.1706.0)
 *
 * Error enumeration for errors encountered by the Canterbury
 * application framework.
 */
typedef enum
{
  CBY_ERROR_FAILED,
  CBY_ERROR_INVALID_ARGUMENT,
  CBY_ERROR_ACCESS_DENIED,
  CBY_ERROR_BUSY,
  CBY_ERROR_LAUNCH_FAILED,
  CBY_ERROR_INVALID_ENTRY_POINT,
  CBY_ERROR_NOT_OUR_ENTRY_POINT,
  CBY_N_ERRORS /*< skip >*/
} CbyError;

GQuark cby_error_quark (void);

G_END_DECLS

#endif

## Canterbury Overview

[Mail the maintainers](mailto:canterbury@apertis.org)

At the heart of the App framework lays Canterbury - the application
manager which controls the existence of all the applications running in
the system. Canterbury is designed to help developers add their
applications very easily into the system and make use of all the
services provided by existing applications and daemons with the help of
D-Bus framework. New applications can also render their own services and
any other application can make use of the same.

Canterbury is started by the SystemD at startup which in turn starts the
other services and applications. It launches a set of services to
support various functionalities in the order explained below. These
services are part of its process space

* Launcher Manager: Launch manager is the first service started by the application
    manager. A complete list of all application's preferences is
    maintained in the configuration database which gets loaded into the
    launch manager during initialization. The launch manager maintains
    these preferences and uses it to launch an application or a daemon.
    The preferences are basically a known set of key value pairs clearly
    understood by the Launch manager. The launch manager is designed
    flexibly to support SDK users to add their applications into the
    system as well.

* Activity Manager: Activity Manager is responsible for maintaining a database of all
    applications running in the system and also controlling the
    different states of each application

* Hardkey Manager: Hardkey Manager defines the available hardkeys and interfaces with
    teh faceplate. It provides the hardkey events to the app framework
    where a suitable action can be taken

* Mutter Plugin: Mutter plugin provides the interface between the application
    manager - Canterbury and the apps for window management

* Audio Manager: Audio Manager is responsible to grant and revoke the audio requests
    made by apps via the audio service. It also interfaces with pulse
    audio to manage the priorities

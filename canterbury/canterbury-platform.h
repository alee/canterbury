/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_CANTERBURY_PLATFORM_H
#define CANTERBURY_CANTERBURY_PLATFORM_H

#include <canterbury/canterbury.h>

#define __CANTERBURY_IN_META_HEADER__
#include <canterbury/platform/component.h>
#include <canterbury/platform/component-index.h>
#include <canterbury/platform/dbus.h>
#include <canterbury/platform/entry-point.h>
#include <canterbury/platform/entry-point-index.h>
#include <canterbury/platform/enumtypes.h>
#include <canterbury/platform/environment.h>
#include <canterbury/platform/org.apertis.Canterbury.PerUserAppManager1.h>
#include <canterbury/platform/org.apertis.Canterbury.PrivilegedAppHelper1.h>
#include <canterbury/platform/paths.h>
#undef __CANTERBURY_IN_META_HEADER__

#endif

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>

/usr/bin/canterbury-exec {
  # This is deliberately not <abstractions/chaiwala-base>, which is designed
  # for general-purpose apps and includes GSettings, gvfs, X11, OpenGL and
  # so on. canterbury-exec is part of the TCB between app-bundles, and
  # shouldn't have unnecessary permissions.
  #include <abstractions/base>
  #include <abstractions/dbus-strict>

  /usr/bin/canterbury-exec mr,

  # Run built-in applications (we use *.* to exclude old-style non-namespaced
  # applications, as well as non-app directories like System)
  /usr/Applications/*.*/bin/* px,

  # Run store applications
  /Applications/*.*/bin/* px,

  # Create app directories
  /var/Applications/*.*/{,**/} rw,

  # Create a symlink for the global dconf database
  /var/Applications/*.*/users/*/config/dconf w,

  # Configure XDG well-known directories, including overwriting via
  # a temporary file
  /var/Applications/*.*/users/*/config/user-dirs.dirs{,.*} rw,

  # Open /proc/self/fd to make fds close-on-exec
  owner @{PROC}/@{pid}/fd/ r,

  # canterbury-full can kill us (but canterbury-core does not need to
  # because it relies on systemd, which is unconfined, for process management)
  signal receive peer=/usr/bin/canterbury,

  # We can talk to Ribchester
  dbus (send, receive) bus=system peer=(label=/usr/bin/ribchester),
  dbus (send, receive) bus=system peer=(label=/usr/lib/ribchester/ribchester-core),

  # Don't use pluggable GIO modules at all.
  deny /usr/lib/@{multiarch}/gio/modules/{,**} mr,
}

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "last-user-mode"
#include "last_user_mode_if.h"

#include <errno.h>

#include "app_launch_mgr_if.h"
#include "audio_mgr_if.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "hardkeys_mgr_if.h"
#include "launch_mgr_if.h"
#include "win_mgr_if.h"

#define LAST_USER_MODE_DEBUG_PRINT(...) DEBUG (__VA_ARGS__)

typedef struct _AppInfo AppInfo;
struct _AppInfo
{
  CbyEntryPoint *entry_point;
  GStrv arguments;
};

static void
app_info_free (AppInfo *app_info)
{
  g_return_if_fail (app_info != NULL);

  g_clear_object (&app_info->entry_point);
  g_strfreev (app_info->arguments);
  g_free (app_info);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (AppInfo, app_info_free)

GList  *pAppInfoList=NULL;
static void last_user_mode_update_back_stack (void);
static void launch_audio_application (CbyEntryPoint *entry_point);

/* Example contents of state.ini:
 *
 * [State]
 * BackStack=org.apertis.Eye;com.example.Other;org.apertis.Frampton.Artists;
 * AudioSource=org.apertis.Frampton.Agent;
 *
 * [BackStackEntry org.apertis.Eye]
 * Argv=/Applications/org.apertis.Eye/bin/eye;--argument;value;
 */
#define STATE_GROUP "State"
#define STATE_KEY_BACK_STACK "BackStack"
#define STATE_KEY_AUDIO_SOURCE "AudioSource"
#define BACK_STACK_ENTRY_GROUP_FORMAT "BackStackEntry %s"
#define BACK_STACK_ENTRY_KEY_ARGV "Argv"

/* Step 2.0: update the back stack */
static void
last_user_mode_update_back_stack (void)
{
  gint inCount = g_list_length (pAppInfoList) - 1;

  LAST_USER_MODE_DEBUG_PRINT ("last_user_mode_update_back_stack\n");

  if(g_list_length(pAppInfoList))
  {
    while (inCount > 0 ) //dont update the last item as you anyways launch it
    {
      AppInfo *pappInfo=(AppInfo *)g_list_nth_data(pAppInfoList,inCount);
      /* We copy the arguments because app_mgr_update_app_back_stack()
       * consumes them. */
      guint i;

      DEBUG ("Back stack entry %d: \"%s\"", inCount,
             cby_entry_point_get_id (pappInfo->entry_point));

      for (i = 0; pappInfo->arguments != NULL && pappInfo->arguments[i] != NULL; i++)
        DEBUG ("- argument: \"%s\"", pappInfo->arguments[i]);

      /* dont launch the app on top just yet, wait for launcher to be up then launch it*/
      back_stack_update_entry (pappInfo->entry_point,
                               (const gchar * const *) pappInfo->arguments,
                               CANTERBURY_LUM_CLIENT);
      inCount--;
    }
    // inform wm that startup window is about to be launched
   v_last_user_mode_status(TRUE);
  }
  else
  {
    v_last_user_mode_status(FALSE);
  }
}

static void
handle_application_audio (CbyEntryPoint *entry_point)
{
  const gchar *id = cby_entry_point_get_id (entry_point);
  AppInfo *pappInfo= NULL;

  if(NULL != pAppInfoList)
    pappInfo=(AppInfo *)g_list_nth_data(pAppInfoList,0);
  /* check if the app on top of back stack is the application holding the audio resource*/
  if (pappInfo == NULL || pappInfo->entry_point != entry_point)
    {
      if (cby_entry_point_get_executable (entry_point) != NULL)
        {
          launch_audio_application (entry_point);
        }
      else
        {
          g_warning ("the audio application %s does not have the right arguments\n",
                     id);
        }
    }
  else if (pappInfo != NULL)
    {
      guint i;

      /* app on top of back stack is the application holding the audio resource, just update the arguments */
      for (i = 0;
           pappInfo->arguments != NULL && pappInfo->arguments[i] != NULL;
           i++)
        {
          if (strcmp (pappInfo->arguments[i], "play-mode") == 0 &&
              pappInfo->arguments[i + 1] != NULL)
            {
              g_free (pappInfo->arguments[i + 1]);
              pappInfo->arguments[i + 1] = g_strdup ("play");
            }
        }
    }
}

static void
launch_audio_daemon_or_app (CbyEntryPoint *entry_point)
{
  CanterburyExecutableType executable_type = _cby_entry_point_get_executable_type (entry_point);

  if ((executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE) ||
      (executable_type == CANTERBURY_EXECUTABLE_TYPE_SERVICE))
    {
      v_launch_audio_daemon (entry_point);
    }
  else if (executable_type == CANTERBURY_EXECUTABLE_TYPE_APPLICATION)
    {
      handle_application_audio (entry_point);
    }
}

/* Step 3.1: play the normal audio agent */

/* Step 3.2: play the normal audio application */
static void
audio_application_launch_finish_cb (GObject *source_object,
                                    GAsyncResult *result,
                                    gpointer user_data)
{
  g_autoptr (CbyEntryPoint) entry_point = CBY_ENTRY_POINT (user_data);
  g_autoptr (GError) error = NULL;

  if (launch_mgr_launch_app_finish (result, &error))
    DEBUG ("Audio application \"%s\" started successfully",
           cby_entry_point_get_id (entry_point));
  else
    WARNING ("Error starting audio application \"%s\": %s",
             cby_entry_point_get_id (entry_point), error->message);
}

static void
launch_audio_application (CbyEntryPoint *entry_point)
{
  g_autoptr (GError) error = NULL;

  launch_mgr_launch_app_async (entry_point, CANTERBURY_LUM_CLIENT, 0xff, NULL,
                               NULL, NULL,
                               (LAUNCH_FLAGS_SET_ACTIVE_TITLE |
                                LAUNCH_FLAGS_ARGV_REPLACE_PLAY_MODE),
                               NULL,
                               audio_application_launch_finish_cb,
                               g_object_ref (entry_point));
}

/* Step 6.0: when the launcher comes up, launch the previous app from the back stack automatically */
static void
last_user_mode_launch_previous_app_cb (GObject *source_object,
                                       GAsyncResult *result,
                                       gpointer user_data)
{
  g_autoptr (CbyEntryPoint) entry_point = user_data;
  g_autoptr (GError) error = NULL;

  if (!app_mgr_launch_new_application_finish (result, &error))
    WARNING ("Unable to launch \"%s\": %s",
             cby_entry_point_get_id (entry_point), error->message);
}

void
last_user_mode_launch_previous_app (void)
{
  g_autoptr (GError) error = NULL;
  AppInfo *app_info;
  const gchar *id;
  guint i;

  if (pAppInfoList == NULL)
    return;

  app_info = pAppInfoList->data;
  g_assert (app_info != NULL);

  id = cby_entry_point_get_id (app_info->entry_point);
  DEBUG ("launching \"%s\"", id);

  for (i = 0;
       app_info->arguments != NULL && app_info->arguments[i] != NULL;
       i++)
    DEBUG ("- argument: \"%s\"", app_info->arguments[i]);

  app_mgr_launch_new_application_async (id, NULL, NULL,
                                        (const gchar * const *) app_info->arguments,
                                        CANTERBURY_LUM_CLIENT, 0xff, NULL,
                                        last_user_mode_launch_previous_app_cb,
                                        g_object_ref (app_info->entry_point));

  /* Step 6.1: free the resources for LUM */
  g_list_free_full (pAppInfoList, (GDestroyNotify) app_info_free);
  pAppInfoList = NULL;
}

/* LUM startup behaviour
 * 1.0 -> Init the pdi and read all its contents
 * 2.0 -> Update the application back stack
 * 3.0 -> launch & play the last heard audio app/agent
 * 4.0 -> Launch all startup apps and agents including launcher
 * 5.0 -> cleanup the LUM content from pdi & wait for launcher to be up
 * 6.0 -> launch the app on top from LUM.
 */

void
last_user_mode_init (CbyEntryPointIndex *entry_point_index)
{
  g_autoptr (GKeyFile) key_file = NULL;
  g_autofree gchar *our_data_dir = NULL;
  g_autofree gchar *filename = NULL;
  GError *error = NULL;
  const gchar *audio_source;

  /* Step 1.0: initialize and read state */

  our_data_dir = g_build_filename (g_get_user_data_dir (), "canterbury", NULL);

  if (g_mkdir_with_parents (our_data_dir, 0700) < 0)
    {
      WARNING ("Unable to create state directory \"%s\": %s",
               our_data_dir, g_strerror (errno));
    }

  key_file = g_key_file_new ();

  if (g_key_file_load_from_data_dirs (key_file, "canterbury/state.ini", NULL,
                                      G_KEY_FILE_NONE, &error))
    {
      guint i;
      g_auto (GStrv) names = NULL;

      /* Ignore any errors here and treat them as an empty GStrv */
      names = g_key_file_get_string_list (key_file, STATE_GROUP,
                                          STATE_KEY_BACK_STACK, NULL, NULL);

      for (i = 0; names != NULL && names[i] != NULL; i++)
        {
          g_autoptr (CbyEntryPoint) entry_point = NULL;
          g_autoptr (AppInfo) app_info = NULL;
          g_auto (GStrv) argv = NULL;
          g_autofree gchar *group = NULL;
          guint j;

          entry_point =
            cby_entry_point_index_get_by_id (entry_point_index, names[i]);

          if (entry_point == NULL)
            {
              WARNING ("Unable to restore \"%s\": Entry point not found",
                       names[i]);
              continue;
            }

          DEBUG ("Restoring app \"%s\"", names[i]);
          app_info = g_new0 (AppInfo, 1);
          app_info->entry_point = g_object_ref (entry_point);
          app_info->arguments = NULL;

          group = g_strdup_printf (BACK_STACK_ENTRY_GROUP_FORMAT, names[i]);
          /* Ignore any errors here and treat them as if there were
           * no arguments */
          argv = g_key_file_get_string_list (key_file, group,
                                             BACK_STACK_ENTRY_KEY_ARGV, NULL,
                                             NULL);

          for (j = 0; argv != NULL && argv[j] != NULL; j++)
            DEBUG ("- argument: \"%s\"", argv[j]);

          app_info->arguments = g_steal_pointer (&argv);
          pAppInfoList = g_list_append (pAppInfoList,
                                        g_steal_pointer (&app_info));
        }
    }
  else if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT) ||
           g_error_matches (error, G_KEY_FILE_ERROR,
                            G_KEY_FILE_ERROR_NOT_FOUND))
    {
      DEBUG ("Last-used mode data not found, ignoring: %s", error->message);
      g_clear_error (&error);
    }
  else
    {
      WARNING ("Unable to load last-used mode data: %s %d: %s",
               g_quark_to_string (error->domain), error->code, error->message);
      g_clear_error (&error);
    }

  last_user_mode_update_back_stack();

  /* Step 3.0: play the normal audio source last played; agent or
   * application */

  /* Ignore any errors here and treat them as if there was no audio source */
  audio_source = g_key_file_get_string (key_file, STATE_GROUP,
                                        STATE_KEY_AUDIO_SOURCE, NULL);

  if (audio_source != NULL)
    {
      g_autoptr (CbyEntryPoint) entry_point = NULL;

      entry_point = cby_entry_point_index_get_by_id (entry_point_index, audio_source);

      if (entry_point == NULL)
        {
          WARNING ("Unable to restore audio source \"%s\": not found",
                   audio_source);
        }
      else if (_cby_entry_point_get_audio_resource_owner_id (entry_point) == NULL)
        {
          WARNING ("Unable to restore audio source \"%s\": it has no "
                   "audio resource owner", audio_source);
        }
      else
        {
          g_autoptr (CbyEntryPoint) owner = NULL;
          const gchar *owner_id = _cby_entry_point_get_audio_resource_owner_id (entry_point);

          owner = cby_entry_point_index_get_by_id (entry_point_index, owner_id);
          if (owner == NULL)
            {
              WARNING ("Unable to restore audio source \"%s\": audio resource "
                       "owner \"%s\" not found", audio_source, owner_id);
            }
          else
            {
              launch_audio_daemon_or_app (owner);
            }
        }
    }

  /* Launcher get launched here, once its confirmation comes, only then will the last app on top would be launched */
  /* FIXME: the name of this function indicates that it might fail, but
   * in fact it can't. Maybe we shouldn't clear out state.ini until startup
   * has reached some milestone? */
  launch_mgr_launch_startup_applications ();

  /* Step 5.0: Empty the state file so we won't relaunch the same things
   * next time.
   * We write out an empty file in the highest-priority data directory,
   * so that it "masks" anything in lower-priority entries of the
   * search path. */
  filename = g_build_filename (our_data_dir, "state.ini", NULL);

  if (!g_file_set_contents (filename, "", 0, &error))
    {
      WARNING ("Unable to clear last-used mode data: %s %d: %s",
               g_quark_to_string (error->domain), error->code, error->message);
      g_clear_error (&error);
    }
}

/* LUM shutdown behaviour
 * 0.0 -> Send kill signal to all apps and deamons
 * 1.0 -> save the back status
 * 2.0 -> save audio status
 */

void
last_user_mode_save_status (void)
{
  g_autoptr (GKeyFile) key_file = NULL;
  g_autofree gchar *our_data_dir = NULL;
  g_autofree gchar *filename = NULL;
  CbyBackStackIter iter;
  BackStack *pAppInfo;
  /* Array of unowned strings, borrowed from global data */
  g_autoptr (GPtrArray) back_stack = NULL;
  g_autofree gchar *audio_source = NULL;
  GError *error = NULL;

  our_data_dir = g_build_filename (g_get_user_data_dir (), "canterbury", NULL);

  if (g_mkdir_with_parents (our_data_dir, 0700) < 0)
    {
      WARNING ("Unable to create state directory \"%s\": %s",
               our_data_dir, g_strerror (errno));
      /* nothing here is going to work if we can't create the state
       * directory, so bail out */
      return;
    }

  back_stack = g_ptr_array_new ();
  key_file = g_key_file_new ();

  /* 1.0 -> save the back status */

  cby_back_stack_iter_init (&iter);

  while (cby_back_stack_iter_next (&iter, &pAppInfo))
    {
      const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

      g_ptr_array_add (back_stack, (gchar *) id);

      if (pAppInfo->pArgvList != NULL && pAppInfo->pArgvList[0] != NULL)
        {
          g_autofree gchar *group = NULL;

          group = g_strdup_printf (BACK_STACK_ENTRY_GROUP_FORMAT, id);
          g_key_file_set_string_list (key_file, group,
                                      BACK_STACK_ENTRY_KEY_ARGV,
                                      (const gchar * const *) pAppInfo->pArgvList,
                                      g_strv_length (pAppInfo->pArgvList));
        }
    }

  g_ptr_array_add (back_stack, NULL);

  g_key_file_set_string_list (key_file, STATE_GROUP, STATE_KEY_BACK_STACK,
                              (const gchar * const *) back_stack->pdata,
                              back_stack->len);

  /* 2.0 -> save audio status */
  audio_source = audio_mgr_get_current_normal_audio_source ();

  DEBUG ("Audio source is %s", audio_source);

  if (audio_source != NULL)
    {
      g_key_file_set_string (key_file, STATE_GROUP, STATE_KEY_AUDIO_SOURCE,
                             audio_source);
    }

  filename = g_build_filename (our_data_dir, "state.ini", NULL);

  if (!g_key_file_save_to_file (key_file, filename, &error))
    {
      WARNING ("Unable to save last-used mode data: %s %d: %s",
               g_quark_to_string (error->domain), error->code, error->message);
      g_clear_error (&error);
    }
}


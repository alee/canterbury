/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __HARDKEYS__
#define __HARDKEYS__

#include <gio/gio.h>

#include "activity_mgr_if.h"
#include "app_mgr_if.h"
#include "audio_mgr_if.h"
#include "canterbury/gdbus/canterbury.h"
#include "hardkeys_mgr_if.h"
#include "last_user_mode_if.h"
#include "launch_mgr_if.h"
#include "launcher_mgr_if.h"
#include "win_mgr_if.h"

G_BEGIN_DECLS

void back_handler_init (void);

gboolean
handle_back_press_clb( CanterburyHardKeys *object,
                       GDBusMethodInvocation   *invocation,
                       gpointer                 pUserData );

gboolean handle_register_back_press_clb (CanterburyHardKeys *object,
                                         GDBusMethodInvocation *invocation,
                                         const gchar *entry_point_id,
                                         gpointer user_data);

gboolean handle_back_press_consumed_clb (CanterburyHardKeys *object,
                                         GDBusMethodInvocation *invocation,
                                         const gchar *entry_point_id,
                                         gboolean consumed_status,
                                         gpointer pUserData);

gboolean handle_disable_back_press_clb (CanterburyHardKeys *object,
                                        GDBusMethodInvocation *invocation,
                                        const gchar *entry_point_id,
                                        gboolean disable_status,
                                        gpointer pUserData);

gboolean
handle_home_press_clb( CanterburyHardKeys *object,
                       GDBusMethodInvocation   *invocation,
                       gpointer                 pUserData );

gboolean
handle_register_now_playing_press_clb( CanterburyHardKeys *object,
                                       GDBusMethodInvocation   *invocation,
                                       gpointer                 pUserData );

gboolean
handle_register_category_press_clb( CanterburyHardKeys *object,
                                    GDBusMethodInvocation   *invocation,
                                    gpointer                 pUserData );

gboolean
handle_register_shutdown_clb( CanterburyHardKeys *object,
                              GDBusMethodInvocation   *invocation,
                              gpointer                 pUserData );

void hard_keys_emit_back_press_response (gboolean result);

void hard_keys_emit_back_press_register_response (const gchar *entry_point_id);

void show_launcher_app (void);

G_END_DECLS

#endif //__HARDKEYS__

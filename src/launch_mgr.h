/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LAUNCH_MGR__
#define __LAUNCH_MGR__

#include <glib.h>

#include "activity_mgr_if.h"
#include "app_mgr.h"
#include "hardkeys_mgr_if.h"
#include "launch_mgr_if.h"
#include "win_mgr_if.h"

G_BEGIN_DECLS

gboolean b_launch_mgr_build_application_database (void);

G_END_DECLS

#endif //__LAUNCH_MGR__

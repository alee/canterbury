/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SYS_RES_MGR_IF__
#define __SYS_RES_MGR_IF__

#include <glib.h>

G_BEGIN_DECLS

/* system resource manager mechanism */
void system_resource_mgr_init (void);

void
add_pid_to_memory_cgroup(guint pid);

void check_if_application_killed_from_OOM (const gchar *entry_point_id);

void
add_bw_prio_to_bandwidth_cgroup(guint pid , guint prio);

G_END_DECLS

#endif //__SYS_RES_MGR_IF__

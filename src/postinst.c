/*
 * canterbury-postinst: configure an app-bundle after mounting it for the
 * first time
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "apparmor.h"
#include "util.h"

static GOptionEntry entries[] = { { NULL } };

static gboolean
enable_apparmor_profile (const gchar *bundle_id,
                         GError **error)
{
  g_autofree gchar *profile_basename =
      g_strdup_printf ("Applications.%s", bundle_id);
  g_autofree gchar *profile_source =
      g_build_filename (CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id,
                        "etc", "apparmor.d", profile_basename, NULL);
  g_autoptr (CbyAppArmorStore) apparmor = NULL;

  /* If there is nothing to do, be happy. Maybe the bundle only contains
   * themes or something. */
  if (!g_file_test (profile_source, G_FILE_TEST_EXISTS))
    return TRUE;

  apparmor = _cby_apparmor_store_open (CBY_PATH_SYSTEM_EXTENSIONS "/apparmor.d",
                                       error);

  if (apparmor == NULL)
    return FALSE;

  return _cby_apparmor_store_enable_and_load_profile (apparmor, profile_source,
                                                      bundle_id, error);
}

int
main (int argc, char **argv)
{
  GError *error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autofree gchar *stamp = NULL;
  const gchar *bundle_id;
  const gchar *version;
  int saved_errno;

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- configure an application bundle");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      g_error_free (error);
      return 2;
    }

  if (argc < 2)
    {
      g_printerr ("%s: a bundle to be configured is required\n",
                  g_get_prgname ());
      return 2;
    }

  if (argc < 3)
    {
      g_printerr ("%s: the version to be configured is required\n",
                  g_get_prgname ());
      return 2;
    }

  bundle_id = argv[1];
  version = argv[2];

  if (!cby_is_bundle_id (bundle_id))
    {
      g_printerr ("%s: not a bundle ID: \"%s\"\n", g_get_prgname (),
                  bundle_id);
      return 2;
    }

  /* TODO: stop doing this when we do something useful instead */
  stamp = g_strdup_printf ("%s/%s.stamp", PKGCACHEDIR, bundle_id);

  if (!g_file_set_contents (stamp, version, -1, &error))
    {
      WARNING ("%s: unable to create \"%s\": %s",
               g_get_prgname (), stamp, error->message);
      g_clear_error (&error);
    }

  if (!enable_apparmor_profile (bundle_id, &error))
    {
      CRITICAL ("%s: unable to load AppArmor profile: %s",
                g_get_prgname (), error->message);
      g_clear_error (&error);
      return 1;
    }

  /* Drop privileges by executing a less special executable */
  execl (PKGLIBEXECDIR "/canterbury-postinst-entry-points", /* program to run */
         PKGLIBEXECDIR "/canterbury-postinst-entry-points", /* argv[0] */
         bundle_id,
         NULL);

  /* Still here? Then something has gone very wrong */
  saved_errno = errno;
  g_printerr ("%s: unable to execute canterbury-postinst-entry-points: %s\n",
              g_get_prgname (), g_strerror (saved_errno));
  return 1;
}

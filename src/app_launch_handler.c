/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "app-launch-mgr"
#include "app_launch_mgr_if.h"

#include <stdlib.h>

#include <gio/gdesktopappinfo.h>
#include <gio/gio.h>
#include <glib.h>

#include "canterbury/canterbury.h"
#include "canterbury/canterbury-platform.h"
#include "canterbury/gdbus/canterbury.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "canterbury/platform/org.apertis.Canterbury.Private.EntryPoints1.h"
#include "launch_mgr_if.h"
#include "launcher_mgr_if.h"

CanterburyAppDbHandler* pAppLaunchMgrDataObj = NULL;
static _CbyEntryPoints1 *entry_points_skeleton = NULL;

typedef struct _DBusMethodInvocationData DBusMethodInvocationData;

struct _DBusMethodInvocationData
{
  CanterburyAppDbHandler *handler;
  GDBusMethodInvocation *invocation;
};

static DBusMethodInvocationData *
dbus_method_invocation_data_new (CanterburyAppDbHandler *handler,
                                 GDBusMethodInvocation *invocation)
{
  DBusMethodInvocationData *self;

  self = g_slice_new0 (DBusMethodInvocationData);
  self->handler = g_object_ref (handler);
  self->invocation = g_object_ref (invocation);
  return self;
}

static void
dbus_method_invocation_data_free (DBusMethodInvocationData *self)
{
  g_object_unref (self->handler);
  g_object_unref (self->invocation);
  g_slice_free (DBusMethodInvocationData, self);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DBusMethodInvocationData,
                               dbus_method_invocation_data_free)

static void launch_app_preferences_process_info_cb (GObject      *obj,
                                                    GAsyncResult *result,
                                                    gpointer      user_data);

static void
launch_app_preferences_async_cb (GObject *source_object,
                                 GAsyncResult *result,
                                 gpointer user_data)
{
  DBusMethodInvocationData *invocation_data = user_data;
  g_autoptr (GError) error = NULL;

  if (app_mgr_launch_new_application_finish (result, &error))
    canterbury_app_db_handler_complete_launch_app_preferences (invocation_data->handler,
                                                               invocation_data->invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation_data->invocation, error);

  dbus_method_invocation_data_free (invocation_data);
}

static gboolean
launch_app_preferences_cb (CanterburyAppDbHandler *obj,
                           GDBusMethodInvocation  *invocation,
                           const gchar            *requested_bundle_id,
                           gpointer                user_data)
{
  DEBUG ("LaunchAppPreferences(‘%s’)", requested_bundle_id);

  /* Validate the bundle ID first. */
  if (!cby_is_bundle_id (requested_bundle_id))
    {
      DEBUG ("LaunchAppPreferences error: %s", "invalid bundle ID");
      g_dbus_method_invocation_return_error (invocation, CBY_ERROR,
                                             CBY_ERROR_INVALID_ARGUMENT,
                                             "‘%s’ is not a valid bundle ID.",
                                             requested_bundle_id);
      return TRUE;
    }

  /* Start the permissions checks for the caller. */
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
                                                  launch_app_preferences_process_info_cb,
                                                  dbus_method_invocation_data_new (obj, invocation));

  return TRUE;
}

static void
launch_app_preferences_process_info_cb (GObject      *obj,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  const gchar *arguments[] = { NULL, NULL };
  const gchar *uris[] = { NULL, NULL };
  g_autofree gchar *uri = NULL;
  CbyProcessType caller_type;
  g_autoptr (CbyProcessInfo) caller_info = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (DBusMethodInvocationData) data = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autofree gchar *opener_entry_point_id = NULL;
  g_autofree gchar *opener_real_path = NULL;
  g_autoptr (CbyProcessInfo) opener_info = NULL;
  CbyProcessType opener_type;
  const gchar *requested_bundle_id, *caller_bundle_id;
  const gchar *opener_filename;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) opener_entry_point = NULL;
  g_autoptr (CbyProcessInfo) opener_process_info = NULL;
  const gchar *opener_bundle_id;

  /* `caller` is asking `opener` to show the preferences of `requested`. */
  data = g_steal_pointer (&user_data);
  g_variant_get (g_dbus_method_invocation_get_parameters (data->invocation),
                 "(&s)", &requested_bundle_id);

  DEBUG ("Preferences bundle ID: %s", requested_bundle_id);

  caller_info = cby_process_info_new_for_dbus_invocation_finish (result,
                                                                 &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (data->invocation, error);
      return;
    }

  /* Get the details for the settings program we’re going to run (typically
   * org.apertis.Mildenhall.Settings, but it might be customised by vendors). */
  settings = g_settings_new ("org.apertis.Canterbury.Config");
  opener_entry_point_id = g_settings_get_string (settings,
                                              "preferences-entry-point");
  entry_point_index = launch_mgr_get_entry_point_index ();
  opener_entry_point = cby_entry_point_index_get_by_id (entry_point_index,
                                                        opener_entry_point_id);

  DEBUG ("Preferences app: entry point: %s", opener_entry_point_id);

  if (opener_entry_point == NULL)
    {
      WARNING ("Preferences application ‘%s’ does not exist.",
               opener_entry_point_id);
      g_dbus_method_invocation_return_error (data->invocation, CBY_ERROR,
                                             CBY_ERROR_ACCESS_DENIED,
                                             "Preferences application ‘%s’ "
                                             "does not exist.",
                                             opener_entry_point_id);
      return;
    }

  /* This will produce something like
   * /usr/share/applications/org.apertis.MildenhallSettings[.EntryPoint].desktop,
   * which is a symlink to a built-in app, theoretically. */
  opener_filename = g_desktop_app_info_get_filename (G_DESKTOP_APP_INFO (_cby_entry_point_get_app_info(opener_entry_point)));
  opener_real_path = realpath (opener_filename, NULL);

  DEBUG ("Preferences app: filename: %s, real path: %s",
         opener_filename, opener_real_path);

  if (opener_real_path == NULL)
    goto opener_error;

  opener_process_info = cby_process_info_new_for_path_and_user (opener_real_path,
                                                                getuid ());

  if (opener_process_info == NULL)
    goto opener_error;

  opener_type = cby_process_info_get_process_type (opener_process_info);
  opener_bundle_id = cby_process_info_get_bundle_id (opener_process_info);

  DEBUG ("Preferences app: type: %u, bundle ID: %s",
         opener_type, opener_bundle_id);

  if (opener_type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
    goto opener_error;

  /* Check that the caller has permission to start the preferences for the given
   * application bundle. If the caller is org.apertis.Mildenhall.Settings or
   * its vendor-specific replacement built-in bundle, it always has permission.
   *
   * This is the implementation of the security policy for
   * LaunchAppPreferences. */
  caller_type = cby_process_info_get_process_type (caller_info);
  caller_bundle_id = cby_process_info_get_bundle_id (caller_info);

  DEBUG ("Caller: type: %u, bundle ID: %s", caller_type, caller_bundle_id);

  if ((caller_type != CBY_PROCESS_TYPE_STORE_BUNDLE &&
       caller_type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE) ||
      (g_strcmp0 (caller_bundle_id, requested_bundle_id) != 0 &&
       g_strcmp0 (caller_bundle_id, opener_bundle_id)) != 0)
    {
      WARNING ("Bundle ‘%s’ is not allowed to launch preferences for bundle "
               "‘%s’.", caller_bundle_id, requested_bundle_id);
      g_dbus_method_invocation_return_error (data->invocation, CBY_ERROR,
                                             CBY_ERROR_ACCESS_DENIED,
                                             "Bundle ‘%s’ is not allowed to "
                                             "launch preferences for bundle "
                                             "‘%s’.",
                                             caller_bundle_id,
                                             requested_bundle_id);
      return;
    }

  /* Typically the equivalent of running:
   *    canterbury-exec /Applications/o.a.M.S/bin/mildenhall-settings \
   *        $requested_bundle_id
   * or
   *    gapplication launch o.a.M.S apertis-bundle:$requested_bundle_id
   */
  arguments[0] = requested_bundle_id;
  uri = g_strdup_printf ("apertis-bundle:%s", requested_bundle_id);
  uris[0] = uri;

  /* The 0xff gets passed through to
   * StartAppSwitch in the Mutter plugin, then promptly ignored. */
  app_mgr_launch_new_application_async (opener_entry_point_id, uris, NULL,
                                        arguments,
                                        CANTERBURY_SELF_CLIENT,
                                        0xff,
                                        NULL,
                                        launch_app_preferences_async_cb,
                                        g_steal_pointer (&data));

  return;

opener_error:
  WARNING ("Preferences application ‘%s’ is not a built-in application.",
           opener_entry_point_id);
  g_dbus_method_invocation_return_error (data->invocation, CBY_ERROR,
                                         CBY_ERROR_ACCESS_DENIED,
                                         "Preferences application ‘%s’ is "
                                         "not a built-in application.",
                                         opener_entry_point_id);
}

static void
activate_entry_point_final_cb (GObject *source_object,
                               GAsyncResult *result,
                               gpointer user_data)
{
  _CbyEntryPoints1 *iface = _CBY_ENTRY_POINTS1 (source_object);
  g_autoptr (GDBusMethodInvocation) invocation = user_data;
  g_autoptr (GError) error = NULL;

  if (g_task_propagate_boolean (G_TASK (result), &error))
    _cby_entry_points1_complete_activate_entry_point (iface,
                                                      invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static void
activate_entry_point_launch_app_cb (GObject *source_object,
                                    GAsyncResult *result,
                                    gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (launch_mgr_launch_app_finish (result, &error))
    g_task_return_boolean (task, TRUE);
  else
    g_task_return_error (task, g_steal_pointer (&error));
}

static void
activate_entry_point_process_info_cb (GObject *source_object,
                                      GAsyncResult *result,
                                      gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyProcessInfo) caller_info = NULL;
  g_autoptr (GVariant) platform_data = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  GVariantDict hints;
  GDBusMethodInvocation *invocation;
  GVariant *tuple;
  const gchar *entry_point_id;
  CbyProcessType caller_type;
  const gchar *launched_by;
  guint32 numeric_special_args;
  LaunchFlags flags;

  caller_info = cby_process_info_new_for_dbus_invocation_finish (result,
                                                                 &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  caller_type = cby_process_info_get_process_type (caller_info);

  if (caller_type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE &&
      caller_type != CBY_PROCESS_TYPE_PLATFORM)
    {
      /* TODO: finer-grained access control on this? */
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "ActivateEntryPoint() can only be called by "
                               "built-in app-bundles or platform components");
      return;
    }

  invocation = g_task_get_task_data (task);
  tuple = g_dbus_method_invocation_get_parameters (invocation);
  g_variant_get (tuple, "(&s@a{sv})", &entry_point_id, &platform_data);

  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_point = cby_entry_point_index_get_by_id (entry_point_index,
                                                 entry_point_id);

  if (entry_point == NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Entry point “%s” not found", entry_point_id);
      return;
    }

  g_variant_dict_init (&hints, platform_data);

  if (!g_variant_dict_lookup (&hints, "X-Apertis-LaunchedBy", "&s",
                             &launched_by))
    launched_by = CANTERBURY_UNKNOWN_CLIENT;

   /* FIXME: I have no idea what this means, but mildenhall-launcher and
    * didcot both use 0xff so that seems safe. If this is still needed,
    * we could pass it through platform data. */
  numeric_special_args = 0xff;

  flags = (LAUNCH_FLAGS_UPDATE_BACK_STACK |
           LAUNCH_FLAGS_START_TIMER |
           LAUNCH_FLAGS_CHECK_EXISTING_GUI |
           LAUNCH_FLAGS_SET_ACTIVE_TITLE |
           LAUNCH_FLAGS_HIDE_GLOBAL_POPUPS);

  launch_mgr_launch_app_async (entry_point, launched_by, numeric_special_args,
                               NULL, platform_data, NULL, flags, NULL,
                               activate_entry_point_launch_app_cb,
                               g_object_ref (task));

  g_variant_dict_clear (&hints);
}

static gboolean
activate_entry_point_cb (CanterburyAppDbHandler *obj,
                         GDBusMethodInvocation  *invocation,
                         const gchar            *entry_point_id,
                         GVariant               *platform_data,
                         gpointer                user_data)
{
  g_autoptr (GTask) task = g_task_new (obj, NULL,
                                       activate_entry_point_final_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, g_object_ref (invocation), g_object_unref);
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
                                                  activate_entry_point_process_info_cb,
                                                  g_object_ref (task));

  return TRUE; /* handled */
}

static void
open_uris_final_cb (GObject *source_object,
                    GAsyncResult *result,
                    gpointer user_data)
{
  _CbyEntryPoints1 *iface = _CBY_ENTRY_POINTS1 (source_object);
  g_autoptr (GDBusMethodInvocation) invocation = user_data;
  g_autoptr (GError) error = NULL;

  if (g_task_propagate_boolean (G_TASK (result), &error))
    _cby_entry_points1_complete_open_uris (iface, invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static void
open_uris_async_cb (GObject *source_object,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (app_mgr_launch_new_application_finish (result, &error))
    g_task_return_boolean (task, TRUE);
  else
    g_task_return_error (task, g_steal_pointer (&error));
}

static void
open_uris_process_info_cb (GObject *source_object,
                           GAsyncResult *result,
                           gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyProcessInfo) caller_info = NULL;
  g_autoptr (GVariant) platform_data = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  GVariantDict hints;
  g_autofree const gchar **uris = NULL; /* shallow copy */
  g_autofree const gchar **content_types = NULL; /* shallow copy */
  GDBusMethodInvocation *invocation;
  GVariant *tuple;
  const gchar *entry_point_id;
  const gchar *apparmor_label;
  const gchar *content_type;
  const gchar *arguments[5];
  CbyProcessType caller_type;
  guint32 numeric_special_args;

  caller_info = cby_process_info_new_for_dbus_invocation_finish (result,
                                                                 &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  caller_type = cby_process_info_get_process_type (caller_info);
  apparmor_label = cby_process_info_get_apparmor_label (caller_info);

  if (caller_type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE &&
      caller_type != CBY_PROCESS_TYPE_PLATFORM)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "OpenURIs() can only be called by "
                               "built-in app-bundles or platform components");
      return;
    }

  if (g_strcmp0 (apparmor_label, "/usr/bin/didcot") != 0 &&
      g_strcmp0 (apparmor_label, "unconfined") != 0)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "OpenURIs() can only be called by "
                               "Didcot or by unconfined processes");
      return;
    }

  invocation = g_task_get_task_data (task);
  tuple = g_dbus_method_invocation_get_parameters (invocation);
  g_variant_get (tuple, "(&s^a&s@a{sv})", &entry_point_id, &uris,
                 &platform_data);

  if (uris == NULL || uris[0] == NULL || uris[1] != NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "OpenURIs() currently only accepts one URI");
      return;
    }

  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_point = cby_entry_point_index_get_by_id (entry_point_index,
                                                 entry_point_id);

  if (entry_point == NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Entry point “%s” not found", entry_point_id);
      return;
    }

  g_variant_dict_init (&hints, platform_data);

  if (g_variant_dict_lookup (&hints, "X-Apertis-ContentTypes", "^a&s",
                             &content_types) &&
      content_types != NULL &&
      content_types[0] != NULL &&
      content_types[1] == NULL)
    content_type = content_types[0];
  else
    content_type = "application/octet-stream";

   /* FIXME: I have no idea what this means, but mildenhall-launcher and
    * didcot both use 0xff so that seems safe. If this is still needed,
    * we could pass it through platform data. */
  numeric_special_args = 0xff;

  /* This is what the combination of Didcot and LaunchNewApp passes as
   * arguments.
   * TODO: if the entry point is DBusActivatable or supports placeholders
   * like %u, use those instead. */
  arguments[0] = "mime-type";
  arguments[1] = content_type;
  arguments[2] = "launch";
  arguments[3] = uris[0];
  arguments[4] = NULL;

  /* FIXME: it isn't clear why, but Launcher.LaunchNewApp() historically
   * called hide_global_popups(), whereas AppLaunchDb didn't. For the moment
   * we do this for ActivateEntryPoint() but not for OpenURIs(). */

  app_mgr_launch_new_application_async (entry_point_id, uris, platform_data,
                                        arguments,
                                        CANTERBURY_DATA_EXG_MGR_CLIENT,
                                        numeric_special_args,
                                        NULL,
                                        open_uris_async_cb,
                                        g_object_ref (task));

  g_variant_dict_clear (&hints);
}

static gboolean
open_uris_cb (CanterburyAppDbHandler *obj,
              GDBusMethodInvocation  *invocation,
              const gchar            *entry_point_id,
              GVariant               *platform_data,
              gpointer                user_data)
{
  g_autoptr (GTask) task = g_task_new (obj, NULL,
                                       open_uris_final_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, g_object_ref (invocation), g_object_unref);
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
                                                  open_uris_process_info_cb,
                                                  g_object_ref (task));

  return TRUE; /* handled */
}

void
init_app_launch (GDBusConnection *connection)
{
  g_autoptr (GError) error = NULL;

  DEBUG ("enter");

  entry_points_skeleton = _cby_entry_points1_skeleton_new ();

  g_signal_connect (entry_points_skeleton,
                    "handle-activate-entry-point",
                    G_CALLBACK (activate_entry_point_cb),
                    NULL);
  g_signal_connect (entry_points_skeleton,
                    "handle-open-uris",
                    G_CALLBACK (open_uris_cb),
                    NULL);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (entry_points_skeleton),
                                         connection,
                                         CBY_OBJECT_PATH_PRIVATE_ENTRY_POINTS1,
                                         &error))
    {
      CRITICAL ("Unable to export EntryPoints1 interface: %s", error->message);
      return;
    }

  pAppLaunchMgrDataObj = canterbury_app_db_handler_skeleton_new();

  g_signal_connect (pAppLaunchMgrDataObj,
                    "handle-launch-app-preferences",
                    (GCallback) launch_app_preferences_cb,
                    NULL);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pAppLaunchMgrDataObj),
                                         connection,
                                         "/org/apertis/Canterbury/AppDbHandler",
                                         NULL))
  {
    app_launch_mgr_debug("export error \n");
  }
}

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"
#include "hard_keys.h"

#include <string.h>

#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"

gboolean handle_register_now_playing_press_clb( CanterburyHardKeys *object,
                                                GDBusMethodInvocation   *invocation,
                                                gpointer                 pUserData )
{
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  gchar *now_playing = audio_mgr_get_current_active_audio_source ();
  /* either NULL or @now_playing */
  const gchar *entry_point_id = NULL;
  /* borrowed from entry_point, which must not be allowed to be freed until we have
   * finished using it */
  const gchar *display_name = NULL;

  hide_global_popups ();

  if (now_playing != NULL)
    {
      entry_point_index = launch_mgr_get_entry_point_index ();
      entry_point = cby_entry_point_index_get_by_id (entry_point_index, now_playing);

      if (entry_point != NULL && _cby_entry_point_get_mangled_display_name (entry_point) != NULL)
        {
          entry_point_id = now_playing;
          display_name = _cby_entry_point_get_mangled_display_name (entry_point);
        }
    }

  canterbury_hard_keys_complete_now_playing_press (object, invocation);
  canterbury_hard_keys_emit_now_playing_press_response (object,
                                                        entry_point_id,
                                                        display_name);
  g_free (now_playing);

  return TRUE;
}


/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "app-mgr"
#include "system_res_mgr_if.h"

#include "activity_mgr_if.h"
#include "app_mgr_if.h"
#include "canterbury/messages-internal.h"
#include "shapwick.h"
#include "shutdown.h"

ShapwickMemory    *pMemoryProxy = NULL;
ShapwickBandwidth *pBandwidthProxy = NULL;

gchar* pAppNameKilled = NULL;


static void
app_pid_clb( GObject *source_object,
             GAsyncResult *res,
             gpointer user_data)
{
  gboolean return_val;
  GError *error = NULL;

  return_val = shapwick_memory_call_add_process_to_memory_cgroup_finish( (ShapwickMemory*)source_object,
                                                                                               res,
                                                                                               &error);
  if(return_val == FALSE)
  {
     gchar* error_msg = g_dbus_error_get_remote_error(error);
     g_printerr("error %s \n",error_msg);
     g_free(error_msg);

     g_dbus_error_strip_remote_error(error);
     g_printerr("error %s \n",error->message);
  }
}

void
add_pid_to_memory_cgroup(guint pid)
{
  if(pMemoryProxy != NULL)
  {
	  shapwick_memory_call_add_process_to_memory_cgroup( pMemoryProxy,
                                                                             pid,
                                                                             NULL,
                                                                             app_pid_clb,
                                                                             NULL
                                                                             );
  }
}

static void
app_bw_prio_clb( GObject *source_object,
                 GAsyncResult *res,
                 gpointer user_data)
{
  gboolean return_val;
  GError *error = NULL;

  return_val = shapwick_bandwidth_call_add_process_to_bandwidth_cgroup_finish( (ShapwickBandwidth*)source_object,
                                                                                                     res,
                                                                                                     &error);
  if(return_val == FALSE)
  {
     gchar* error_msg = g_dbus_error_get_remote_error(error);
     g_printerr("error %s \n",error_msg);
     g_free(error_msg);

     g_dbus_error_strip_remote_error(error);
     g_printerr("error %s \n",error->message);
  }
}

void
add_bw_prio_to_bandwidth_cgroup(guint pid , guint prio)
{
  if(pBandwidthProxy != NULL)
  {
	  shapwick_bandwidth_call_add_process_to_bandwidth_cgroup( pBandwidthProxy,
                                                                                   pid,
                                                                                   prio,
                                                                                   NULL,
                                                                                   app_bw_prio_clb,
                                                                                   NULL
                                                                                 );
  }
}

static void
shutdown_cb (GObject      *source_object,
             GAsyncResult *result,
             gpointer      user_data)
{
  GError *error = NULL;

  canterbury_shutdown_finish (result, &error);

  if (error != NULL)
    {
      g_printerr ("Reboot unsuccessful: %s\n", error->message);
      g_error_free (error);
    }
}

static void
handle_oom_notification( ShapwickMemory *object,
                         gpointer  pUserData )
{
  g_autofree gchar *victim;

  g_printerr("handle_oom_notification \n");

  victim = activity_mgr_force_kill_something ();

  if (victim == NULL)
    {
      WARNING ("Could not find a process to kill: rebooting system instead");
      canterbury_shutdown_async (CANTERBURY_SHUTDOWN_REBOOT, TRUE, NULL,
                                 shutdown_cb, NULL);

      return;
    }

  pAppNameKilled = g_steal_pointer (&victim);
}


static gboolean
register_for_oom_notification (gpointer pData)
{
	shapwick_memory_call_register_oomnotification( pMemoryProxy,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL );
  return FALSE;
}

void
check_if_application_killed_from_OOM (const gchar *entry_point_id)
{
  if (pAppNameKilled != NULL && g_strcmp0 (pAppNameKilled, entry_point_id) == 0)
  {
    g_free(pAppNameKilled);
    pAppNameKilled = NULL;
    g_print("check_if_application_killed_from_OOM \n");

    if(pMemoryProxy != NULL)
    {
      g_timeout_add_seconds(3 , register_for_oom_notification , NULL);
    }
  }
}

static void
memory_proxy_clb( GObject *source_object,
                  GAsyncResult *res,
                  gpointer pUserData )
{
  GError *error;
  /* finishes the proxy creation and gets the proxy ptr */
  pMemoryProxy = shapwick_memory_proxy_new_finish(res , &error);

  if(pMemoryProxy == NULL)
  {
     g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
  }
  shapwick_memory_call_register_oomnotification( pMemoryProxy,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL );
  g_signal_connect (pMemoryProxy,
                    "oomnotification",
                    G_CALLBACK (handle_oom_notification),
                    pUserData);
}

static void
memory_name_appeared ( GDBusConnection *connection,
                       const gchar     *name,
                       const gchar     *name_owner,
                       gpointer         pUserData)
{
  //g_print("memory_name_appeared \n");

  /* Asynchronously creates a proxy for the D-Bus interface */
	shapwick_memory_proxy_new(
                                                    connection,
                                                    G_DBUS_PROXY_FLAGS_NONE,
                                                    "Shapwick.Memory",
                                                    "/Shapwick/Memory",
                                                    NULL,
                                                    memory_proxy_clb,
                                                    NULL );
}

static void
memory_name_vanished(GDBusConnection *connection,
                     const gchar     *name,
                     gpointer         pUserData)
{
 g_print("memory_name_vanished \n");
 if(NULL != pMemoryProxy)
 g_object_unref(pMemoryProxy);
 pMemoryProxy = NULL;
}

static void
bandwidth_proxy_clb( GObject *source_object,
                     GAsyncResult *res,
                     gpointer pUserData )
{
 GError *error;
  /* finishes the proxy creation and gets the proxy ptr */
 pBandwidthProxy = shapwick_bandwidth_proxy_new_finish(res , &error);

 if(pBandwidthProxy == NULL)
 {
     g_print("error %s \n",g_dbus_error_get_remote_error(error));
 }

}

static void
bandwidth_name_appeared (  GDBusConnection *connection,
                           const gchar     *name,
                           const gchar     *name_owner,
                           gpointer         pUserData)
{
  //g_print("bandwidth_name_appeared \n");

  /* Asynchronously creates a proxy for the D-Bus interface */
	shapwick_bandwidth_proxy_new (
                                                      connection,
                                                      G_DBUS_PROXY_FLAGS_NONE,
                                                      "Shapwick.Bandwidth",
                                                      "/Shapwick/Bandwidth",
                                                      NULL,
                                                      bandwidth_proxy_clb,
                                                      NULL );
}

static void
bandwidth_name_vanished( GDBusConnection *connection,
                         const gchar     *name,
                         gpointer         pUserData)
{
  g_print("bandwidth_name_vanished \n");
  if(NULL != pBandwidthProxy)
   g_object_unref(pBandwidthProxy);
}

void
system_resource_mgr_init (void)
{
  g_bus_watch_name (G_BUS_TYPE_SYSTEM,
      "Shapwick.Memory", G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
      memory_name_appeared,
      memory_name_vanished,
      NULL, NULL);

  g_bus_watch_name (G_BUS_TYPE_SYSTEM,
      "Shapwick.Bandwidth",
      G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
      bandwidth_name_appeared,
      bandwidth_name_vanished,
      NULL, NULL);
}

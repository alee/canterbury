/*
 * canterbury-init: initialize system state during boot
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "apparmor.h"
#include "util.h"

static GOptionEntry entries[] = { { NULL } };

static gboolean
populate_apparmor_ubercache (GError **error)
{
  g_autoptr (CbyAppArmorStore) apparmor = NULL;

  apparmor = _cby_apparmor_store_open (CBY_PATH_SYSTEM_EXTENSIONS "/apparmor.d",
                                       error);

  if (apparmor == NULL)
    return FALSE;

  if (!_cby_apparmor_store_invalidate_ubercache (apparmor, error))
    return FALSE;

  return _cby_apparmor_store_ensure_and_load_ubercache (apparmor, error);
}

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  gboolean failed = FALSE;

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- update system-wide caches");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return 2;
    }

  if (argc > 1)
    {
      g_printerr ("%s: too many arguments\n", g_get_prgname ());
      return 2;
    }

  if (!populate_apparmor_ubercache (&error))
    {
      g_printerr ("%s: %s\n", g_get_prgname (), error->message);
      g_clear_error (&error);
      failed = TRUE;
    }

  if (failed)
    return 1;
  else
    return 0;
}

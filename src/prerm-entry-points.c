/*
 * canterbury-prerm: deconfigure an app-bundle before unmounting it
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "util.h"

static GOptionEntry entries[] = { { NULL } };

static const guint supported_icon_sizes[] = {
  8, 16, 22, 24, 32, 36, 42, 48, 64, 72, 96, 128, 192, 256, 512
};

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GDir) dir = NULL;
  g_autoptr (GFile) bundle_file = NULL;
  g_autofree gchar *bundle_prefix = NULL;
  g_autofree gchar *extensions_apps_path = NULL;
  g_autofree gchar *extensions_icons_path = NULL;
  gint error_exit_status = 1;
  const gchar *bundle_id;
  const gchar *name;

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- deconfigure an application bundle");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      error_exit_status = 2;
      goto error;
    }

  if (argc < 2)
    {
      g_printerr ("%s: a bundle to be removed is required\n", g_get_prgname ());
      return 2;
    }

  if (argc > 2)
    {
      g_printerr ("%s: too many arguments\n", g_get_prgname ());
      return 2;
    }

  bundle_id = argv[1];

  if (!cby_is_bundle_id (bundle_id))
    {
      g_printerr ("%s: not a bundle ID: \"%s\"\n", g_get_prgname (),
                  bundle_id);
      return 2;
    }

  bundle_prefix = g_build_filename (CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id, NULL);
  bundle_file = g_file_new_for_path (bundle_prefix);

  extensions_apps_path = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS, "applications", NULL);

  INFO ("Searching for entry points symbolic links for app-bundle \"%s\" on \"%s\"", bundle_id, extensions_apps_path);
  dir = g_dir_open (extensions_apps_path, 0, &error);
  if (dir == NULL)
    {
      if (_cby_ignore_enoent (&error))
        {
          INFO ("No entry point found");
        }
      else
        {
          g_prefix_error (&error, "Unable to open \"%s\" for reading: ", extensions_apps_path);
          goto error;
        }
    }
  else
    {
      g_autoptr (GFile) extensions_apps_file = NULL;

      extensions_apps_file = g_file_new_for_path (extensions_apps_path);

      /* Iterate over CBY_PATH_SYSTEM_EXTENSIONS/applications searching
       * for entry points (.desktop) whose symlinks have
       * CBY_PATH_PREFIX_STORE_BUNDLE/<bundle_id> as prefix and
       * remove them accordingly.
       */
      for (name = g_dir_read_name (dir);
           name != NULL;
           name = g_dir_read_name (dir))
        {
          if (g_str_has_suffix (name, ".desktop"))
            {
              g_autoptr (GFile) link_file = NULL;
              g_autoptr (GFileInfo) link_file_info = NULL;
              g_autoptr (GFile) target_file = NULL;
              const gchar *target = NULL;

              INFO ("Checking if entry point \"%s/%s\" belongs to app-bundle \"%s\"",
                    extensions_apps_path, name, bundle_id);

              link_file = g_file_get_child (extensions_apps_file, name);
              link_file_info = g_file_query_info (link_file, G_FILE_ATTRIBUTE_STANDARD_SYMLINK_TARGET,
                                                  G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, NULL, &error);
              if (!link_file_info)
                {
                  WARNING ("Unable to read info from entry point \"%s/%s\" - ignoring entry point: %s",
                           extensions_apps_path, name, error->message);
                  g_clear_error (&error);
                  continue;
                }

              target = g_file_info_get_symlink_target (link_file_info);
              if (!target)
                {
                  WARNING ("Entry point \"%s/%s\" is not a symbolic link - ignoring entry point",
                           extensions_apps_path, name);
                  continue;
                }

              target_file = g_file_resolve_relative_path (extensions_apps_file, target);
              if (g_file_has_prefix (target_file, bundle_file))
                {
                  INFO ("Removing entry point symbolic link \"%s/%s\" belonging to app-bundle \"%s\"",
                        extensions_apps_path, name, bundle_id);
                  if (!g_file_delete (link_file, NULL, &error))
                    {
                      g_prefix_error (&error,
                                      "Unable to remove entry point symbolic link \"%s/%s\" → \"%s\": ",
                                      extensions_apps_path, name, target);
                      goto error;
                    }
                }
            }
        }
    }

  g_clear_pointer (&dir, g_dir_close);

  extensions_icons_path = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS, "icons", NULL);

  INFO ("Searching for icon symbolic links for app-bundle \"%s\" on \"%s\"", bundle_id, extensions_icons_path);
  dir = g_dir_open (extensions_icons_path, 0, &error);
  if (dir == NULL)
    {
      if (_cby_ignore_enoent (&error))
        {
          INFO ("No entry point found");
        }
      else
        {
          g_prefix_error (&error, "Unable to open \"%s\" for reading: ", extensions_apps_path);
          goto error;
        }
    }
  else
    {
      /* Iterate over CBY_PATH_SYSTEM_EXTENSIONS/icons
       * searching for icons whose symlinks have
       * CBY_PATH_PREFIX_STORE_BUNDLE/<bundle_id> as prefix and
       * remove them accordingly.
       */
      for (name = g_dir_read_name (dir);
           name != NULL;
           name = g_dir_read_name (dir))
        {
          g_autofree gchar *path = NULL;
          const gchar *theme_name;
          GStatBuf stat_buf;

          path = g_build_filename (extensions_icons_path, name, NULL);
          if (g_lstat (path, &stat_buf) == -1 || !S_ISDIR (stat_buf.st_mode))
            continue;

          theme_name = name;

          for (guint i = 0; i < G_N_ELEMENTS (supported_icon_sizes); ++i)
            {
              g_autofree gchar *icon_size = NULL;
              g_autofree gchar *extensions_icon_theme_apps_path = NULL;
              g_autoptr (GDir) extensions_icon_theme_apps_dir = NULL;
              g_autoptr (GFile) extensions_icon_theme_apps_file = NULL;
              const gchar *icon_name;

              icon_size = g_strdup_printf ("%dx%d", supported_icon_sizes[i],
                                           supported_icon_sizes[i]);

              extensions_icon_theme_apps_path = g_build_filename (extensions_icons_path, theme_name,
                                                                  icon_size, "apps", NULL);
              extensions_icon_theme_apps_file = g_file_new_for_path (extensions_icon_theme_apps_path);

              extensions_icon_theme_apps_dir = g_dir_open (extensions_icon_theme_apps_path, 0, &error);
              if (extensions_icon_theme_apps_dir == NULL)
                {
                  if (_cby_ignore_enoent (&error))
                    {
                      INFO ("No icon found on \"%s\"", extensions_icon_theme_apps_path);
                      continue;
                    }
                  else
                    {
                      g_prefix_error (&error, "Unable to open \"%s\" for reading: ", extensions_apps_path);
                      goto error;
                    }
                }

              for (icon_name = g_dir_read_name (extensions_icon_theme_apps_dir);
                   icon_name != NULL;
                   icon_name = g_dir_read_name (extensions_icon_theme_apps_dir))
                {
                  g_autoptr (GFile) link_file = NULL;
                  g_autoptr (GFileInfo) link_file_info = NULL;
                  g_autoptr (GFile) target_file = NULL;
                  const gchar *target = NULL;

                  INFO ("Checking if icon \"%s/%s\" belongs to app-bundle \"%s\"",
                        extensions_icon_theme_apps_path, icon_name, bundle_id);

                  link_file = g_file_get_child (extensions_icon_theme_apps_file, icon_name);
                  link_file_info = g_file_query_info (link_file, G_FILE_ATTRIBUTE_STANDARD_SYMLINK_TARGET,
                                                      G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, NULL, &error);
                  if (!link_file_info)
                    {
                      WARNING ("Unable to read info from icon \"%s/%s\" - ignoring icon: %s",
                               extensions_icon_theme_apps_path, icon_name, error->message);
                      g_clear_error (&error);
                      continue;
                    }

                  target = g_file_info_get_symlink_target (link_file_info);
                  if (!target)
                    {
                      WARNING ("Icon \"%s/%s\" is not a symbolic link - ignoring icon",
                               extensions_apps_path, icon_name);
                      continue;
                    }

                  target_file = g_file_resolve_relative_path (extensions_icon_theme_apps_file, target);
                  if (g_file_has_prefix (target_file, bundle_file))
                    {
                      INFO ("Removing icon symbolic link \"%s/%s\" belonging to app-bundle \"%s\"",
                            extensions_icon_theme_apps_path, icon_name, bundle_id);
                      if (!g_file_delete (link_file, NULL, &error))
                        {
                          g_prefix_error (&error,
                                          "Unable to remove icon symbolic link \"%s/%s\" → \"%s\": ",
                                          extensions_icon_theme_apps_path, icon_name, target);
                          goto error;
                        }
                    }
                }
            }
        }
    }

  g_assert (error == NULL);
  return 0;

error:
  g_printerr ("%s\n", error->message);
  return error_exit_status;
}

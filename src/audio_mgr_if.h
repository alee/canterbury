/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __AUDIO_MGR_IF__
#define __AUDIO_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

void init_audio_mgr (GDBusConnection *connection);

void
automotive_proxy_set_datapool_type_bool	(	const gchar* method_name,	const gboolean DP_Value_bool);

void audio_mgr_update_with_new_audio_source (const gchar *app_name,
                                             guint audio_resource_type);
void audio_mgr_update_with_audio_channel_name (const gchar *app_name,
                                               const gchar *audio_channel_name);
void audio_mgr_update_with_removed_audio_source (const gchar *app_name);

gchar *audio_mgr_get_current_normal_audio_source (void);
gchar *audio_mgr_get_current_active_audio_source (void);

G_END_DECLS

#endif //__AUDIO_MGR_IF__

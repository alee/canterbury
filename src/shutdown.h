/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SHUTDOWN_H__
#define __SHUTDOWN_H__

#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/**
 * CanterburyShutdownMode:
 * @CANTERBURY_SHUTDOWN_NONE: Do not shut down; a placeholder value.
 * @CANTERBURY_SHUTDOWN_REBOOT: Reboot the machine.
 * @CANTERBURY_SHUTDOWN_POWER_OFF: Power off the machine.
 *
 * Modes for shutting down the machine. This roughly maps to the
 * [logind API](http://www.freedesktop.org/wiki/Software/systemd/logind/#themanagerobject).
 *
 * Since: 0.2.99
 */
typedef enum {
  CANTERBURY_SHUTDOWN_NONE,
  CANTERBURY_SHUTDOWN_REBOOT,
  CANTERBURY_SHUTDOWN_POWER_OFF,
} CanterburyShutdownMode;

void
canterbury_shutdown_async (CanterburyShutdownMode   mode,
                           gboolean                 query_user,
                           GCancellable            *cancellable,
                           GAsyncReadyCallback      callback,
                           gpointer                 user_data);
gboolean
canterbury_shutdown_finish (GAsyncResult           *result,
                            GError                **error);

G_END_DECLS

#endif /* !__SHUTDOWN_H__ */

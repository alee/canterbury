/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "app-mgr"
#include "app_mgr_if.h"
#include "app_mgr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gio/gio.h>
#include <gio/gdesktopappinfo.h>

#include "activity_mgr_if.h"
#include "app_launch_mgr_if.h"
#include "audio_mgr_if.h"
#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include <canterbury/gdbus/canterbury.h>
#include "canterbury/platform/entry-point-index-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "canterbury/messages-internal.h"
#include "hardkeys_mgr_if.h"
#include "last_user_mode_if.h"
#include "launch_mgr_if.h"
#include "launcher_mgr_if.h"
#include "win_mgr_if.h"

extern AppMgr appManagerObj;
static gboolean register_my_app_clb(CanterburyAppManager *object,
                                    GDBusMethodInvocation *invocation,
                                    const gchar *arg_app_name,
                                    guint64 arg_window_id,
                                    gpointer pUserData);

static void
app_mgr_add_application (CbyEntryPoint *entry_point)
{
  const gchar *id;
  g_return_if_fail (entry_point != NULL);
  id = cby_entry_point_get_id (entry_point);

  if (_cby_entry_point_get_audio_channel (entry_point) != NULL)
    {
      audio_mgr_update_with_new_audio_source (id,
                                              _cby_entry_point_get_audio_resource_type (entry_point));
      audio_mgr_update_with_audio_channel_name (id,
                                                _cby_entry_point_get_audio_channel (entry_point));
    }
  if (_cby_entry_point_get_executable_type (entry_point) == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE)
    {
      v_launch_agent_or_service (entry_point);
    }
}

static void
app_mgr_remove_application (CbyEntryPoint *entry_point)
{
  const gchar *id = cby_entry_point_get_id (entry_point);

  if (_cby_entry_point_get_executable_type (entry_point) == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE)
    {
      v_kill_running_agent (id);
      audio_mgr_update_with_removed_audio_source (id);
    }
  else
    {
      audio_mgr_update_with_removed_audio_source (id);
      launch_mgr_kill_the_current_running_application (entry_point);
      back_stack_remove_entry (entry_point);
    }
}

static gchar *
argv_to_string (const gchar * const *argv)
{
  guint i;
  GString *output = NULL;

  output = g_string_new ("");

  for (i = 0; argv != NULL && argv[i] != NULL; i++)
    {
      g_autofree gchar *quoted = g_shell_quote (argv[i]);

      if (i != 0)
        g_string_append_c (output, ' ');

      g_string_append (output, quoted);
    }

  return g_string_free (output, FALSE);
}

static void
app_mgr_launch_new_application_cb (GObject *source_object,
                                   GAsyncResult *result,
                                   gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (launch_mgr_launch_app_finish (result, &error))
    {
      g_task_return_boolean (task, TRUE);
    }
  else
    {
      g_task_return_error (task, g_steal_pointer (&error));
    }
}

void
app_mgr_launch_new_application_async (const gchar *entry_point_id,
                                      const gchar * const *uris,
                                      GVariant *platform_data,
                                      const gchar * const *prepend_args,
                                      const gchar* pLaunchedFrom,
                                      guint uinSpecialArgs,
                                      GCancellable *cancellable,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data)
{
    g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
    g_autoptr (CbyEntryPoint) entry_point = NULL;
    g_autoptr (GTask) task = NULL;
    g_autoptr (GVariant) platform_data_ref = NULL;
    g_autofree gchar *argument_list_string = NULL;
    LaunchFlags flags;

    if (platform_data != NULL)
      platform_data_ref = g_variant_ref_sink (platform_data);

    task = g_task_new (NULL, cancellable, callback, user_data);
    g_task_set_source_tag (task, app_mgr_launch_new_application_async);

    argument_list_string = argv_to_string (prepend_args);
    DEBUG ("%s %s (launched from %s, special args: %u)",
           entry_point_id, argument_list_string, pLaunchedFrom, uinSpecialArgs);

    entry_point_index = launch_mgr_get_entry_point_index ();
    entry_point = cby_entry_point_index_get_by_id (entry_point_index,
                                                   entry_point_id);

    if (entry_point != NULL)
    {
      flags = (LAUNCH_FLAGS_START_TIMER_IF_EXISTING |
               LAUNCH_FLAGS_EMIT_NEW_APP_STATE_IF_EXISTING |
               LAUNCH_FLAGS_CHECK_EXISTING_GUI |
               LAUNCH_FLAGS_CHECK_EXISTING_DAEMON |
               LAUNCH_FLAGS_SET_ACTIVE_TITLE |
               LAUNCH_FLAGS_ARGV_REPLACE);

      /* FIXME: This is checking that pLaunchedFrom >= CANTERBURY_BACK_KEY,
       * which is probably not what was intended; it was probably meant
       * to be pLaunchedFrom != CANTERBURY_BACK_KEY. But this is what
       * Canterbury has done since the initial code-drop, so keeping it
       * for now. */
      if (g_strcmp0 (pLaunchedFrom, CANTERBURY_BACK_KEY) == 1)
        flags |= LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_NEW;

      /* FIXME: This is checking that pLaunchedFrom == CANTERBURY_BACK_KEY,
       * which seems wrong given the other check for CANTERBURY_BACK_KEY.
       * But, again, this is what Canterbury has done since the initial
       * code-drop, so keeping it for now. */
      if (g_strcmp0 (pLaunchedFrom , CANTERBURY_BACK_KEY) == 0)
        flags |= LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_EXISTING;

      launch_mgr_launch_app_async (entry_point,
                                   pLaunchedFrom,
                                   uinSpecialArgs,
                                   uris,
                                   platform_data,
                                   prepend_args,
                                   flags,
                                   NULL,
                                   app_mgr_launch_new_application_cb,
                                   g_object_ref (task));
    }
    else
    {
      g_autoptr (GError) error = NULL;

      g_set_error (&error, CBY_ERROR, CBY_ERROR_LAUNCH_FAILED,
                   "No such entry point");
      g_task_return_error (task, g_steal_pointer (&error));
    }
}

gboolean
app_mgr_launch_new_application_finish (GAsyncResult *result,
                                       GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, NULL), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  app_mgr_launch_new_application_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void set_current_active_audio_source (const gchar *pAppName)
{
  app_mgr_debug("set_current_active_audio_source %s \n" , pAppName);
  canterbury_app_manager_set_current_active_audio_source( appManagerObj.pAppMgrDbusObj, pAppName );
}

void
app_mgr_set_app_on_top (CbyEntryPoint *entry_point)
{
  const gchar *id;
  CanterburyBandwidthPriority priority;

  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));
  g_return_if_fail (appManagerObj.pAppMgrDbusObj != NULL);

  id = cby_entry_point_get_id (entry_point);
  canterbury_app_manager_set_current_active_app (appManagerObj.pAppMgrDbusObj,
                                                 id);

  DEBUG ("%s", id);

  priority = _cby_entry_point_get_internet_bandwidth_priority (entry_point);
  DEBUG ("Internet bandwidth priority: %d", priority);

  if (priority == CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN )
    {
      canterbury_app_manager_set_current_active_app_requires_internet( appManagerObj.pAppMgrDbusObj, FALSE );
    }
  else
    {
      canterbury_app_manager_set_current_active_app_requires_internet( appManagerObj.pAppMgrDbusObj, TRUE );
    }
}


static gboolean register_my_app_clb(CanterburyAppManager  *object,
                                    GDBusMethodInvocation   *invocation,
                                    const gchar             *entry_point_id,
                                    guint64                  arg_window_id,
                                    gpointer                 pUserData)
{
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  LaunchNewAppInfo *info;
  const gchar *const *argv = NULL;

  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_point = cby_entry_point_index_get_by_id (entry_point_index,
                                                 entry_point_id);

  if (entry_point != NULL &&
      _cby_entry_point_get_dbus_activatable (entry_point))
    {
      /* Legacy-free entry points shouldn't be calling this, just receiving
       * Activate() and replying to it (which has essentially the same
       * side-effects in Canterbury). */
      g_dbus_method_invocation_return_error (invocation,
                                             CBY_ERROR,
                                             CBY_ERROR_INVALID_ARGUMENT,
                                             "DBusActivatable entry points "
                                             "do not call RegisterMyApp()");
      return TRUE; /* handled */
    }

  canterbury_app_manager_complete_register_my_app(object,invocation);

  info = launch_mgr_find_running_app (entry_point_id);

  if (info != NULL)
    argv = (const gchar * const *) info->legacy_arguments;

  if (entry_point != NULL)
    activity_mgr_entry_point_activated (entry_point);

  emit_new_app_state (entry_point_id, CANTERBURY_APP_STATE_START, argv);
  return TRUE;
}

/*
 * @entry_point_id: Entry point identifier
 * @state: New state of the given entry point
 * @arguments: New argv[1], argv[2]...
 *
 * Tell the given entry point to change its state. In the case of startup-like
 * states, it is to behave as though it had been passed the given command-line
 * arguments, not counting argv[0].
 */
void
emit_new_app_state (const gchar *entry_point_id,
                    CanterburyAppState state,
                    const gchar * const *arguments)
{
  GVariantBuilder gvb;
  guint i;

  DEBUG ("%s", entry_point_id);

  g_variant_builder_init (&gvb, G_VARIANT_TYPE ("a{ss}"));

  for (i = 0;
       arguments != NULL && arguments[i] != NULL && arguments[i + 1] != NULL;
       i += 2)
    {
      g_variant_builder_add (&gvb, "{ss}", arguments[i], arguments[i + 1]);
      DEBUG ("\"%s\": \"%s\"", arguments[i], arguments[i + 1]);
    }

  if (arguments != NULL && arguments[i] != NULL)
    {
      WARNING ("Unpaired argument \"%s\" cannot be represented",
               arguments[i]);
    }

  /* Sinks floating reference returned by g_variant_builder_end() */
  canterbury_app_manager_emit_new_app_state (appManagerObj.pAppMgrDbusObj,
                                             entry_point_id, state,
                                             g_variant_builder_end (&gvb));
}

static void
entry_point_index_added_cb (CbyEntryPointIndex *entry_point_index,
                            CbyEntryPoint *entry_point,
                            gpointer user_data)
{
  app_mgr_add_application (entry_point);
}

static void
entry_point_index_removed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  app_mgr_remove_application (entry_point);
}

static void
entry_point_index_changed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  /* TODO: improve it, update instead of removing/adding */
  app_mgr_remove_application (entry_point);
  app_mgr_add_application (entry_point);
}

gboolean
app_mgr_init (GDBusConnection *session_bus,
              CbyEntryPointIndex *entry_point_index,
              CbyServiceManager *service_manager,
              GError **error)
{
  activity_mgr_init (session_bus);

  appManagerObj.pAppMgrDbusObj = (CanterburyAppManager *)canterbury_app_manager_skeleton_new();

  g_signal_connect (appManagerObj.pAppMgrDbusObj,
                    "handle-register-my-app",
                    G_CALLBACK (register_my_app_clb),
                    NULL);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (appManagerObj.pAppMgrDbusObj),
                                         session_bus,
                                         "/org/apertis/Canterbury/AppManager",
                                         error))
    return FALSE;

  launch_mgr_init (service_manager, entry_point_index);

  init_launcher (session_bus);

  init_hardkey (session_bus);

  init_audio_mgr (session_bus);

  init_app_launch (session_bus);

  g_signal_connect (entry_point_index,
                    "added",
                    G_CALLBACK (entry_point_index_added_cb),
                    NULL);
  g_signal_connect (entry_point_index,
                    "removed",
                    G_CALLBACK (entry_point_index_removed_cb),
                    NULL);
  g_signal_connect (entry_point_index,
                    "changed",
                    G_CALLBACK (entry_point_index_changed_cb),
                    NULL);

  return TRUE;
}

void
hide_global_popups (void)
{
  canterbury_app_manager_emit_hide_global_popups(appManagerObj.pAppMgrDbusObj);
}

void
v_set_system_shutdown_signal(gboolean bSignal)
{
  appManagerObj.bSystemShutdownSignal = bSignal;
}

gboolean
v_get_system_shutdown_signal (void)
{
  return appManagerObj.bSystemShutdownSignal;
}

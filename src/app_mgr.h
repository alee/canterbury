/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __APP_MGR__
#define __APP_MGR__

#include <glib.h>

#include "canterbury/gdbus/canterbury.h"
#include "canterbury/platform/entry-point-index.h"
#include "per-user-bridge.h"
#include "service-manager.h"

G_BEGIN_DECLS

/* Macros */

#define APP_MANAGER_CONFIG_SCHEMA "org.apertis.Canterbury.Config"
#define CONFIG_LAST_USER_MODE_KEY	"enable-last-user-mode"
#define MOUNT_MGR_CONFIG_SCHEMA "org.apertis.Ribchester.ApplicationsMntMgr"

/*
* include all structure declarations here
*/

typedef struct _AppMgr AppMgr;

struct _AppMgr
{
  GHashTable*             pWindowIdHashTable;
  CanterburyAppManager* pAppMgrDbusObj;
  gboolean                bSystemShutdownSignal;
  GSettings*              pSchema;
};

/*
* include all function declarations here
*/

gboolean app_mgr_init (GDBusConnection *session_bus,
                       CbyEntryPointIndex *entry_point_index,
                       CbyServiceManager *service_manager,
                       GError **error);

G_END_DECLS

#endif //__APP_MGR__

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "launch-mgr-db"
#include "launch_mgr_if.h"
#include "launch_mgr.h"

#include <gio/gio.h>

#include "canterbury/gdbus/enumtypes.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"


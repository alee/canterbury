/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "app-mgr"
#include "shutdown.h"

#include <glib.h>
#include <gio/gio.h>

static void
shutdown_async_cb1 (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data);
static void
shutdown_async_cb2 (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data);

typedef struct {
  CanterburyShutdownMode mode;
  gboolean query_user;
} ShutdownData;

static ShutdownData *
shutdown_data_new (CanterburyShutdownMode mode,
                   gboolean               query_user)
{
  ShutdownData *data;

  data = g_slice_new0 (ShutdownData);
  data->mode = mode;
  data->query_user = query_user;

  return data;
}

static void
shutdown_data_free (ShutdownData *data)
{
  g_slice_free (ShutdownData, data);
}

/**
 * canterbury_shutdown_async:
 * @mode: shutdown mode to use
 * @query_user: whether to query the user for authorisation to shut down if
 *   needed; if %FALSE, and authorisation is needed, the shutdown operation
 *   will fail
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke once the asynchronous operation is complete
 * @error: return location for a #GError, or %NULL
 *
 * Send a message to systemd to shut down or reboot the system immediately. No
 * shutdown wall message will be sent. This will query polkit for permission to
 * shut down the system. If user authorisation is required, and @query_user is
 * %TRUE, this will open a polkit agent (typically a UI) to query the user for
 * authorisation. Otherwise it will fail.
 *
 * It is the responsibility of the caller to list and check all
 * [http://www.freedesktop.org/wiki/Software/systemd/inhibit/](shutdown inhibitors)
 * before calling this method.
 *
 * Since: 0.2.99
 */
void
canterbury_shutdown_async (CanterburyShutdownMode  mode,
                           gboolean                query_user,
                           GCancellable           *cancellable,
                           GAsyncReadyCallback     callback,
                           gpointer                user_data)
{
  GTask *task = NULL;  /* owned */

  g_return_if_fail (mode != CANTERBURY_SHUTDOWN_NONE);
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_task_data (task, shutdown_data_new (mode, query_user),
                        (GDestroyNotify) shutdown_data_free);
  g_bus_get (G_BUS_TYPE_SYSTEM, cancellable,
             shutdown_async_cb1, g_object_ref (task));
  g_object_unref (task);
}

static void
shutdown_async_cb1 (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  GTask *task = NULL;  /* owned */
  GDBusConnection *connection = NULL;  /* owned */
  ShutdownData *data;  /* unowned */
  const gchar *method_name;
  GError *error = NULL;

  task = G_TASK (user_data);
  data = g_task_get_task_data (task);

  connection = g_bus_get_finish (result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      goto done;
    }

  switch (data->mode)
    {
    case CANTERBURY_SHUTDOWN_REBOOT:
      method_name = "Reboot";
      break;
    case CANTERBURY_SHUTDOWN_POWER_OFF:
      method_name = "PowerOff";
      break;
    case CANTERBURY_SHUTDOWN_NONE:
    default:
      g_assert_not_reached ();
    }

  /* Reference:
   * http://www.freedesktop.org/wiki/Software/systemd/logind/#themanagerobject.
   *
   * Checks one of the following polkit actions:
   *  • org.freedesktop.login1.power-off
   *  • org.freedesktop.login1.power-off-multiple-sessions
   *  • org.freedesktop.login1.power-off-ignore-inhibit
   *  • org.freedesktop.login1.reboot
   *  • org.freedesktop.login1.reboot-multiple-sessions
   *  • org.freedesktop.login1.reboot-ignore-inhibit
   */
  g_dbus_connection_call (connection,
                          "org.freedesktop.login1",
                          "/org/freedesktop/login1",
                          "org.freedesktop.login1.Manager",
                          method_name,
                          g_variant_new ("(b)", data->query_user),
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          shutdown_async_cb2,
                          g_object_ref (task));

done:
  g_clear_object (&connection);
  g_clear_object (&task);
}

static void
shutdown_async_cb2 (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  GTask *task = NULL;  /* owned */
  GDBusConnection *connection;  /* unowned */
  GVariant *reply = NULL;  /* owned */
  GError *error = NULL;

  connection = G_DBUS_CONNECTION (source_object);
  task = G_TASK (user_data);

  reply = g_dbus_connection_call_finish (connection, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
    }
  else
    {
      g_task_return_boolean (task, TRUE);
    }

  g_clear_pointer (&reply, g_variant_unref);
  g_clear_object (&task);
}

/**
 * canterbury_shutdown_finish:
 * @result: result of the asynchronous shutdown operation
 * @error: return location for a #GError
 *
 * Finish a shutdown operation started with canterbury_shutdown_async().
 *
 * Returns: %TRUE on success, %FALSE otherwise
 *
 * Since: 0.2.99
 */
gboolean
canterbury_shutdown_finish (GAsyncResult           *result,
                            GError                **error)
{
  g_return_val_if_fail (g_task_is_valid (result, NULL), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

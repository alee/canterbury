/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "launcher-mgr"
#include "launcher_mgr_if.h"

#include <gio/gio.h>
#include <systemd/sd-daemon.h>

#include "activity_mgr_if.h"
#include "app_mgr_if.h"
#include "canterbury/gdbus/canterbury.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "hardkeys_mgr_if.h"
#include "launch_mgr_if.h"
#include "win_mgr_if.h"

CanterburyLauncher* launcher_obj = NULL;

void
set_launcher_menu_entry_to_focus (CbyEntryPoint *entry_point)
{
  const gchar *label;

  label = _cby_entry_point_get_mangled_display_name (entry_point);

  if (label != NULL)
    canterbury_launcher_set_current_active_menu_entry (launcher_obj, label);
}

void inform_launcher_about_fullscreen_animation( gboolean fullscreen_status , guint8 delay_in_sec , guint16 delay_in_msec )
{
  canterbury_launcher_emit_full_screen_animation( launcher_obj ,
                                                          fullscreen_status ,
                                                          delay_in_sec,
                                                          delay_in_msec );
}

void
init_launcher (GDBusConnection *connection)
{
  DEBUG ("enter");

  launcher_obj = canterbury_launcher_skeleton_new();

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (launcher_obj),
                                         connection,
                                         "/org/apertis/Canterbury/Launcher",
                                         NULL))
  {
    launcher_mgr_debug("export error \n");
  }
}

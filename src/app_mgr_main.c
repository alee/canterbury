/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "app-mgr"
#include "app_mgr_if.h"
#include "app_mgr.h"
#include "audio_mgr.h"

#include <errno.h>
#include <locale.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>
#include <glib-unix.h>
#include <systemd/sd-daemon.h>

#include "app_launch_mgr_if.h"
#include "audio_mgr_if.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/platform/component-index.h"
#include "canterbury/platform/entry-point-index.h"
#include "canterbury/platform/environment.h"
#include "canterbury/messages-internal.h"
#include "hardkeys_mgr_if.h"
#include "last_user_mode_if.h"
#include "launch_mgr_if.h"
#include "launcher_mgr_if.h"
#include "shutdown.h"
#include "system_res_mgr_if.h"
#include "win_mgr_if.h"
#include "util.h"

/* Function prototypes */
static gboolean kill_all_services(gpointer data);
static gboolean shutdown_the_system(gpointer data);

AppMgr appManagerObj;

static gboolean
app_manager_termination_handler (gpointer user_data)
{
		gint signum = 0;
		guint cnt;
		LaunchNewAppInfo *pAppInfo = NULL;
		guint uin_RunningAppsListLen;

		if (user_data == NULL)
		{
			INFO ("Terminating app manager");
		}
		else
		{
			signum = GPOINTER_TO_INT (user_data);

			INFO ("Terminating app manager due to signal #%d (%s)",
				signum, strsignal (signum));
		}

		  v_set_system_shutdown_signal(TRUE);

		  //send wm display off signal
		  v_display_off(TRUE);

		  //send off signals to all applications

		uin_RunningAppsListLen =  get_running_app_info_stack_length();

		  for( cnt = 0 ; cnt< uin_RunningAppsListLen ; cnt++)
		  {
			pAppInfo = get_running_app_info_at_position( cnt );

			DEBUG ("Killing \"%s\"", cby_entry_point_get_id (pAppInfo->entry_point));

            if (!launch_mgr_stop_running_app (pAppInfo->entry_point, pAppInfo->pid))
              {
                WARNING ("Failed to stop entry point \"%s\"",
                         cby_entry_point_get_id (pAppInfo->entry_point));
              }
		  }
	/* Only for SDK, the services and apps should be killed.
	 * In case of target only app-manager should get killed and
	 * services continue */
	if (!_cby_is_target ())
	{
		g_timeout_add_seconds (2, kill_all_services, NULL);
	}
	else
	{
		if( (0 == signum))// if function called NOT because of "kill" then RESTART app_manager
		{
			//gchar* shutdown = g_strdup("sudo reboot");
			//if( system( shutdown ) == -1 )
			//{
				//g_printerr( "SHUTDOWN MGR: shutdown unsuccessful \n" );
			//}
		}
		else
		exit(0);
	}

	return G_SOURCE_REMOVE;
}

static gboolean kill_all_services(gpointer data)
{
  //send off signals to all agents and services
  guint cnt;
  LaunchNewAppInfo *pAppInfo = NULL;
  guint uin_RunningDaemonsListLen =  get_running_daemon_info_stack_length( );
  for( cnt = 0 ; cnt< uin_RunningDaemonsListLen ; cnt++)
  {
    pAppInfo = get_running_daemon_info_at_position( cnt );

    if (!launch_mgr_stop_running_app (pAppInfo->entry_point, pAppInfo->pid))
      {
        WARNING ("Failed to stop entry point \"%s\"",
                 cby_entry_point_get_id (pAppInfo->entry_point));
      }
  }

  DEBUG ("Will shut down soon");

  g_timeout_add_seconds(2 , (GSourceFunc)shutdown_the_system , NULL);
  return FALSE;
}

static void
shutdown_cb (GObject      *source_object,
             GAsyncResult *result,
             gpointer      user_data)
{
  GError *error = NULL;

  canterbury_shutdown_finish (result, &error);

  if (error != NULL)
    {
      g_printerr ("SHUTDOWN MGR: shutdown unsuccessful: %s\n",
                  error->message);
      g_error_free (error);
    }
}

static gboolean shutdown_the_system(gpointer data)
{
    //save the LUM state
  if(b_get_last_user_mode_status())
    {
      DEBUG ("Saving last-user-mode status");
      last_user_mode_save_status();
    }

  if (!_cby_is_target ())
    {
      /* If we are in the "simulator" environment, just exit. */
      DEBUG ("Simulator environment detected. Exiting Canterbury but not "
          "rebooting the system");
      exit(0);
    }
  else
    {
      /* If we are running on real hardware, really shut down. */
      DEBUG ("Requesting reboot");
      canterbury_shutdown_async (CANTERBURY_SHUTDOWN_REBOOT, TRUE, NULL,
                                 shutdown_cb, NULL);
    }
    return TRUE;
}

static void app_manager_register_exit_functionalities(void)
{
  g_unix_signal_add (SIGINT, app_manager_termination_handler,
                     GINT_TO_POINTER (SIGINT));
  g_unix_signal_add (SIGHUP, app_manager_termination_handler,
                     GINT_TO_POINTER (SIGHUP));
  g_unix_signal_add (SIGTERM, app_manager_termination_handler,
                     GINT_TO_POINTER (SIGTERM));
}

gboolean
b_get_last_user_mode_status (void)
{
  return g_settings_get_boolean(appManagerObj.pSchema , "enable-last-user-mode");
}

static void
init_completed_cb (GObject *nil G_GNUC_UNUSED,
                   GAsyncResult *result,
                   gpointer user_data)
{
  GTask *task = G_TASK (result);

  if (g_task_had_error (task))
    g_main_loop_quit (user_data);
  else
    DEBUG ("Initialization completed");
}

static void
on_app_mgr_name_acquired (GDBusConnection *connection,
                          const gchar *name,
                          gpointer user_data)
{
  GTask *task = user_data;
  int ret;

  DEBUG ("Acquired name %s", name);
  ret = sd_notify (0, "READY=1");

  if (ret > 0)
    {
      DEBUG ("notified systemd that we are ready");
      g_task_return_boolean (task, TRUE);
    }
  else if (ret == 0)
    {
      DEBUG ("ready, but not started by systemd");
      g_task_return_boolean (task, TRUE);
    }
  else
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Unable to notify systemd that we are ready: %s",
                               strerror (-ret));
    }
}

static void
on_app_mgr_name_lost (GDBusConnection *connection,
                      const gchar *name,
                      gpointer user_data)
{
  GTask *task = user_data;

  DEBUG ("Lost name %s", name);
  g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                           "Lost or could not acquire bus name \"%s\"", name);
}

static void
start_agents_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source);
  CbyEntryPointIndex *entry_point_index =
      _cby_service_manager_get_entry_point_index (service_manager);
  GDBusConnection *session_bus =
      _cby_service_manager_get_session_bus (service_manager);
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (!_cby_service_manager_start_agents_finish (service_manager, result,
                                                 &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  last_user_mode_init (entry_point_index);
  last_user_mode_launch_previous_app ();

  g_bus_own_name_on_connection (session_bus,
                                CBY_SESSION_BUS_NAME,
                                G_BUS_NAME_OWNER_FLAGS_NONE,
                                on_app_mgr_name_acquired,
                                on_app_mgr_name_lost,
                                g_object_ref (task), g_object_unref);
}

static void
reload_units_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source);
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (!_cby_service_manager_reload_units_finish (service_manager, result,
                                                 &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  _cby_service_manager_start_agents_async (service_manager, NULL,
                                           start_agents_cb,
                                           g_object_ref (task));
}

static void
win_mgr_init_cb (GObject *object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = user_data;
  CbyServiceManager *service_manager = g_task_get_task_data (task);

  /* Failure here typically occurs on a minimal image, so mostly ignore it,
   * and let other services come up anyway */
  if (!win_mgr_init_finish (result, &error))
    DEBUG ("Failed to initialize compositor integration, but continuing "
           "anyway: %s", error->message);

  /* Before initializing the last-user-mode code (which starts processes),
   * we must also ask systemd to reload units */
  _cby_service_manager_reload_units_async (service_manager, NULL,
                                           reload_units_cb,
                                           g_object_ref (task));
}

static void
per_user_bridge_cb (GObject *source,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = user_data;
  CbyServiceManager *service_manager = g_task_get_task_data (task);
  GDBusConnection *session_bus =
      _cby_service_manager_get_session_bus (service_manager);

  if (!cby_per_user_bridge_register_finish (CBY_PER_USER_BRIDGE (source),
                                            result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  /* Before we can start launching processes, we need to either know the
   * window manager is up, or work out that it isn't present (on a minimal
   * image) */
  win_mgr_init_async (session_bus, NULL, win_mgr_init_cb, g_object_ref (task));
}

gint main(gint argc, gchar *pArgv[])
{
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyPerUserBridge) per_user_bridge = NULL;
  g_autoptr (CbyServiceManager) service_manager = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GMainLoop) main_loop = NULL;
  g_autoptr (GTask) task = NULL;

  setlocale (LC_ALL, "");

  /* we're going to need this sooner or later - populate the cache */
  (void) _cby_is_target ();

  cby_init_environment ();

  /* Initialize all required variables */
  v_set_system_shutdown_signal(FALSE);
  appManagerObj.pAppMgrDbusObj = NULL;

   /* Initialize the application window id hash table
    *  Its a hash table with app name as the key and win id as the value
    */

  appManagerObj.pWindowIdHashTable = g_hash_table_new_full ( g_str_hash , g_str_equal , g_free , g_free) ;

  /* The app manager configuration file used to configure variant specific changes */
  appManagerObj.pSchema = g_settings_new(APP_MANAGER_CONFIG_SCHEMA);
  g_print("last user mode %d \n", g_settings_get_boolean(appManagerObj.pSchema , CONFIG_LAST_USER_MODE_KEY) );

  /* Canterbury cannot do anything useful until it has connected to the
   * session bus, so there's no point in doing this asynchronously. */
  session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);

  if (session_bus == NULL)
    {
      CRITICAL ("Unable to connect to D-Bus: %s", error->message);
      return 1;
    }

  component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE,
                                             &error);
  if (component_index == NULL)
    {
      CRITICAL ("Unable to set up component index: %s", error->message);
      return 1;
    }

  entry_point_index = cby_entry_point_index_new (component_index);
  service_manager = _cby_service_manager_new (session_bus, entry_point_index,
                                              &error);

  if (service_manager == NULL)
    {
      CRITICAL ("Unable to set up service manager: %s", error->message);
      return 1;
    }

  if (!app_mgr_init (session_bus, entry_point_index, service_manager, &error))
    {
      CRITICAL ("Unable to initialize app manager: %s", error->message);
      return 1;
    }

  main_loop = g_main_loop_new (NULL, FALSE);
  task = g_task_new (NULL, NULL, init_completed_cb, main_loop);
  /* Later steps will need the CbyServiceManager */
  g_task_set_task_data (task, g_object_ref (service_manager), g_object_unref);

  /* Register to receive TerminateBundle() before we start launching anything,
   * so that anything we launch, we are definitely able to terminate. */
  per_user_bridge = cby_per_user_bridge_new (service_manager);
  cby_per_user_bridge_register_async (per_user_bridge, per_user_bridge_cb,
                                      g_object_ref (task));

  /* Initialize system resource mgr client handler
   * the support for manageing of system resources is handled here
   */
	if(_cby_is_target ())
		system_resource_mgr_init();


 /* during system shutdown, if necessary handle all exit functionalities
   */
	app_manager_register_exit_functionalities();

  g_main_loop_run (main_loop);

  if (g_task_get_completed (task) &&
      !g_task_propagate_boolean (task, &error))
    {
      CRITICAL ("Unable to finish initialization: %s", error->message);
      return 1;
    }

  return 0;
}

void
v_restart_app_manager (void)
{
  app_manager_termination_handler (NULL);
}

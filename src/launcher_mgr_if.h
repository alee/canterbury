/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LAUNCHER_MGR_IF__
#define __LAUNCHER_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

#include "app_mgr_if.h"
#include "canterbury/platform/entry-point-internal.h"

G_BEGIN_DECLS

typedef enum
{
  APP_NOT_AVAILABLE = 0,
  APP_STARTED = 1,
  APP_ALREADY_STARTED = 2
} AppLaunchResult;

void init_launcher (GDBusConnection *connection);

void
inform_launcher_about_fullscreen_animation( gboolean fullscreen_status ,
                                            guint8 delay_in_sec ,
                                            guint16 delay_in_msec );

void launcher_insert_menu_entry (CbyServiceType type,
                                 const gchar * const *main_menu_label,
                                 const gchar * const *main_menu_icon,
                                 const gchar * const *quick_menu_label,
                                 const gchar * const *quick_menu_icon,
                                 const gchar * const *app_tile_thumbnail,
                                 guint new_app_status,
                                 gdouble progress);
void launcher_remove_menu_entry (CbyServiceType type ,
                                 const gchar * const *quick_menu_label);

void set_launcher_menu_entry_to_focus (CbyEntryPoint *entry_point);

G_END_DECLS

#endif //__LAUNCHER_MGR_IF__

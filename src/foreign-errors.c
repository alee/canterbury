/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "foreign-errors.h"

#include <gio/gio.h>

static const GDBusErrorEntry systemd_error_entries[] =
{
    { CBY_SYSTEMD_ERROR_NO_SUCH_UNIT, "org.freedesktop.systemd1.NoSuchUnit" },
};

G_STATIC_ASSERT (G_N_ELEMENTS (systemd_error_entries) == CBY_N_SYSTEMD_ERRORS);

GQuark
_cby_systemd_error_quark (void)
{
  static volatile gsize id = 0;

  g_dbus_error_register_error_domain ("cby-systemd-error-quark", &id,
                                      systemd_error_entries,
                                      G_N_ELEMENTS (systemd_error_entries));

  return (GQuark) id;
}

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LAST_USER_MODE_IF__
#define __LAST_USER_MODE_IF__

#include <glib.h>

#include "canterbury/platform/entry-point-index.h"

G_BEGIN_DECLS

/* last user mode mechanism */
void last_user_mode_init (CbyEntryPointIndex *entry_point_index);

void last_user_mode_save_status (void);

void last_user_mode_launch_previous_app (void);

G_END_DECLS

#endif //__LAST_USER_MODE_IF__

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "util.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gio/gio.h>

#include "canterbury/messages-internal.h"

/*
 * _cby_setenv_disable_services:
 *
 * Set environment variables to disable use of daemons for GIO, GSettings
 * and similar pluggable modules.
 *
 * This function has the same lack of thread-safety as g_setenv(), and
 * can only be called safely before the main program has had an opportunity
 * to start any threads.
 *
 * See:
 * https://bugs.freedesktop.org/show_bug.cgi?id=95487
 * https://bugzilla.gnome.org/show_bug.cgi?id=767182
 * https://bugzilla.gnome.org/show_bug.cgi?id=767183
 */
void
_cby_setenv_disable_services (void)
{
  g_setenv ("GIO_USE_VFS", "local", TRUE);
  g_setenv ("GIO_USE_VOLUME_MONITOR", "unix", TRUE);
  g_setenv ("GSETTINGS_BACKEND", "memory", TRUE);
  g_setenv ("GVFS_DISABLE_FUSE", "1", TRUE);
}

/* First fd that is not a special one (stdin, stdout or stderr) */
#define FIRST_NORMAL_FD (3)

/*
 * _cby_file_descriptor_copy:
 * @fd: a file descriptor
 *
 * Returns: a duplicate of fd
 */
CbyFileDescriptor
_cby_file_descriptor_copy (CbyFileDescriptor fd,
                           GError **error)
{
  CbyFileDescriptor ret;

  do
    ret = fcntl (fd, F_DUPFD_CLOEXEC, FIRST_NORMAL_FD);
  while (ret == -1 && errno == EINTR);

  if (ret < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to copy fd %d: %s", fd, g_strerror (saved_errno));
      return -1;
    }

  return ret;
}

/*
 * Clear *errorp if it represents "no such file or directory".
 */
gboolean
_cby_ignore_enoent (GError **errorp)
{
  if (errorp == NULL || *errorp == NULL)
    return FALSE;

  if (g_error_matches (*errorp, G_FILE_ERROR, G_FILE_ERROR_NOENT) ||
      g_error_matches (*errorp, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
    {
      g_clear_error (errorp);
      return TRUE;
    }

  return FALSE;
}

/*
 * _cby_open_dir_at:
 * @log_path: (nullable): something identifying @dirfd for debug/error
 *  messages, or %NULL to use a placeholder
 * @dirfd: an open file descriptor to a directory, or `AT_FDCWD`
 * @path: a path relative to @dirfd
 * @flags: flags affecting how path is interpreted
 * @error: used to raise a %CBY_ERROR if @basename cannot be opened
 *
 * Open @path relative to @dirfd, or raise %CBY_ERROR_NOT_FOUND if it
 * does not exist, or another %CBY_ERROR if it exists but cannot be opened.
 *
 * If @dirfd is `AT_FDCWD`, then @path is evaluated relative to the current
 * working directory, as it would be for `openat(2)`. See the `openat`
 * documentation for details.
 *
 * If the last component of @path is a symbolic link, the presence or
 * absence of %CBY_PATH_FLAGS_NOFOLLOW_SYMLINKS in @flags determines whether
 * the symbolic link is dereferenced. Symbolic links earlier in @path
 * are always followed.
 *
 * @log_path may be an absolute or relative path, or a placeholder
 * such as `<app-bundles directory>`. It will be used in debug/error messages
 * as though it was the path to @dirfd. This function never uses @log_path
 * for the actual file I/O.
 *
 * Returns: a file descriptor >= 0, or -1 on error
 */
CbyFileDescriptor
_cby_open_dir_at (const gchar *log_path,
                  CbyFileDescriptor dirfd,
                  const gchar *path,
                  CbyPathFlags flags,
                  GError **error)
{
  int openat_flags = O_RDONLY | O_DIRECTORY | O_NONBLOCK | O_CLOEXEC;
  const gchar *separator = G_DIR_SEPARATOR_S;
  CbyFileDescriptor fd;

  g_return_val_if_fail (dirfd == AT_FDCWD || dirfd >= 0, -1);
  g_return_val_if_fail (path != NULL, -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);
  g_return_val_if_fail (flags == (flags & CBY_PATH_FLAGS_NOFOLLOW_SYMLINKS),
                        -1);

  if (log_path == NULL)
    {
      if (dirfd == AT_FDCWD)
        log_path = ".";
      else
        log_path = "…";
    }

  if (g_path_is_absolute (path))
    {
      log_path = "";
      separator = "";
    }

  if (flags & CBY_PATH_FLAGS_NOFOLLOW_SYMLINKS)
    openat_flags |= O_NOFOLLOW;

  fd = openat (dirfd, path, openat_flags);

  if (fd < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Cannot open \"%s%s%s\": %s", log_path, separator, path,
                   g_strerror (saved_errno));

      return -1;
    }

  return fd;
}

/*
 * _cby_durable_symlink_at:
 * @target: the target of the new symbolic link
 * @log_parent_path: (nullable): the location of @parent_fd, or %NULL to use
 *  a placeholder; this is only used for debug/error messages
 * @parent_fd: an open fd in the directory to use as the parent (`AT_FDCWD`
 *  is not allowed here)
 * @link_name: symlink name, which must not contain path separators
 * @error: used to raise a %G_IO_ERROR on failure; in particular,
 *  %G_IO_ERROR_EXISTS means that @link_name already existed and was not
 *  overwritten.
 *
 * Create a symbolic link named @link_name relative to @parent_fd, pointing
 * to @target.
 *
 * If @link_name already exists, it will cause %G_IO_ERROR_EXISTS,
 * unless %CBY_PATH_FLAGS_OVERWRITE was given.
 *
 * The symbolic link creation is atomic, in the sense that if @link_name
 * exists, and the process crashes during execution of this function,
 * @link_name is guaranteed to either have its old contents or be a symlink
 * pointing to @target: it will not be deleted or have different contents.
 * Similarly, if @link_name does not exist, it will either be replaced with
 * a symbolic link to @target or continue to not exist.
 *
 * As much as possible, the same atomicity properties apply if the entire
 * system crashes or loses power: in particular, this is believed to be true
 * for btrfs.
 *
 * This function may create a file named @link_name + `.tmp~` in @parent_fd,
 * which might remain present if the process or system crashes. Do not rely
 * on the existence or contents of this file, which is not necessarily
 * created atomically. The only manipulation that should be done to this
 * file is deletion. Its name was chosen to make it likely that it avoids
 * collision with files in any limited character set, in particular those
 * based on bundle IDs or D-Bus names.
 *
 * Returns: %TRUE if successful
 */
gboolean
_cby_durable_symlink_at (const gchar *target,
                         const gchar *log_parent_path,
                         CbyFileDescriptor parent_fd,
                         const gchar *link_name,
                         CbyPathFlags flags,
                         GError **error)
{
  g_autofree gchar *tmp = NULL;

  g_return_val_if_fail (target != NULL, FALSE);
  g_return_val_if_fail (parent_fd != AT_FDCWD, FALSE);
  g_return_val_if_fail (parent_fd >= 0, FALSE);
  g_return_val_if_fail (link_name != NULL, FALSE);
  g_return_val_if_fail (link_name[0] != '\0', FALSE);
  /* The current implementation does not actually require that the link name
   * doesn't contain these, but if we need to call fsync (parent_fd) later,
   * it will make life simpler if this is true. */
  g_return_val_if_fail (strchr (link_name, '/') == NULL, FALSE);
  g_return_val_if_fail (strcmp (link_name, ".") != 0, FALSE);
  g_return_val_if_fail (strcmp (link_name, "..") != 0, FALSE);
  /* This is the only flag that makes sense here */
  g_return_val_if_fail (flags == (flags & CBY_PATH_FLAGS_OVERWRITE), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (log_parent_path == NULL)
    log_parent_path = "…";

  tmp = g_strconcat (link_name, ".tmp~", NULL);

  /* The temporary file might be left over from a previous manipulation to
   * the same directory. */
  if (unlinkat (parent_fd, tmp, 0) != 0 && errno != ENOENT)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to remove temporary file \"%s/%s\": %s",
                   log_parent_path, tmp, g_strerror (saved_errno));
      return FALSE;
    }

  if (symlinkat (target, parent_fd, tmp) != 0)
    {
      int saved_errno = errno;

      if (saved_errno == EEXIST)
        {
          /* If this fails with EEXIST, then we must be racing with another
           * process that was also trying to make the same symlink. That would
           * be bad, so bail out. We don't use G_IO_ERROR_EXISTS because it
           * would be somewhat misleading when it isn't @link_name that exists,
           * and we don't currently have a specific G_IO_ERROR_CONFLICTING (or
           * similar) because it isn't clear that we'd ever want to handle it
           * differently. */
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "Unable to create symbolic link \"%s/%s\" → \"%s\": %s "
                       "(conflicting with another process also trying to "
                       "create \"%s\"?)",
                       log_parent_path, tmp, target, g_strerror (saved_errno),
                       link_name);
        }
      else
        {
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                       "Unable to create symbolic link \"%s/%s\" → \"%s\": %s",
                       log_parent_path, tmp, target, g_strerror (saved_errno));
        }

      return FALSE;
    }

  /* On btrfs, a renameat() that overwrites a file is atomic and durable,
   * but a renameat() that does not overwrite a file is not; so make sure
   * there is something there for us to overwrite. Another symbolic link to
   * the same file will do.
   *
   * A secondary purpose of this symlinkat() call is that it detects the case
   * where the file already exists, when we don't want to be overwriting
   * an existing file. We can't use renameat2() with RENAME_NOREPLACE
   * because the kernel we use on i.mx6 doesn't support that. */
  if (symlinkat (target, parent_fd, link_name) == 0)
    {
      /* OK: @link_name didn't already exist. To ensure that btrfs commits it,
       * we'll still do an atomic overwrite with another copy of the same
       * thing. */
    }
  else if (errno == EEXIST)
    {
      if (!(flags & CBY_PATH_FLAGS_OVERWRITE))
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_EXISTS,
                       "\"%s/%s\" already exists",
                       log_parent_path, link_name);

          if (unlinkat (parent_fd, tmp, 0) != 0)
            {
              WARNING ("\"%s/%s\" already exists, but unable to remove "
                       "\"%s/%s\": %s",
                       log_parent_path, link_name, log_parent_path, tmp,
                       g_strerror (errno));
            }

          return FALSE;
        }
    }
  else
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to create symbolic link \"%s/%s\" → \"%s\": %s",
                   log_parent_path, link_name, target,
                   g_strerror (saved_errno));
      return FALSE;
    }

  /* By the time we get here, we know it's OK to overwrite link_name:
   * either we wanted to overwrite it, or it is now a symbolic link to
   * the same thing as tmp anyway. */
  if (renameat (parent_fd, tmp, parent_fd, link_name) != 0)
    {
      int saved_errno = errno;

      if (saved_errno == ENOENT)
        {
          /* If tmp has been deleted, that's probably because we're racing
           * with another process that just passed its unlinkat() call.
           * Bail out, and don't use G_IO_ERROR_NOT_FOUND (similar reasoning
           * to the EEXIST case above). */
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "Unable to rename \"%s/%s\" to \"%s/%s\": %s "
                       "(conflicting with another process also trying to "
                       "create \"%s\"?)",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (saved_errno), link_name);
        }
      else
        {
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                       "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (saved_errno));

          if (unlinkat (parent_fd, tmp, 0) != 0)
            {
              WARNING ("Unable to rename \"%s/%s\" to \"%s/%s\", but cannot "
                       "remove it either: %s",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (saved_errno));
            }
        }

      return FALSE;
    }

  return TRUE;
}

gboolean
_cby_is_target (void)
{
  static gsize initialized = 0;
  static gboolean is_target;

  if (g_once_init_enter (&initialized))
    {
      g_autofree gchar *path;

      path = g_build_filename ("/etc", "init.d", "nodm", NULL);
      is_target = !g_file_test (path, G_FILE_TEST_EXISTS);

      DEBUG ("Detected environment (via /etc/init.d/nodm): %s",
             is_target ? "target" : "simulator");

      g_once_init_leave (&initialized, 1);
    }

  return is_target;
}

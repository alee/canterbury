/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <glib.h>

/*
 * dconf-helper — helper for dconf test case
 *
 * Retrieve the "foo" setting from the org.apertis.CanterburyTests.Dconf
 * GSettings schema, increment it, and write the old and new values to
 * a file for inspection by dconf.t.
 */

int
main (int argc,
      char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autofree gchar *output = NULL;
  g_autofree gchar *path =
      g_build_filename (g_get_user_data_dir (), "output.txt", NULL);
  int old;

  /* Disable gvfs, our AppArmor profile won't let us talk to it */
  g_setenv ("GIO_USE_VOLUME_MONITOR", "unix", TRUE);

  settings = g_settings_new ("org.apertis.CanterburyTests.Dconf");
  old = g_settings_get_uint (settings, "foo");
  g_assert_true (g_settings_set_uint (settings, "foo", old + 1));

  /* We can't just write this to stdout because canterbury-exec reopens
   * stdout, so our stdout won't go into a pipe to the test. Write it
   * to a file so the test can check it. */
  output = g_strdup_printf ("Changed foo from %d to %d\n", old, old + 1);

  g_file_set_contents (path, output, -1, &error);
  g_assert_no_error (error);

  g_print ("%s", output);

  /* Make sure the changes have reached disk */
  g_settings_sync ();
  return 0;
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Standalone wrapper for ExampleRequestManager, for manual testing.
 *
 * Usage:
 *
 * (first shell)
 * $ G_MESSAGES_DEBUG=all ./example-request-manager
 * Bus name: :1.123
 *
 * (second shell)
 * $ ./example-requester :1.123 start --message="Hello" --priority=5
 * /_0
 * $ ./example-requester :1.123 start --message="Hi" --role=greeting
 * /_1
 * $ ./example-requester :1.123 stop /_1
 * $ ./example-requester :1.123 stop /_0
 *
 * (watch log messages in first shell, Ctrl+C when done)
 */

#include "tests/example-request-manager.h"

int
main (int argc, char **argv)
{
  g_autoptr (GError) index_error = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyComponentIndex) index =
      cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &index_error);
  g_autoptr (ExampleRequestManager) request_manager =
      example_request_manager_new (index);
  g_autoptr (GDBusConnection) bus =
      g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_autoptr (GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);

  if (index == NULL)
    {
      g_printerr ("%s\n", index_error->message);
      return 1;
    }

  if (bus == NULL ||
      !g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (request_manager), bus,
          "/org/apertis/CanterburyTests/RequestManager", &error))
    {
      g_printerr ("%s\n", error->message);
      return 1;
    }

  g_print ("Bus name: %s\n", g_dbus_connection_get_unique_name (bus));

  g_main_loop_run (main_loop);
  /* not reached */
  return 0;
}

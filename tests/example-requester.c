/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "canterbury/canterbury.h"

#include "tests/example-request-manager-generated.h"

/*
 * example-requester — helper for request-manager test case.
 *
 * This helper process starts or stops a request to the example
 * request manager service. For example, the request might represent an
 * audio stream sent to the audio manager, or an org.freedesktop.Notifications
 * notification sent to the Notifications implementation.
 *
 * It can be used for manual testing in conjunction with
 * the example-request-manager executable (see example-request-manager-main.c)
 * and is also used by the automated test.
 */

static const gchar *opt_role = "";
static gint opt_priority = 0;
static const gchar *opt_message = "Hello, world!";

static GOptionEntry start_options[] = {
  /* In this sample code we assume that the activity started by a request
   * has a *role*, which gives a relatively coarse-grained classification
   * for the activity's importance such as music or phone, and a *priority*,
   * which sets the relative importance of activities with the same role.
   * See #Activity in example-request-manager.c. */
  { "role", 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &opt_role,
    "Class of request [default: \"\"]", NULL },
  { "priority", 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &opt_priority,
    "Priority within the class [default: 0]", NULL },

  /* This represents any attribute of the activity that isn't significant
   * when deciding the priority, such as the title or message of a
   * notification. We use it in the unit test to distinguish between
   * activities. */
  { "message", 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &opt_message,
    "Message to send along with the request [default: a traditional greeting]",
    NULL },

  { NULL }
};

typedef enum
{
  START,
  STOP
} Mode;

static gint
usage (const gchar *message, GOptionContext *context)
{
  g_autofree gchar *help = g_option_context_get_help (context, FALSE, NULL);

  if (message != NULL)
    g_printerr ("%s: %s\n\n", g_get_prgname (), message);

  g_printerr ("%s\n", help);
  return 2;
}

int
main (int argc, char **argv)
{
  g_autoptr (TestsRequestManager) proxy = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GOptionGroup) group = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *activity = NULL;
  Mode mode;

  context = g_option_context_new ("BUS_NAME start|stop");

  group = g_option_group_new ("start", "Options for 'start'",
                              "Show start options", NULL, NULL);
  g_option_group_add_entries (group, start_options);
  g_option_context_add_group (context, g_steal_pointer (&group));

  if (!g_option_context_parse (context, &argc, &argv, &error))
    return usage (error->message, context);

  if (argc < 3 || !g_dbus_is_name (argv[1]))
    return usage ("BUS_NAME is required", context);

  if (g_strcmp0 (argv[2], "start") == 0)
    mode = START;
  else if (g_strcmp0 (argv[2], "stop") == 0)
    mode = STOP;
  else
    return usage ("MODE must be start or stop", context);

  proxy = tests_request_manager_proxy_new_for_bus_sync (
      G_BUS_TYPE_SESSION, G_DBUS_PROXY_FLAGS_NONE, argv[1],
      "/org/apertis/CanterburyTests/RequestManager", NULL, &error);

  if (proxy == NULL)
    goto error;

  switch (mode)
    {
    case START:
      if (argc != 3)
        return usage ("start does have positional parameters", context);

      if (!tests_request_manager_call_start_activity_sync (
              proxy, opt_message, opt_role, opt_priority, &activity, NULL,
              &error))
        goto error;

      g_print ("%s\n", activity);

      break;

    case STOP:
      if (argc != 4 || !g_variant_is_object_path (argv[3]))
        return usage ("stop requires one positional parameter", context);

      if (!tests_request_manager_call_stop_activity_sync (proxy, argv[3], NULL,
                                                          &error))
        goto error;

      break;

    default:
      g_assert_not_reached ();
    }

  return 0;

error:
  g_printerr ("%s\n", error->message);
  return 1;
}

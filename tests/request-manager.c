/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <canterbury/canterbury.h>

#include <unistd.h>

#include "canterbury/platform/component-index-internal.h"

#include "tests/common.h"
#include "tests/example-request-manager.h"

#define APP_BUNDLE                                                            \
  "/Applications/org.apertis.CanterburyTests.RequestManagerClient"
#define IMPORTANT_BUNDLE                                                      \
  "/Applications/org.apertis.CanterburyTests.ImportantClient"

typedef struct
{
  TestsTempComponentIndex component_index;
  GDBusConnection *bus;
  ExampleRequestManager *request_manager;
  const gchar *unique_name;
} Fixture;

static void
setup (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyComponentIndex) index = NULL;

  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (30);

  tests_temp_component_index_setup (&f->component_index);
  index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (index != NULL);

  f->request_manager = example_request_manager_new (index);
  f->bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_assert_no_error (error);
  f->unique_name = g_dbus_connection_get_unique_name (f->bus);

  g_dbus_interface_skeleton_export (
      G_DBUS_INTERFACE_SKELETON (f->request_manager), f->bus,
      "/org/apertis/CanterburyTests/RequestManager", &error);
  g_assert_no_error (error);
}

static void
store_result_cb (GObject *source_object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  GAsyncResult **p = user_data;

  g_assert_nonnull (p);
  g_assert_null (*p);
  *p = g_object_ref (result);
}

static void
wait_for_result (GAsyncResult **result)
{
  while (*result == NULL)
    g_main_context_iteration (NULL, TRUE);
}

static void
test_activities (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *first_id = NULL;
  g_autofree gchar *second_id = NULL;
  const gchar *example_requester;

  example_requester =
      g_test_get_filename (G_TEST_BUILT, "example-requester", NULL);
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, &error,
                                 example_requester, f->unique_name, "start",
                                 "--message=First", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, &first_id, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_strchomp (first_id);
  g_assert_true (g_variant_is_object_path (first_id));
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, first_id), ==,
      "First");

  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, &error,
                                 example_requester, f->unique_name, "start",
                                 "--message=Second", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, &second_id, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_strchomp (second_id);
  g_assert_true (g_variant_is_object_path (second_id));
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, second_id), ==,
      "Second");

  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, example_requester,
                        f->unique_name, "stop", second_id, NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, first_id), ==,
      "First");
  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, second_id), ==,
      NULL);
}

static void
test_ownership (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *first_id = NULL;
  g_autofree gchar *second_id = NULL;
  const gchar *example_requester;

  /* First request to start an activity is from this test, a platform
   * component. We assume we're unconfined. */
  example_requester =
      g_test_get_filename (G_TEST_BUILT, "example-requester", NULL);
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, &error,
                                 example_requester, f->unique_name, "start",
                                 "--message=Platform", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, &first_id, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_strchomp (first_id);
  g_assert_true (g_variant_is_object_path (first_id));
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, first_id), ==,
      "Platform");

  /* Second request to start an activity is from an app-bundle. */
  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, &error, "aa-exec",
                        "--profile", APP_BUNDLE, example_requester,
                        f->unique_name, "start", "--message=Bundle", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, &second_id, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_strchomp (second_id);
  g_assert_true (g_variant_is_object_path (second_id));
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, second_id), ==,
      "Bundle");

  /* The platform process can't remove the app-bundle's activity. */
  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, example_requester,
                        f->unique_name, "stop", second_id, NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_finish (subprocess, result, NULL, NULL, &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 1);
  g_clear_object (&subprocess);

  /* The app-bundle process can't remove the platform's activity. */
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, "aa-exec",
                                 "--profile", APP_BUNDLE, example_requester,
                                 f->unique_name, "stop", first_id, NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_finish (subprocess, result, NULL, NULL, &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 1);
  g_clear_object (&subprocess);

  /* Both are still there. */
  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, first_id), ==,
      "Platform");
  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, second_id), ==,
      "Bundle");

  /* Each can remove its own activity. */

  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, example_requester,
                        f->unique_name, "stop", first_id, NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_finish (subprocess, result, NULL, NULL, &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, "aa-exec",
                                 "--profile", APP_BUNDLE, example_requester,
                                 f->unique_name, "stop", second_id, NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_async (subprocess, NULL, NULL, store_result_cb,
                                  &result);
  wait_for_result (&result);
  g_subprocess_communicate_finish (subprocess, result, NULL, NULL, &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, first_id), ==,
      NULL);
  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, second_id), ==,
      NULL);
}

static void
test_permission (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *path = NULL;
  const gchar *example_requester;

  example_requester =
      g_test_get_filename (G_TEST_BUILT, "example-requester", NULL);

  /* The unprivileged app-bundle is not allowed to use the phone role... */
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, "aa-exec",
                                 "--profile", APP_BUNDLE, example_requester,
                                 f->unique_name, "start", "--message=Bundle",
                                 "--role=phone", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 1);
  g_clear_object (&subprocess);

  /* ... or high priorities. */
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error, "aa-exec",
                                 "--profile", APP_BUNDLE, example_requester,
                                 f->unique_name, "start", "--message=Bundle",
                                 "--priority=123", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 1);
  g_clear_object (&subprocess);

  g_assert_cmpuint (
      example_request_manager_count_activities (f->request_manager), ==, 0);

  /* The privileged app-bundle is allowed to do both: it is allowed to say
   * it's a phone call because it is in the Telephony category, and it is
   * allowed to give itself high priority because it has the
   * X-WayneIndustries-Critical annotation. */
  subprocess = g_subprocess_new (
      G_SUBPROCESS_FLAGS_STDOUT_PIPE, &error, "aa-exec", "--profile",
      IMPORTANT_BUNDLE, example_requester, f->unique_name, "start",
      "--message=Bat phone", "--role=phone", "--priority=123", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, &path, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_strchomp (path);
  g_assert_true (g_variant_is_object_path (path));
  g_clear_object (&subprocess);

  g_assert_cmpstr (
      example_request_manager_get_message (f->request_manager, path), ==,
      "Bat phone");
  g_assert_cmpuint (
      example_request_manager_count_activities (f->request_manager), ==, 1);
}

static void
test_priorities (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *path = NULL;
  g_auto (GStrv) ordered = NULL;
  const gchar *example_requester;

  example_requester =
      g_test_get_filename (G_TEST_BUILT, "example-requester", NULL);

  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error,
                                 example_requester, f->unique_name, "start",
                                 "--message=High priority music",
                                 "--role=music", "--priority=123", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error,
                                 example_requester, f->unique_name, "start",
                                 "--message=Low priority music",
                                 "--role=music", "--priority=-42", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  subprocess = g_subprocess_new (
      G_SUBPROCESS_FLAGS_NONE, &error, example_requester, f->unique_name,
      "start", "--message=Really high priority role", "--role=phone", NULL);
  g_assert_no_error (error);
  g_subprocess_communicate_utf8_async (subprocess, NULL, NULL, store_result_cb,
                                       &result);
  wait_for_result (&result);
  g_subprocess_communicate_utf8_finish (subprocess, result, NULL, NULL,
                                        &error);
  g_clear_object (&result);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (subprocess));
  g_assert_cmpint (g_subprocess_get_exit_status (subprocess), ==, 0);
  g_clear_object (&subprocess);

  ordered = example_request_manager_list_messages (f->request_manager);
  g_assert_cmpstr (ordered[0], ==, "Really high priority role");
  g_assert_cmpstr (ordered[1], ==, "High priority music");
  g_assert_cmpstr (ordered[2], ==, "Low priority music");
  g_assert_cmpstr (ordered[3], ==, NULL);
}

static void
teardown (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  /* cancel watchdog */
  alarm (0);

  tests_temp_component_index_teardown (&f->component_index);

  g_dbus_interface_skeleton_unexport (
      G_DBUS_INTERFACE_SKELETON (f->request_manager));
  g_clear_object (&f->request_manager);
  g_clear_object (&f->bus);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/request-manager/activities", Fixture, NULL, setup,
              test_activities, teardown);
  g_test_add ("/request-manager/ownership", Fixture, NULL, setup,
              test_ownership, teardown);
  g_test_add ("/request-manager/permission", Fixture, NULL, setup,
              test_permission, teardown);
  g_test_add ("/request-manager/priorities", Fixture, NULL, setup,
              test_priorities, teardown);

  return g_test_run ();
}

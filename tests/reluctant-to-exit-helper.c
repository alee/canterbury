/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * reluctant-to-exit-helper — an instrumented application which does not die
 *  on receiving SIGTERM
 *
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <signal.h>

#include <glib.h>
#include <glib-unix.h>
#include <gio/gio.h>

#define BUNDLE_ID "org.apertis.CanterburyTests.ReluctantToExit"
#define ENTRY_POINT_ID BUNDLE_ID
#define LOGGING_IFACE BUNDLE_ID

#define TESTS_TYPE_APP (tests_app_get_type ())
G_DECLARE_FINAL_TYPE (TestsApp, tests_app, TESTS, APP, GApplication)

struct _TestsApp
{
  GApplication parent;
};

G_DEFINE_TYPE (TestsApp, tests_app, G_TYPE_APPLICATION)

/*
 * Called from the main loop after we receive SIGTERM.
 */
static gboolean
sigterm_cb (gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  GDBusConnection *connection = G_DBUS_CONNECTION (user_data);

  /* Tell the test what has happened: this simulates saving our state
   * or something */
  g_dbus_connection_emit_signal (connection, NULL, "/", LOGGING_IFACE,
                                 "ReceivedTerminateSignal", NULL, &error);
  g_assert_no_error (error);
  g_dbus_connection_flush_sync (connection, NULL, &error);
  g_assert_no_error (error);

  /* Don't exit: this simulates an app that gets stuck while trying to exit */

  return G_SOURCE_CONTINUE;
}

static gboolean
dbus_register (GApplication *app,
               GDBusConnection *session_bus,
               const gchar *name,
               GError **error)
{
  g_application_hold (app);

  if (!G_APPLICATION_CLASS (tests_app_parent_class)
      ->dbus_register (app, session_bus, name, error))
    return FALSE;

  g_unix_signal_add_full (G_PRIORITY_DEFAULT, SIGTERM, sigterm_cb,
                          g_object_ref (session_bus), g_object_unref);
  return TRUE;
}

static void
tests_app_class_init (TestsAppClass *cls)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (cls);

  app_class->dbus_register = dbus_register;
}

static void
tests_app_init (TestsApp *self)
{
}

int
main (int argc, char **argv)
{
  g_autoptr (GApplication) app = NULL;

  app = G_APPLICATION (g_object_new (TESTS_TYPE_APP,
                                     "application-id", ENTRY_POINT_ID,
                                     NULL));

  return g_application_run (app, argc, argv);
}

#!/usr/bin/env python3

# Copyright © 2016-2017 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import contextlib
import os
import shutil
import subprocess
import sys
import textwrap

if __name__ == '__main__':
    assert len(sys.argv) == 3
    top_srcdir = sys.argv[1]
    id_version = sys.argv[2]

    bundle_id, version = id_version.split('-')

    destdir = os.path.join('tests', '{}-{}.d'.format(bundle_id, version))

    with contextlib.suppress(FileNotFoundError):
        shutil.rmtree(destdir)

    repo = os.path.join('tests', '{}-{}.repo'.format(bundle_id, version))

    with contextlib.suppress(FileNotFoundError):
        shutil.rmtree(repo)

    bundle = os.path.join('tests', '{}-{}.bundle'.format(bundle_id, version))

    with contextlib.suppress(FileNotFoundError):
        os.remove(bundle)

    os.makedirs(os.path.join(destdir, 'export'), exist_ok=True)
    os.makedirs(os.path.join(destdir, 'files', 'bin'), exist_ok=True)
    os.makedirs(os.path.join(destdir, 'files', 'etc', 'apparmor.d'),
            exist_ok=True)
    os.makedirs(os.path.join(destdir, 'files', 'share', 'applications'),
            exist_ok=True)
    os.makedirs(os.path.join(destdir, 'files', 'share', 'metainfo'),
            exist_ok=True)

    with open(os.path.join(destdir, 'metadata'), 'w') as writer:
        writer.write(textwrap.dedent('''\
            [Application]
            name={bundle_id}
            X-Apertis-BundleVersion={version}
        ''').format(bundle_id=bundle_id, version=version))

    if bundle_id == 'org.apertis.CanterburyTests.PostinstPrerm':
        # Keep this code to populate the app-bundle in sync with
        # what's required, and what's installed with the low-level API,
        # in tests/postinst-prerm.c

        with open(
                os.path.join(destdir, 'files', 'etc', 'apparmor.d',
                    'Applications.' + bundle_id),
                'w') as writer:
            # For test_install_apparmor()
            writer.write(textwrap.dedent('''\
                # Version {v}
                #include <tunables/global>
                /Applications/{bundle_id}/** {{
                    #include <abstractions/base>

                    # Allow executing postinst-test-helper, wherever it is
                    /**/postinst-test-helper rmix,

                    # Allow writing files as required by
                    # test_install_apparmor() in postinst-prerm.c
                    /tmp/cby-postinst-prerm.*/both-may-write* rw,
                    /tmp/cby-postinst-prerm.*/v{v}-may-write* rw,
                }}
            ''').format(bundle_id=bundle_id, v=version[0]))

        other_entry_point_id = bundle_id + '.v' + version[0]

        # For test_install()
        for entry_point in (bundle_id, other_entry_point_id):
            with open(
                    os.path.join(destdir, 'files', 'share', 'applications',
                        entry_point + '.desktop'),
                    'w') as writer:
                writer.write(textwrap.dedent('''\
                    [Desktop Entry]
                    Name=Test bundle
                    Exec=canterbury-exec /Applications/{bundle_id}/bin/{entry_point}
                    MimeType=application/vnd.example.{entry_point}
                    Type=Application
                ''').format(
                    bundle_id=bundle_id,
                    entry_point=entry_point))

            source_icon = os.path.join(top_srcdir, 'tests', 'usr', 'share',
                    'icons', 'example.png')

            # See
            # https://appdev.apertis.org/documentation/bundle-spec.html#icon-for-the-bundle
            # for more info on icons for app-bundles.
            for theme in ('hicolor', 'com.example.Metallic'):
                for size in [8, 16, 22, 24, 32, 36, 42, 48, 64, 72, 96, 128,
                        192, 256, 512]:
                    icon_dest_dir = os.path.join(destdir, 'files', 'share',
                            'icons', theme, '{size}x{size}'.format(size=size),
                            'apps')
                    icon_dest = os.path.join(icon_dest_dir,
                            entry_point + '.png')
                    os.makedirs(icon_dest_dir, exist_ok=True)
                    shutil.copy(source_icon, icon_dest)

    elif bundle_id == 'org.apertis.CanterburyTests.ServiceManager':
        # Keep this code to populate the app-bundle in sync with
        # what's required, and what's installed with the low-level API,
        # in tests/service-manager.c

        agent_entry_point_id = bundle_id + '.Agent'

        # For test_services_availability()
        for entry_point, type, exec_options in (
                (bundle_id, 'application', ''),
                (agent_entry_point_id, 'agent-service', '--'),
                ):
            if entry_point == agent_entry_point_id and version == '2.0':
                continue

            with open(
                    os.path.join(destdir, 'files', 'share', 'applications',
                        entry_point + '.desktop'),
                    'w') as writer:
                writer.write(textwrap.dedent('''\
                    [Desktop Entry]
                    Name=Test bundle ({entry_point} version: {version})
                    Exec=canterbury-exec {exec_options} /Applications/{bundle_id}/bin/{entry_point} version "{version}"
                    Type=Application
                    X-Apertis-Type={type}
                    Path=/Applications/{bundle_id}
                ''').format(
                    bundle_id=bundle_id,
                    entry_point=entry_point,
                    exec_options=exec_options,
                    type=type,
                    version=version,
                ))

            # Create a fake executable to make it more realistic
            with open(
                    os.path.join(destdir, 'files', 'bin', entry_point),
                    'w') as writer:
                writer.write('#!/bin/sh\n')
                writer.write('exit 0\n')

            os.chmod(os.path.join(destdir, 'files', 'bin', entry_point), 0o755)

    elif bundle_id == 'org.apertis.CanterburyTests.StoreAppBundleAppArmor':
        # Keep this code to populate the app-bundle in sync with
        # what's required for debian/tests/store-app-bundle-apparmor

        with open(
                os.path.join(destdir, 'files', 'etc', 'apparmor.d',
                    'Applications.' + bundle_id),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                #include <tunables/global>
                /Applications/{bundle_id}/** {{
                    #include <abstractions/base>
                    /{{usr/,}}bin/true ix,
                    /{{usr/,}}lib/** mr,
                }}
            ''').format(bundle_id=bundle_id))

        with open(
                os.path.join(destdir, 'files', 'share', 'metainfo',
                    bundle_id + '.appdata.xml'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                <?xml version="1.0" encoding="UTF-8"?>
                <component type="desktop">
                  <id>{bundle_id}</id>
                  <metadata_license>CC0-1.0</metadata_license>
                  <project_license>MPL-2.0</project_license>
                  <name>A store app-bundle</name>
                  <release version="0" date="1970-01-01"/>
                </component>
            ''').format(bundle_id=bundle_id))

    elif bundle_id == 'org.apertis.CanterburyTests.Agent':
        # Keep this code to populate the app-bundle in sync with
        # what's required for tests/agents.c

        with open(
                os.path.join(destdir, 'files', 'etc', 'apparmor.d',
                    'Applications.' + bundle_id),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                #include <tunables/global>
                /Applications/{bundle_id}/** {{
                    #include <abstractions/base>
                    #include <abstractions/dbus-session-strict>
                    /Applications/{bundle_id}/** mrix,
                    owner /var/Applications/{bundle_id}/users/** rwk,
                    dbus send
                         bus=session
                         path=/org/freedesktop/DBus
                         interface=org.freedesktop.DBus
                         member=RequestName
                         peer=(name=org.freedesktop.DBus),
                    dbus bind bus=session name={bundle_id},
                    dbus bind bus=session name={bundle_id}.NotAgent,
                    dbus (send, receive)
                         bus=session
                         peer=(label=@{{profile_name}}),

                    # For the test, which receives our signals
                    dbus send
                         bus=session
                         interface={bundle_id}
                         peer=(label=unconfined),
                }}
            ''').format(bundle_id=bundle_id))

        with open(
                os.path.join(destdir, 'files', 'share', 'metainfo',
                    bundle_id + '.appdata.xml'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                <?xml version="1.0" encoding="UTF-8"?>
                <component type="desktop">
                  <id>{bundle_id}</id>
                  <metadata_license>CC0-1.0</metadata_license>
                  <project_license>MPL-2.0</project_license>
                  <name>Test bundle ({bundle_id})</name>
                  <release version="1.0" date="1970-01-01"/>
                </component>
            ''').format(bundle_id=bundle_id))

        entry_point = bundle_id

        with open(
                os.path.join(destdir, 'files', 'share', 'applications',
                    entry_point + '.desktop'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                [Desktop Entry]
                Name=Test agent ({entry_point})
                Exec=canterbury-exec /Applications/{bundle_id}/bin/agent-helper
                Type=Application
                X-Apertis-Type=agent-service
            ''').format(
                bundle_id=bundle_id,
                entry_point=entry_point,
            ))

        entry_point = bundle_id + '.NotAgent'

        with open(
                os.path.join(destdir, 'files', 'share', 'applications',
                    entry_point + '.desktop'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                [Desktop Entry]
                Name=Test non-agent ({entry_point})
                Exec=canterbury-exec /Applications/{bundle_id}/bin/not-agent
                Type=Application
                X-Apertis-Type=application
            ''').format(
                bundle_id=bundle_id,
                entry_point=entry_point,
            ))

        subprocess.check_call([
            './libtool', '--mode=install', 'install', '-m755',
            'agent-helper',
            os.path.join(os.getcwd(), destdir, 'files', 'bin', 'agent-helper'),
            ])
        subprocess.check_call([
            './libtool', '--mode=install', 'install', '-m755',
            'not-agent',
            os.path.join(os.getcwd(), destdir, 'files', 'bin', 'not-agent'),
            ])

    elif bundle_id == 'org.apertis.CanterburyTests.ReluctantToExit':
        # Keep this code to populate the app-bundle in sync with
        # what's required for tests/terminate.c

        with open(
                os.path.join(destdir, 'files', 'etc', 'apparmor.d',
                    'Applications.' + bundle_id),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                #include <tunables/global>
                /Applications/{bundle_id}/** {{
                    #include <abstractions/base>
                    #include <abstractions/dbus-session-strict>
                    /Applications/{bundle_id}/** mrix,
                    owner /var/Applications/{bundle_id}/users/** rwk,
                    dbus send
                         bus=session
                         path=/org/freedesktop/DBus
                         interface=org.freedesktop.DBus
                         member=RequestName
                         peer=(name=org.freedesktop.DBus),
                    dbus bind bus=session name={bundle_id},
                    dbus (send, receive)
                         bus=session
                         peer=(label=@{{profile_name}}),

                    # For the test, which receives our signals
                    dbus send
                         bus=session
                         interface={bundle_id}
                         peer=(label=unconfined),
                }}
            ''').format(bundle_id=bundle_id))

        with open(
                os.path.join(destdir, 'files', 'share', 'metainfo',
                    bundle_id + '.appdata.xml'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                <?xml version="1.0" encoding="UTF-8"?>
                <component type="desktop">
                  <id>{bundle_id}</id>
                  <metadata_license>CC0-1.0</metadata_license>
                  <project_license>MPL-2.0</project_license>
                  <name>Test bundle ({bundle_id})</name>
                  <release version="1.0" date="1970-01-01"/>
                </component>
            ''').format(bundle_id=bundle_id))

        entry_point = bundle_id

        with open(
                os.path.join(destdir, 'files', 'share', 'applications',
                    entry_point + '.desktop'),
                'w') as writer:
            writer.write(textwrap.dedent('''\
                [Desktop Entry]
                Name=Test agent ({entry_point})
                Exec=canterbury-exec /Applications/{bundle_id}/bin/reluctant-to-exit-helper
                Type=Application
                X-Apertis-Type=application
            ''').format(
                bundle_id=bundle_id,
                entry_point=entry_point,
            ))

        subprocess.check_call([
            './libtool', '--mode=install', 'install', '-m755',
            'reluctant-to-exit-helper',
            os.path.join(os.getcwd(), destdir, 'files', 'bin',
                'reluctant-to-exit-helper'),
            ])

    subprocess.check_call(['flatpak', 'build-export', repo, destdir])
    shutil.rmtree(destdir)
    subprocess.check_call(['flatpak', 'build-bundle', repo, bundle, bundle_id])
    shutil.rmtree(repo)

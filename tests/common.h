/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef TESTS_COMMON_H
#define TESTS_COMMON_H

#include <errno.h>

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <canterbury/canterbury.h>

G_BEGIN_DECLS

#define SYSTEMD_BUS_NAME "org.freedesktop.systemd1"
#define SYSTEMD_PATH_MANAGER "/org/freedesktop/systemd1"
#define SYSTEMD_IFACE_MANAGER SYSTEMD_BUS_NAME ".Manager"
#define SYSTEMD_IFACE_UNIT SYSTEMD_BUS_NAME ".Unit"
#define DBUS_BUS_NAME_DAEMON "org.freedesktop.DBus"
#define DBUS_PATH_DAEMON "/org/freedesktop/DBus"
#define DBUS_IFACE_DAEMON "org.freedesktop.DBus"
#define DBUS_IFACE_PROPERTIES "org.freedesktop.DBus.Properties"

const gchar *tests_printf (const gchar *format, ...) G_GNUC_PRINTF (1, 2);

gboolean tests_require_apparmor_label (const gchar *expected_label);

typedef enum
{
  TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM,
  TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_STORE,
} TestsRunUpdateComponentIndexMode;

typedef struct
{
  gchar *tmpdir;
  gchar *store_cache;
  gchar *platform_cache;
  gchar *extensions_cache;
} TestsTempComponentIndex;

void tests_temp_component_index_update (TestsTempComponentIndex *self,
                                        TestsRunUpdateComponentIndexMode mode);

void tests_temp_component_index_setup (TestsTempComponentIndex *self);
void tests_temp_component_index_teardown (TestsTempComponentIndex *self);

void tests_store_result_cb (GObject *source_object,
                            GAsyncResult *result,
                            gpointer user_data);

/* Using string comparison so we get a better assertion message */
#define tests_assert_syscall_0(syscall) \
  g_assert_cmpstr ((syscall) != 0 ? g_strerror (errno) : "", ==, "")

#define tests_assert_syscall_0_or_enoent(syscall) \
  g_assert_cmpstr ((syscall) != 0 && errno != ENOENT ? g_strerror (errno) : "", ==, "")

typedef enum
{
  TESTS_ROLLBACK_MODE_CANCEL_INSTALL,
  TESTS_ROLLBACK_MODE_ROLL_BACK
} TestsRollbackMode;

typedef struct
{
  gchar *number;
} TestsVersion;

typedef struct
{
  GDBusProxy *app_store_proxy;
  GDBusProxy *bundle_manager1_proxy;
  GDBusConnection *system_bus;
  const gchar *ribchester_bus_name; /* borrowed from a GDBusProxy */
  CbyProcessInfo *process_info;
  gchar *id;
  TestsVersion *versions;
  gsize n_versions;
} TestsAppBundle;

void tests_app_bundle_install_bundle (TestsAppBundle *self,
                                      const TestsVersion *from_version,
                                      const TestsVersion *to_version);
gchar *tests_app_bundle_begin_install (TestsAppBundle *self,
                                       const TestsVersion *from_version,
                                       const TestsVersion *to_version);
void tests_app_bundle_commit_install (TestsAppBundle *self,
                                      const TestsVersion *from_version,
                                      const TestsVersion *to_version);
void tests_app_bundle_roll_back (TestsAppBundle *self,
                                 TestsRollbackMode mode,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version);
void tests_app_bundle_uninstall (TestsAppBundle *self,
                                 const TestsVersion *version);
void tests_app_bundle_reinstall (TestsAppBundle *self,
                                 const TestsVersion *version);
gboolean tests_app_bundle_try_remove (TestsAppBundle *self,
                                      GError **error);
void tests_app_bundle_remove (TestsAppBundle *self);
void tests_app_bundle_delete_rollback_snapshot (TestsAppBundle *self,
                                                const TestsVersion *version);

gboolean tests_app_bundle_set_up (TestsAppBundle *bundle,
                                  GDBusProxy *app_store_proxy,
                                  GDBusProxy *bundle_manager1_proxy,
                                  const gchar *bundle_id,
                                  guint ordinary_uid,
                                  const gchar * const *version_numbers,
                                  gsize n_versions);
void tests_app_bundle_tear_down (TestsAppBundle *bundle);

gboolean tests_require_ribchester (GDBusConnection **bus_out,
                                   GDBusProxy **app_store_proxy_out,
                                   GDBusProxy **bundle_manager1_proxy_out);

gboolean tests_require_apparmor_active (void);
gboolean tests_require_running_as_root (void);
gboolean tests_require_real_session_bus (void);

gboolean tests_require_ordinary_uid (guint *ordinary_uid,
                                     guint *ordinary_gid,
                                     guint *unrelated_uid,
                                     guint *unrelated_gid);

gboolean tests_remove_apparmor_profile (const gchar *name,
                                        GError **error);

void tests_assert_symlink_to (const gchar *path,
                              const gchar *expected_target);
void tests_assert_no_file (const gchar *path);
void tests_assert_type_nofollow (const gchar *path, unsigned int s_ifmt);

void tests_wait_for_no_name_owner (GDBusConnection *bus,
                                   const gchar *name,
                                   guint seconds);
void tests_wait_for_name_owner (GDBusConnection *bus,
                                const gchar *name,
                                guint seconds,
                                gchar **name_owner_out);

typedef guint TestsNameWatch;
void tests_name_watch_clear (TestsNameWatch *id_p);
G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (TestsNameWatch, tests_name_watch_clear)

typedef guint TestsNameOwnership;
void tests_name_ownership_clear (TestsNameOwnership *id_p);
G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (TestsNameOwnership,
                                  tests_name_ownership_clear)

typedef struct
{
  GDBusConnection *connection;
  guint id;
} TestsSignalSubscription;
#define TESTS_SIGNAL_SUBSCRIPTION_INIT { NULL, 0 }
void tests_signal_subscription_init (TestsSignalSubscription *sub);
void tests_signal_subscription_set (TestsSignalSubscription *sub,
                                    GDBusConnection *connection,
                                    guint id);
void tests_signal_subscription_clear (TestsSignalSubscription *sub);
G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (TestsSignalSubscription,
                                  tests_signal_subscription_clear)

typedef guint TestsScopedTimeout;
void tests_scoped_timeout_start (TestsScopedTimeout *id_p,
                                 guint seconds);
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (TestsScopedTimeout, g_source_remove, 0)

G_END_DECLS

#endif

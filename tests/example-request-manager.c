/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * ExampleRequestManager:
 *
 * A code sample demonstrating a service that receives prioritized
 * requests from unprivileged app-bundles.
 *
 * For example, the implementation of org.freedesktop.Notifications in Apertis
 * might have a design vaguely resembling this: it receives requests from
 * callers of the form "display this notification" containing priority
 * information, and it displays them.
 *
 * The audio manager might also have a design similar to this, receiving
 * requests to play an audio stream and selecting whichever audio stream
 * is currently highest-priority, although we anticipate that its audio
 * streams will probably be received as PulseAudio streams (via AF_UNIX)
 * instead of D-Bus method calls.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "ExampleRequestManager"

#include "tests/example-request-manager.h"

#include <canterbury/canterbury.h>
#include "canterbury/messages-internal.h"

struct _ExampleRequestManager
{
  /*< private >*/
  TestsRequestManagerSkeleton parent_instance;
  /* owned */
  CbyComponentIndex *component_index;
  /* unowned object path => owned Activity */
  GHashTable *activities;
  /* list of unowned Activity (borrowed from @activities), most important
   * first */
  GQueue sorted_activities;
};

typedef enum
{
  PROP_COMPONENT_INDEX = 1,
} Property;

static GParamSpec *property_specs[PROP_COMPONENT_INDEX + 1] = { NULL };

G_DEFINE_TYPE (ExampleRequestManager,
               example_request_manager,
               TESTS_TYPE_REQUEST_MANAGER_SKELETON)

/*
 * Activity:
 * @refcount: reference count
 * @path: an object path used as an opaque identifier for this activity
 * @message: a human-readable message that was supplied when this activity
 *  was started
 * @role: the role, class or category of this activity
 * @priority: the priority of this activity within its @role
 * @process_info: details of the process that initiated this activity
 *
 * A data structure representing whatever this manager manages.
 *
 * For example, in the Notifications implementation, the equivalent of
 * Activity would probably be named Notification and would represent a
 * notification message. In the Audio Manager implementation, the equivalent
 * of Activity might be named Stream and would represent an ongoing stream
 * of audio samples.
 *
 * In this code sample, we have assumed that each activity has a *role*,
 * which provides a relatively coarse idea of how important it is, and a
 * *priority*, which specifies how important it is when compared with
 * other activities with the same role.
 *
 * @message represents any other information in an activity, other than
 * the parts used for priority assignment. It is used in the regression test
 * for this sample as a simple way to distinguish between activities.
 */
typedef struct
{
  gsize refcount;
  gchar *path;
  gchar *message;
  gchar *role;
  gint32 priority;
  gint effective_priority;
  CbyProcessInfo *process_info;
} Activity;

static void
activity_unref (Activity *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);

  if (--self->refcount == 0)
    {
      g_free (self->path);
      g_free (self->message);
      g_free (self->role);
      g_free (self);
    }
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Activity, activity_unref)

static Activity *
activity_new (const gchar *message, const gchar *role, gint32 priority)
{
  g_autoptr (Activity) self = g_new0 (Activity, 1);

  self->refcount = 1;
  self->path = NULL;
  self->message = g_strdup (message);
  self->role = g_strdup (role);
  self->priority = priority;

  return g_steal_pointer (&self);
}

static Activity *
activity_ref (Activity *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);

  self->refcount++;
  return self;
}

enum
{
  PRIORITY_BAND_PHONE = 9000,
  PRIORITY_BAND_NAVIGATION = 7000,
  PRIORITY_BAND_SMS = 6000,
  PRIORITY_BAND_TRAFFIC_BULLETIN = 3000,
  PRIORITY_BAND_DEFAULT = 0,
  PRIORITY_BAND_MUSIC = -1000,
};

static void
activity_prioritize (Activity *self,
                     CbyComponent *component)
{
  gint band;

  /* An example of one possible OEM-specific policy for how to prioritize
   * audio streams, or notifications, or whatever concrete thing Activity
   * represents. */

  if (g_strcmp0 (self->role, "phone") == 0)
    band = PRIORITY_BAND_PHONE;
  else if (g_strcmp0 (self->role, "navigation") == 0)
    band = PRIORITY_BAND_NAVIGATION;
  else if (g_strcmp0 (self->role, "sms") == 0)
    band = PRIORITY_BAND_SMS;
  else if (g_strcmp0 (self->role, "traffic-bulletin") == 0)
    band = PRIORITY_BAND_TRAFFIC_BULLETIN;
  else if (g_strcmp0 (self->role, "music") == 0)
    band = PRIORITY_BAND_MUSIC;
  else
    band = PRIORITY_BAND_DEFAULT;

  /* An example of how categories could be used to influence OEM policy */
  if (component != NULL &&
      g_strv_contains (cby_component_get_categories (component), "Amusement"))
    band -= 500;

  self->effective_priority = band + CLAMP (self->priority, -200, +200);
}

static gint
activity_cmp_most_important_first (gconstpointer a,
                                   gconstpointer b,
                                   gpointer user_data)
{
  const Activity *left = a;
  const Activity *right = b;

  if (left->effective_priority > right->effective_priority)
    return -1;

  if (left->effective_priority < right->effective_priority)
    return 1;

  return 0;
}

static void
dump_priority_queue (ExampleRequestManager *self)
{
  GList *l;

  DEBUG ("Activity queue (most important first) is now:");

  for (l = g_queue_peek_head_link (&self->sorted_activities);
       l != NULL;
       l = l->next)
    {
      Activity *a = l->data;

      DEBUG ("\t* %s", a->message);
    }

  DEBUG ("\t(end)");
}

static void
start_activity_get_sender_cb (GObject *source G_GNUC_UNUSED,
                              GAsyncResult *result,
                              gpointer user_data)
{
  /* Used to generate unique IDs */
  static guint64 activity_number = 0;

  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = g_steal_pointer (&user_data);
  g_autoptr (CbyProcessInfo) process_info =
      cby_process_info_new_for_dbus_invocation_finish (result, &error);
  g_autoptr (CbyComponent) component = NULL;
  Activity *activity = g_task_get_task_data (task);
  ExampleRequestManager *self =
      EXAMPLE_REQUEST_MANAGER (g_task_get_source_object (task));
  const gchar *bundle_id;

  if (process_info == NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Unable to identify caller: %s",
                               error->message);
      return;
    }

  if (cby_process_info_get_process_type (process_info) ==
      CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Unable to identify caller");
      return;
    }

  /* To keep this example simple we treat all platform processes as
   * equivalent (bundle_id == NULL). */
  bundle_id = cby_process_info_get_bundle_id (process_info);

  if (bundle_id != NULL)
    {
      component = cby_component_index_get_by_process (self->component_index,
                                                      process_info);

      if (component == NULL)
        {
          g_task_return_new_error (
              task, CBY_ERROR, CBY_ERROR_FAILED,
              "Unable to match caller '%s' to a software component",
              bundle_id);
          return;
        }

      /* As an example OEM policy for this sample code, let's suppose that
       * app-bundles are not allowed to set role = phone unless they are in
       * the Telephony category. */
      if (g_strcmp0 (activity->role, "phone") == 0)
        {
          const gchar *const *categories =
              cby_component_get_categories (component);

          if (!g_strv_contains (categories, "Telephony"))
            {
              g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                                       "'%s' is not a telephony app",
                                       bundle_id);
              return;
            }
        }

      /* As another example OEM policy for this sample code, let's suppose that
       * app-bundles are not allowed to set priority > 10 unless they have
       * the X-WayneIndustries-Critical metadata item with a non-empty value.
       */
      if (activity->priority > 10)
        {
          AsApp *app = cby_component_get_appstream_app (component);
          const gchar *value =
              as_app_get_metadata_item (app, "X-WayneIndustries-Critical");

          if (value == NULL || value[0] == '\0')
            {
              g_task_return_new_error (
                  task, CBY_ERROR, CBY_ERROR_FAILED,
                  "Non-critical app-bundle '%s' cannot set high priorities",
                  bundle_id);
              return;
            }
        }
    }

  activity->process_info = g_object_ref (process_info);
  activity->path =
      g_strdup_printf ("/_%" G_GINT64_MODIFIER "x", activity_number++);
  activity_prioritize (activity, component);

  DEBUG ("Opening new activity (audio stream or notification or whatever):");
  DEBUG ("\tBundle ID: %s", bundle_id ? bundle_id : "NULL");
  DEBUG ("\tMessage: %s", activity->message);
  DEBUG ("\tRole: %s", activity->role);
  DEBUG ("\tPriority: %d", activity->priority);
  DEBUG ("\t-> Object path: %s", activity->path);
  DEBUG ("\t-> Effective priority: %d", activity->effective_priority);

  g_hash_table_replace (self->activities, activity->path,
                        activity_ref (activity));
  g_queue_insert_sorted (&self->sorted_activities, activity,
                         activity_cmp_most_important_first, NULL);

  dump_priority_queue (self);

  g_task_return_pointer (task, g_strdup (activity->path), g_free);
}

static void
start_activity_done_cb (GObject *object,
                        GAsyncResult *result,
                        gpointer user_data)
{
  ExampleRequestManager *self = EXAMPLE_REQUEST_MANAGER (object);
  GTask *task = G_TASK (result);
  g_autoptr (GError) error = NULL;
  g_autofree gchar *activity = g_task_propagate_pointer (task, &error);
  /* take ownership */
  g_autoptr (GDBusMethodInvocation) invocation = user_data;

  if (activity != NULL)
    tests_request_manager_complete_start_activity (
        TESTS_REQUEST_MANAGER (self), invocation, activity);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static gboolean
example_request_manager_start_activity_cb (ExampleRequestManager *self,
                                           GDBusMethodInvocation *invocation,
                                           const gchar *message,
                                           const gchar *role,
                                           gint32 priority,
                                           gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (Activity) activity = activity_new (message, role, priority);
  g_autoptr (GTask) task = g_task_new (self, NULL, start_activity_done_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, activity_ref (activity),
                        (GDestroyNotify) activity_unref);
  cby_process_info_new_for_dbus_invocation_async (
      invocation, NULL, start_activity_get_sender_cb, g_object_ref (task));
  return TRUE; /* handled */
}

static void
stop_activity_get_sender_cb (GObject *source G_GNUC_UNUSED,
                             GAsyncResult *result,
                             gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  /* take ownership */
  g_autoptr (GTask) task = user_data;
  g_autoptr (CbyProcessInfo) process_info =
      cby_process_info_new_for_dbus_invocation_finish (result, &error);
  ExampleRequestManager *self =
      EXAMPLE_REQUEST_MANAGER (g_task_get_source_object (task));
  const gchar *activity_path = g_task_get_task_data (task);
  const gchar *bundle_id;
  Activity *activity;

  if (process_info == NULL ||
      cby_process_info_get_process_type (process_info) ==
        CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Unable to identify caller: %s",
                               error->message);
      return;
    }

  bundle_id = cby_process_info_get_bundle_id (process_info);

  activity = g_hash_table_lookup (self->activities, activity_path);

  if (activity == NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                               "Activity '%s' does not exist", activity_path);
      return;
    }

  /* To keep this example simple we allow any platform process to remove
   * activities started by any other platform process. */
  if (g_strcmp0 (bundle_id,
                 cby_process_info_get_bundle_id (activity->process_info)) != 0)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                               "Activity '%s' was not created by you",
                               activity_path);
      return;
    }

  DEBUG ("Removing activity:");
  DEBUG ("\tBundle ID: %s", bundle_id ? bundle_id : "NULL");
  DEBUG ("\tMessage: %s", activity->message);
  DEBUG ("\tRole: %s", activity->role);
  DEBUG ("\tPriority: %d", activity->priority);
  DEBUG ("\t-> Object path: %s", activity->path);

  g_queue_remove_all (&self->sorted_activities, activity);
  g_hash_table_remove (self->activities, activity->path);

  dump_priority_queue (self);

  g_task_return_boolean (task, TRUE);
}

static void
stop_activity_done_cb (GObject *object,
                       GAsyncResult *result,
                       gpointer user_data)
{
  ExampleRequestManager *self = EXAMPLE_REQUEST_MANAGER (object);
  GTask *task = G_TASK (result);
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusMethodInvocation) invocation = g_steal_pointer (&user_data);

  if (g_task_propagate_boolean (task, &error))
    tests_request_manager_complete_stop_activity (TESTS_REQUEST_MANAGER (self),
                                                  invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static gboolean
example_request_manager_stop_activity_cb (ExampleRequestManager *self,
                                          GDBusMethodInvocation *invocation,
                                          const gchar *activity,
                                          gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GTask) task = g_task_new (self, NULL, stop_activity_done_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, g_strdup (activity), g_free);
  cby_process_info_new_for_dbus_invocation_async (
      invocation, NULL, stop_activity_get_sender_cb, g_object_ref (task));
  return TRUE; /* handled */
}

static void
example_request_manager_init (ExampleRequestManager *self)
{
  self->activities = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                            (GDestroyNotify) activity_unref);
}

static void
example_request_manager_constructed (GObject *object)
{
  ExampleRequestManager *self = EXAMPLE_REQUEST_MANAGER (object);

  G_OBJECT_CLASS (example_request_manager_parent_class)->constructed (object);

  g_signal_connect (self, "handle-start-activity",
                    G_CALLBACK (example_request_manager_start_activity_cb),
                    NULL);
  g_signal_connect (self, "handle-stop-activity",
                    G_CALLBACK (example_request_manager_stop_activity_cb),
                    NULL);
}

static void
example_request_manager_set_property (GObject *object,
                                      guint prop_id,
                                      const GValue *value,
                                      GParamSpec *pspec)
{
  ExampleRequestManager *self = EXAMPLE_REQUEST_MANAGER (object);

  switch ((Property) prop_id)
    {
    case PROP_COMPONENT_INDEX:
      /* construct-only */
      g_assert (self->component_index == NULL);
      self->component_index = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
example_request_manager_dispose (GObject *object)
{
  ExampleRequestManager *self = EXAMPLE_REQUEST_MANAGER (object);

  g_clear_object (&self->component_index);
  g_clear_pointer (&self->activities, g_hash_table_unref);

  G_OBJECT_CLASS (example_request_manager_parent_class)->dispose (object);
}

static void
example_request_manager_class_init (ExampleRequestManagerClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed = example_request_manager_constructed;
  object_class->dispose = example_request_manager_dispose;
  object_class->set_property = example_request_manager_set_property;

  /*
   * ExampleRequestManager:component-index:
   *
   * The component index in which this request manager will look up
   * the requesting application. If the requesting application is an
   * app-bundle, this is used to determine whether the role and priority
   * specified in the request are allowed.
   */
  property_specs[PROP_COMPONENT_INDEX] = g_param_spec_object (
      "component-index", "Component index", "Component index",
      CBY_TYPE_COMPONENT_INDEX, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (
      object_class, G_N_ELEMENTS (property_specs), property_specs);
}

/*
 * example_request_manager_new:
 *
 * Return a new request manager.
 *
 * Returns: (transfer full): the request manager
 */
ExampleRequestManager *
example_request_manager_new (CbyComponentIndex *component_index)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (component_index), NULL);

  return g_object_new (EXAMPLE_TYPE_REQUEST_MANAGER,
                       "component-index", component_index,
                       NULL);
}

/**
 * example_request_manager_count_activities:
 * @self: the request manager
 *
 * Return the number of activities currently in progress. This is used in the
 * regression test for this sample code, to assert that the expected number
 * of activities are present.
 *
 * Returns: the number of activities in progress
 */
guint
example_request_manager_count_activities (ExampleRequestManager *self)
{
  g_return_val_if_fail (EXAMPLE_IS_REQUEST_MANAGER (self), 0);

  return g_hash_table_size (self->activities);
}

/*
 * example_request_manager_get_message:
 * @self: the request manager
 * @activity_path: an object path that might have been returned by
 *  the StartActivity D-Bus method
 *
 * If @activity_path is the object path of an activity that has been started
 * and not stopped, return its associated human-readable message. Otherwise,
 * return %NULL.
 *
 * This method is used in the regression test for this sample code, to
 * assert that the expected activities are present.
 *
 * Returns: (transfer none) (nullable): the human-readable message associated
 *  with the activity with the given path, or %NULL if none
 */
const gchar *
example_request_manager_get_message (ExampleRequestManager *self,
                                     const gchar *activity_path)
{
  Activity *activity;

  g_return_val_if_fail (EXAMPLE_IS_REQUEST_MANAGER (self), NULL);

  activity = g_hash_table_lookup (self->activities, activity_path);

  if (activity == NULL)
    return NULL;

  return activity->message;
}

/*
 * example_request_manager_list_messages:
 * @self: the request manager
 *
 * List the human-readable message associated with each activity that is
 * currently in progress, in priority order with the most important first.
 *
 * Returns: (array zero-terminated=1) (element-type utf8) (transfer full): an
 *  array of human-readable messages
 */
GStrv
example_request_manager_list_messages (ExampleRequestManager *self)
{
  g_autoptr (GPtrArray) messages =
      g_ptr_array_new_full (self->sorted_activities.length + 1, g_free);
  GList *l;

  for (l = g_queue_peek_head_link (&self->sorted_activities);
       l != NULL;
       l = l->next)
    {
      Activity *a = l->data;

      g_ptr_array_add (messages, g_strdup (a->message));
    }

  g_ptr_array_add (messages, NULL);

  return (GStrv) g_ptr_array_free (g_steal_pointer (&messages), FALSE);
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <sys/types.h>
#include <unistd.h>

#include <glib.h>

#include <canterbury/canterbury.h>

#define HELPER \
  "/usr/Applications/org.apertis.CanterburyTests.Dconf/bin/dconf-helper"
#define SCHEMAS \
  "/usr/Applications/org.apertis.CanterburyTests.Dconf/share/glib-2.0/schemas"

static void
do_increment (const gchar *output_txt,
              int expected_old_value)
{
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *expected_output =
      g_strdup_printf ("Changed foo from %d to %d\n", expected_old_value,
                       expected_old_value + 1);
  g_autofree gchar *output = NULL;

  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error,
                        "canterbury-exec",
                        "--",
                        HELPER,
                        NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (subprocess, NULL, &error);
  g_assert_no_error (error);

  /* We can't use a pipe because canterbury-exec reopens stdout */
  g_file_get_contents (output_txt, &output, NULL, &error);
  g_assert_no_error (error);
  g_assert_cmpstr (output, ==, expected_output);
}

/* GSettingsSchemaSource isn't set up for g_autoptr yet */
typedef GSettingsSchemaSource AutoSchemaSource;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (AutoSchemaSource,
                               g_settings_schema_source_unref)

static void
test_dconf (void)
{
  g_autoptr (AutoSchemaSource) source = NULL;
  g_autoptr (CbyProcessInfo) helper = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autoptr (GSettingsSchema) schema = NULL;
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autofree gchar *config = NULL;
  g_autofree gchar *output_txt = NULL;

  if (getuid () == 0)
    {
      g_test_skip ("This test uses dconf and cannot be run as root");
      return;
    }

  helper = cby_process_info_new_for_path_and_user (HELPER, getuid ());
  output_txt =
      g_build_filename (cby_process_info_get_persistence_path (helper),
                        "data", "output.txt", NULL);
  config =
      g_build_filename (cby_process_info_get_persistence_path (helper),
                        "config", NULL);

  source =
      g_settings_schema_source_new_from_directory (SCHEMAS, NULL, TRUE, &error);
  g_assert_no_error (error);
  g_assert_nonnull (source);
  schema = g_settings_schema_source_lookup (source,
                                            "org.apertis.CanterburyTests.Dconf",
                                            FALSE);
  g_assert_nonnull (schema);
  settings = g_settings_new_full (schema, NULL,
                                  "/org/apertis/CanterburyTests/Dconf/");
  g_assert_nonnull (settings);
  g_settings_reset (settings, "foo");

  subprocess =
      g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error,
                        "rm",
                        "-fr",
                        config,
                        NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (subprocess, NULL, &error);
  g_assert_no_error (error);

  do_increment (output_txt, 0);
  do_increment (output_txt, 1);
  do_increment (output_txt, 2);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  /* This test is only usable when properly installed */
  g_assert_null (g_getenv ("CANTERBURY_TESTS_UNINSTALLED"));

  g_test_add_func ("/dconf/basic", test_dconf);

  return g_test_run ();
}

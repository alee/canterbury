/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <gio/gio.h>

#include <canterbury/canterbury.h>
#include "canterbury/messages-internal.h"

/*
 * activatable-helper — an app-bundle to be activated in service activation
 * test cases
 *
 * This has two entry points, like a miniature version of Frampton. It
 * pretends to be a graphical program but does not actually display
 * anything.
 */

#define BUNDLE_ID "org.apertis.CanterburyTests.Activatable"
#define OBJECT_PATH "/org/apertis/CanterburyTests/Activatable"
#define LOGGING_IFACE BUNDLE_ID ".Log"

static void
quit_activate_cb (GSimpleAction *quit_action,
                  GVariant *parameter G_GNUC_UNUSED,
                  gpointer user_data)
{
  MESSAGE ("Exiting due to \"quit\" action");
  g_application_quit (user_data);
}

static void
main_activate_cb (GApplication *main_app,
                  gpointer user_data G_GNUC_UNUSED)
{
  GDBusConnection *session_bus;
  const gchar *entry_point_id;

  session_bus = g_application_get_dbus_connection (main_app);
  g_assert (session_bus != NULL);
  entry_point_id = g_application_get_application_id (main_app);

  MESSAGE ("Main entry point activated");
  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedActivate",
                                 g_variant_new ("(s)", entry_point_id), NULL);
}

static void
main_open_cb (GApplication *main_app,
              GFile **files,
              gint n_files,
              const gchar *hint,
              gpointer user_data G_GNUC_UNUSED)
{
  GDBusConnection *session_bus;
  g_autoptr (GPtrArray) urls = g_ptr_array_new_with_free_func (g_free);
  const gchar *entry_point_id;
  gint i;

  session_bus = g_application_get_dbus_connection (main_app);
  g_assert (session_bus != NULL);
  entry_point_id = g_application_get_application_id (main_app);

  MESSAGE ("Main entry point received %d files", n_files);

  for (i = 0; i < n_files; i++)
    {
      g_ptr_array_add (urls, g_file_get_uri (files[i]));
      MESSAGE ("- %s", (const gchar *) g_ptr_array_index (urls, urls->len - 1));
    }

  g_ptr_array_add (urls, NULL);
  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedOpen",
                                 g_variant_new ("(s^as)", entry_point_id,
                                                urls->pdata),
                                 NULL);
}

static void
register_my_app_cb (GObject *source_object,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple =
    g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                   result, &error);

  if (tuple != NULL)
    MESSAGE ("RegisterMyApp() succeeded");
  else
    MESSAGE ("RegisterMyApp() failed: %s", error->message);
}

static gint
main_command_line_cb (GApplication *main_app,
                      GApplicationCommandLine *command_line,
                      gpointer user_data)
{
  gint argc;
  g_auto (GStrv) argv = NULL;
  GApplication *other_app = user_data;
  GDBusConnection *session_bus;
  const gchar *entry_point_id;

  session_bus = g_application_get_dbus_connection (main_app);
  g_assert (session_bus != NULL);
  entry_point_id = g_application_get_application_id (main_app);
  argv = g_application_command_line_get_arguments (command_line, &argc);

  if (argc > 2 && g_strcmp0 (argv[1], "--this-is") == 0 &&
      g_strcmp0 (argv[2], "the-other-one") == 0)
    {
      entry_point_id = g_application_get_application_id (other_app);
    }

  MESSAGE ("Received %d command-line arguments for %s", argc, entry_point_id);

  if ((g_application_get_flags (main_app) & G_APPLICATION_IS_SERVICE) == 0)
    {
      /* We were invoked other than via D-Bus activation, so let's work the
       * old way - use the historical Canterbury API. */

      /* For simplicity we do this with message-passing rather than making a
       * proxy */
      g_dbus_connection_call (session_bus, "org.apertis.Canterbury",
                              "/org/apertis/Canterbury/AppManager",
                              "org.apertis.Canterbury.AppManager",
                              "RegisterMyApp",
                              g_variant_new ("(st)", entry_point_id,
                                             G_GUINT64_CONSTANT (0)),
                              G_VARIANT_TYPE_UNIT,
                              G_DBUS_CALL_FLAGS_NONE,
                              -1,
                              NULL,
                              register_my_app_cb,
                              NULL);
    }

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedCommandLine",
                                 g_variant_new ("(s^as)", entry_point_id, argv),
                                 NULL);
  return 0;
}

static void
other_activate_cb (GApplication *other_app,
                   gpointer user_data)
{
  GApplication *main_app = G_APPLICATION (user_data);
  GDBusConnection *session_bus;
  const gchar *other_entry_point_id;

  session_bus = g_application_get_dbus_connection (main_app);
  g_assert (session_bus != NULL);
  other_entry_point_id = g_application_get_application_id (other_app);

  MESSAGE ("Secondary entry point %s activated", other_entry_point_id);

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedActivate",
                                 g_variant_new ("(s)",
                                                other_entry_point_id),
                                 NULL);
}

static void
new_app_state_cb (GDBusConnection *session_bus,
                  const gchar *sender,
                  const gchar *object_path,
                  const gchar *iface,
                  const gchar *signal,
                  GVariant *parameters,
                  gpointer user_data)
{
  g_autoptr (GPtrArray) arr = g_ptr_array_new_with_free_func (g_free);
  g_autoptr (GVariant) map = NULL;
  g_autofree gchar *entry_point_id = NULL;
  g_autofree gchar *k = NULL;
  g_autofree gchar *v = NULL;
  guint app_state;
  GVariantIter iter;

  if (!g_variant_is_of_type (parameters, G_VARIANT_TYPE ("(sua{ss})")))
    {
      WARNING ("Ignoring unexpected signature for NewAppState");
      return;
    }

  g_variant_get (parameters, "(su@a{ss})", &entry_point_id, &app_state, &map);

  g_variant_iter_init (&iter, map);

  while (g_variant_iter_next (&iter, "{ss}", &k, &v))
    {
      g_ptr_array_add (arr, g_steal_pointer (&k));
      g_ptr_array_add (arr, g_steal_pointer (&v));
    }

  g_ptr_array_add (arr, NULL);

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedNewAppState",
                                 g_variant_new ("(su^as)", entry_point_id,
                                                app_state, arr->pdata),
                                 NULL);
}

static guint main_subscription = 0;
static guint other_subscription = 0;

/*
 * Invoked when the primary instance has taken its bus name (and at no
 * other time).
 *
 * If a second instance is invoked, it results in command-line being emitted
 * in the primary instance (possibly for a second time).
 */
static void
main_startup_cb (GApplication *main_app,
                 gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *other_entry_point_id = NULL;
  GApplication *other_app = user_data;
  GDBusConnection *session_bus;
  const gchar *main_entry_point_id;

  MESSAGE ("Starting primary instance");

  session_bus = g_application_get_dbus_connection (main_app);
  g_assert (session_bus != NULL);
  main_entry_point_id = g_application_get_application_id (main_app);

  other_entry_point_id = g_strdup_printf ("%s.Other", main_entry_point_id);

  main_subscription =
    g_dbus_connection_signal_subscribe (session_bus,
                                        "org.apertis.Canterbury",
                                        "org.apertis.Canterbury.AppManager",
                                        "NewAppState",
                                        "/org/apertis/Canterbury/AppManager",
                                        main_entry_point_id,
                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                        new_app_state_cb, NULL, NULL);
  other_subscription =
    g_dbus_connection_signal_subscribe (session_bus,
                                        "org.apertis.Canterbury",
                                        "org.apertis.Canterbury.AppManager",
                                        "NewAppState",
                                        "/org/apertis/Canterbury/AppManager",
                                        other_entry_point_id,
                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                        new_app_state_cb, NULL, NULL);

  g_application_register (other_app, NULL, &error);
  g_assert_no_error (error);
}

int
main (int argc,
      char **argv)
{
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (GApplication) main_app = NULL;
  g_autoptr (GApplication) other_app = NULL;
  g_autoptr (GAction) quit_action = NULL;
  const gchar *bundle_id = cby_get_bundle_id ();
  const gchar *main_entry_point_id = "org.apertis.CanterburyTests.Activatable";
  g_autofree gchar *other_entry_point_id = NULL;
  g_autoptr (GError) error = NULL;
  int ret;

  MESSAGE ("Starting");

  if (bundle_id != NULL)
    main_entry_point_id = bundle_id;

  other_entry_point_id = g_strdup_printf ("%s.Other", main_entry_point_id);

  main_app = g_application_new (main_entry_point_id,
                                (G_APPLICATION_HANDLES_COMMAND_LINE |
                                 G_APPLICATION_HANDLES_OPEN));
  other_app = g_application_new (other_entry_point_id,
                                 G_APPLICATION_FLAGS_NONE);
  quit_action = G_ACTION (g_simple_action_new ("quit", NULL));

  g_application_hold (main_app);
  g_signal_connect_object (main_app, "startup",
                           G_CALLBACK (main_startup_cb), other_app, 0);
  g_signal_connect (main_app, "activate",
                    G_CALLBACK (main_activate_cb), NULL);
  g_signal_connect_object (main_app, "command-line",
                           G_CALLBACK (main_command_line_cb), other_app, 0);
  g_signal_connect (main_app, "open",
                    G_CALLBACK (main_open_cb), NULL);

  g_action_map_add_action (G_ACTION_MAP (main_app), quit_action);
  g_signal_connect_object (quit_action, "activate",
                           G_CALLBACK (quit_activate_cb), main_app, 0);

  g_signal_connect_object (other_app, "activate",
                           G_CALLBACK (other_activate_cb), main_app, 0);

  ret = g_application_run (main_app, argc, argv);
  g_application_release (main_app);

  session_bus = g_application_get_dbus_connection (main_app);

  if (session_bus != NULL)
    {
      if (main_subscription != 0)
        g_dbus_connection_signal_unsubscribe (session_bus, main_subscription);

      if (other_subscription != 0)
        g_dbus_connection_signal_unsubscribe (session_bus, other_subscription);
    }

  return ret;
}

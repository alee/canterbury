/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>

#include <canterbury/canterbury.h>

#include "tests/common.h"
#include "tests/process-info-generated.h"

typedef struct
{
  GError *error;
  GDBusConnection *conn;

  TestsProcessInfo *skel;

  GPid child_pid;
  gint child_status;

  GDBusMethodInvocation *invocation;
  gchar *claimed_apparmor_label;
  gchar *claimed_bundle_id;
  CbyProcessType claimed_process_type;
  guint32 claimed_user_id;

  GAsyncResult *result;
} Fixture;

typedef struct
{
  const gchar *executable;
  const gchar *expected_apparmor_label;
  const gchar *expected_bundle_id;
  CbyProcessType expected_process_type;
} Config;

static const char *
not_null (const char *s)
{
  return (s == NULL ? "(null)" : s);
}

/*
 * Check that the process-info test is installed in the expected location
 * and labelled with the expected AppArmor profile.
 *
 * During installed testing, this is required to be true.
 *
 * During unit testing ("make check"), this is expected to be false,
 * and many tests need to be skipped.
 *
 * Returns: %TRUE if the AppArmor profile is in place
 */
static gboolean
check_apparmor (void)
{
  const gchar *expected_label = g_test_get_filename (G_TEST_BUILT,
      "process-info.t", NULL);

  return tests_require_apparmor_label (expected_label);
}

/*
 * Called as a GDBus method implementation when the process-info-helper
 * child process calls back to the test. See test_invocation().
 */
static gboolean
hello_cb (TestsProcessInfo *iface,
    GDBusMethodInvocation *invocation,
    const gchar *apparmor_label,
    const gchar *bundle_id,
    guint32 process_type,
    guint32 user_id,
    gpointer user_data)
{
  Fixture *f = user_data;

  /* it should currently only be called once per test */
  g_assert (f->invocation == NULL);

  g_test_message ("called back by subprocess");
  g_test_message ("- it thinks its AppArmor label is \"%s\"", apparmor_label);

  /* record what it said it was */
  f->claimed_apparmor_label = g_strdup (apparmor_label);
  g_test_queue_free (f->claimed_apparmor_label);
  f->claimed_bundle_id = g_strdup (bundle_id);
  g_test_queue_free (f->claimed_bundle_id);
  f->claimed_process_type = process_type;
  f->claimed_user_id = user_id;

  /* don't reply to the method call right now so we can inspect it first */
  f->invocation = g_object_ref (invocation);
  g_test_queue_unref (f->invocation);
  return TRUE;
}

static void
setup (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (30);

  f->child_status = -1;

  f->conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &f->error);
  g_assert_no_error (f->error);
  g_test_queue_unref (f->conn);

  f->skel = tests_process_info_skeleton_new ();
  g_test_queue_unref (f->skel);
  g_signal_connect (f->skel, "handle-hello",
      G_CALLBACK (hello_cb), f);

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (f->skel),
      f->conn, "/org/apertis/CanterburyTests/ProcessInfo", &f->error);
  g_assert_no_error (f->error);
}

/*
 * During installed testing, check that cby_process_info_get_self()
 * returns details of the calling process.
 *
 * During unit testing ("make check") this is just skipped.
 */
static void
test_myself (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  CbyProcessInfo *me;
  gchar *apparmor_label;
  gchar *bundle_id;
  CbyProcessType process_type;
  guint user_id;
  const gchar *expected_label;

  if (!check_apparmor ())
    return;

  expected_label = g_test_get_filename (G_TEST_BUILT, "process-info.t", NULL);

  me = cby_process_info_get_self ();
  g_assert_true (CBY_IS_PROCESS_INFO (me));
  g_assert_cmpstr (cby_process_info_get_apparmor_label (me), ==,
      expected_label);
  g_assert_cmpstr (cby_process_info_get_bundle_id (me), ==, NULL);
  /* assume we're installed in /usr so we are part of the platform */
  g_assert_cmpint (cby_process_info_get_process_type (me), ==,
      CBY_PROCESS_TYPE_PLATFORM);
  g_assert_cmpuint (cby_process_info_get_user_id (me), ==, getuid ());

  /* there is only one object representing the process, cached globally */
  g_assert_true (me == cby_process_info_get_self ());

  g_object_get (me,
      "apparmor-label", &apparmor_label,
      "bundle-id", &bundle_id,
      "process-type", &process_type,
      "user-id", &user_id,
      NULL);
  g_assert_cmpstr (apparmor_label, ==, expected_label);
  g_assert_cmpstr (bundle_id, ==, NULL);
  g_assert_cmpint (process_type, ==, CBY_PROCESS_TYPE_PLATFORM);
  g_assert_cmpint (user_id, ==, getuid ());
  g_free (apparmor_label);
  g_free (bundle_id);
}

/*
 * @context: an AppArmor context
 * @label: the corresponding AppArmor label, or %NULL if @context is invalid
 * @expected_bundle_id: the bundle ID that should be parsed from @context
 *  and @label
 * @expected_process_type: the process type that should be parsed
 *  from @context and @label
 *
 * Test data for test_apparmor()
 */
static struct {
  const gchar *context;
  const gchar *label;
  const gchar *expected_bundle_id;
  CbyProcessType expected_process_type;
} apparmor_contexts[] = {
  { "", NULL, NULL, CBY_PROCESS_TYPE_UNKNOWN },
  { "foo", NULL, NULL, CBY_PROCESS_TYPE_UNKNOWN },
  { "unconfined", "unconfined", NULL, CBY_PROCESS_TYPE_PLATFORM },
  { "/usr/bin/foo (enforce)", "/usr/bin/foo", NULL,
    CBY_PROCESS_TYPE_PLATFORM },
  { "/usr/bin/foo (complain)", "/usr/bin/foo", NULL,
    CBY_PROCESS_TYPE_PLATFORM },
  { "/lib/x86_64-linux-gnu/foo (complain)",
    "/lib/x86_64-linux-gnu/foo", NULL,
    CBY_PROCESS_TYPE_PLATFORM },
  { "/Applications/com.example.App (complain)",
    "/Applications/com.example.App", "com.example.App",
    CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "/Applications/com.example.App/bin/sh (enforce)",
    "/Applications/com.example.App/bin/sh", "com.example.App",
    CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "/usr/Applications/com.example.App (enforce)",
    "/usr/Applications/com.example.App", "com.example.App",
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
  { "/usr/Applications/com.example.App/libexec/foobar (complain)",
    "/usr/Applications/com.example.App/libexec/foobar", "com.example.App",
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
};

/*
 * During installed testing or unit tests, check that
 * cby_process_info_new_for_apparmor_context() and
 * cby_process_info_new_for_apparmor_label() produce the intended results.
 */
static void
test_apparmor (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (apparmor_contexts); i++)
    {
      CbyProcessInfo *them;
      const gchar *non_null_label;
      gchar *apparmor_label;
      gchar *bundle_id;
      CbyProcessType process_type;
      guint user_id;

      g_test_message ("from context: \"%s\"", apparmor_contexts[i].context);
      them = cby_process_info_new_for_apparmor_context (
          apparmor_contexts[i].context);
      g_assert_true (CBY_IS_PROCESS_INFO (them));

      g_object_get (them,
          "apparmor-label", &apparmor_label,
          "bundle-id", &bundle_id,
          "process-type", &process_type,
          "user-id", &user_id,
          NULL);

      g_assert_cmpstr (apparmor_label, ==, apparmor_contexts[i].label);
      g_assert_cmpstr (bundle_id, ==, apparmor_contexts[i].expected_bundle_id);
      g_assert_cmpint (process_type, ==,
          apparmor_contexts[i].expected_process_type);
      g_assert_cmpint (user_id, ==, CBY_PROCESS_INFO_NO_USER_ID);

      /* accessors return the same thing as properties */
      g_assert_cmpstr (cby_process_info_get_apparmor_label (them),
          ==, apparmor_label);
      g_assert_cmpstr (cby_process_info_get_bundle_id (them), ==, bundle_id);
      g_assert_cmpint (cby_process_info_get_process_type (them),
          ==, process_type);
      g_assert_cmpuint (cby_process_info_get_user_id (them), ==, user_id);

      g_free (apparmor_label);
      g_free (bundle_id);
      g_object_unref (them);

      non_null_label = apparmor_contexts[i].label;

      if (non_null_label == NULL)
        non_null_label = "";

      g_test_message ("from label: \"%s\"", non_null_label);
      them = cby_process_info_new_for_apparmor_label (non_null_label);
      g_assert_true (CBY_IS_PROCESS_INFO (them));

      g_object_get (them,
          "apparmor-label", &apparmor_label,
          "bundle-id", &bundle_id,
          "process-type", &process_type,
          "user-id", &user_id,
          NULL);

      g_assert_cmpstr (apparmor_label, ==, apparmor_contexts[i].label);
      g_assert_cmpstr (bundle_id, ==, apparmor_contexts[i].expected_bundle_id);
      g_assert_cmpint (process_type, ==,
          apparmor_contexts[i].expected_process_type);
      g_assert_cmpint (user_id, ==, CBY_PROCESS_INFO_NO_USER_ID);
      /* we don't bother re-testing the accessors this time, the properties
       * are enough to be rather confident */

      g_free (apparmor_label);
      g_free (bundle_id);
      g_object_unref (them);
    }
}

/*
 * @path: an absolute path
 * @expected_bundle_id: the bundle ID that should be parsed from @path
 * @expected_process_type: the process type that should be parsed from @path
 *
 * Test data for test_path()
 */
static struct
{
  const gchar *path;
  const gchar *expected_bundle_id;
  CbyProcessType expected_process_type;
} paths[] = {
  { "/usr/bin/foo", NULL, CBY_PROCESS_TYPE_PLATFORM },
  { "/lib/x86_64-linux-gnu/foo", NULL, CBY_PROCESS_TYPE_PLATFORM },
  { "/Applications/com.example.App", "com.example.App",
    CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "/Applications/com.example.App/bin/sh", "com.example.App",
    CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "/usr/Applications/com.example.App", "com.example.App",
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
  { "/usr/Applications/com.example.App/libexec/foobar", "com.example.App",
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
};

/*
 * During installed testing or unit tests, check that
 * cby_process_info_new_for_path_and_user() produces the intended results.
 */
static void
test_path (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (paths); i++)
    {
      CbyProcessInfo *them;
      gchar *apparmor_label;
      gchar *bundle_id;
      CbyProcessType process_type;
      guint user_id;
      guint reference_user_id;

      /* Do one test as CBY_PROCESS_INFO_NO_USER_ID, and the rest with a
       * varying uid (== i) just to check that they're passed through
       * correctly. */
      reference_user_id = i;

      if (i == 0)
        reference_user_id = CBY_PROCESS_INFO_NO_USER_ID;

      g_test_message ("from path: \"%s\"", paths[i].path);
      them = cby_process_info_new_for_path_and_user (paths[i].path,
                                                     reference_user_id);
      g_assert_true (CBY_IS_PROCESS_INFO (them));

      g_object_get (them,
          "apparmor-label", &apparmor_label,
          "bundle-id", &bundle_id,
          "process-type", &process_type,
          "user-id", &user_id,
          NULL);

      g_assert_cmpstr (apparmor_label, ==, NULL);
      g_assert_cmpstr (bundle_id, ==, paths[i].expected_bundle_id);
      g_assert_cmpint (process_type, ==, paths[i].expected_process_type);
      g_assert_cmpint (user_id, ==, reference_user_id);

      g_assert_cmpstr (cby_process_info_get_apparmor_label (them), ==,
                       apparmor_label);
      g_assert_cmpstr (cby_process_info_get_bundle_id (them), ==, bundle_id);
      g_assert_cmpint (cby_process_info_get_process_type (them), ==,
                       process_type);
      g_assert_cmpuint (cby_process_info_get_user_id (them), ==, user_id);

      g_free (apparmor_label);
      g_free (bundle_id);
      g_object_unref (them);
    }
}

/*
 * During installed testing or unit tests, check that
 * invalid g_initable_new() uses are caught.
 */
static void
test_inconsistent (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  CbyProcessInfo *them = NULL;
  GError *error = NULL;

  them = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                         "apparmor-label", "/Applications/com.example.Foo",
                         "bundle-id", "org.example.Bar",
                         NULL);
  g_assert_error (error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT);
  g_assert (them == NULL);
  g_clear_error (&error);

  them = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                         "apparmor-label", "/Applications/com.example.Foo",
                         "process-type", CBY_PROCESS_TYPE_BUILT_IN_BUNDLE,
                         NULL);
  g_assert_error (error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT);
  g_assert (them == NULL);
  g_clear_error (&error);

  them = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                         "apparmor-label", "/Applications/com.example.Foo",
                         "process-type", CBY_PROCESS_TYPE_PLATFORM,
                         NULL);
  g_assert_error (error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT);
  g_assert (them == NULL);
  g_clear_error (&error);

  them = g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, &error,
                         "bundle-id", "com.example.Foo",
                         "process-type", CBY_PROCESS_TYPE_PLATFORM,
                         NULL);
  g_assert_error (error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT);
  g_assert (them == NULL);
  g_clear_error (&error);
}

/*
 * During installed testing or unit tests, check that
 * cby_process_info_get_persistence_path() produces the intended results.
 */
static void
test_persistence_path (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  CbyProcessInfo *them;

  /* Platform components don't have a persistence path */
  them = cby_process_info_new_for_path_and_user ("/usr/bin/canterbury", 1000);
  g_assert_cmpstr (cby_process_info_get_persistence_path (them), ==, NULL);
  g_object_unref (them);

  /* Built-in apps with an unspecified uid don't either */
  them = cby_process_info_new_for_path_and_user (
      "/usr/Applications/com.example.App", CBY_PROCESS_INFO_NO_USER_ID);
  g_assert_cmpstr (cby_process_info_get_persistence_path (them), ==, NULL);
  g_object_unref (them);

  /* Neither do store apps with an unspecified uid */
  them = cby_process_info_new_for_path_and_user (
      "/Applications/com.example.App", CBY_PROCESS_INFO_NO_USER_ID);
  g_assert_cmpstr (cby_process_info_get_persistence_path (them), ==, NULL);
  g_object_unref (them);

  /* Built-in apps with a specified uid do */
  them = cby_process_info_new_for_path_and_user (
      "/usr/Applications/com.example.App", 1000);
  g_assert_cmpstr (cby_process_info_get_persistence_path (them), ==,
                   "/var/Applications/com.example.App/users/1000");
  g_object_unref (them);

  /* Store apps with a specified uid do too */
  them = cby_process_info_new_for_path_and_user (
      "/Applications/com.example.App", 1000);
  g_assert_cmpstr (cby_process_info_get_persistence_path (them), ==,
                   "/var/Applications/com.example.App/users/1000");
  g_object_unref (them);
}

/*
 * During installed testing or unit tests, check that
 * cby_process_info_get_downloads_path() produces the intended results.
 */
static void
test_downloads_path (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyProcessInfo) store_app_bundle = cby_process_info_new_for_apparmor_label_and_user ("/Applications/com.example.Foo/bin/app", getuid ());
  g_autoptr (CbyProcessInfo) built_in_app_bundle = cby_process_info_new_for_apparmor_label_and_user ("/usr/Applications/com.example.Bar/bin/app", getuid ());
  g_autoptr (CbyProcessInfo) platform_component = cby_process_info_new_for_apparmor_label_and_user ("/usr/bin/canterbury", getuid ());
  g_autofree gchar *store_expected = g_strdup_printf ("/var/Applications/com.example.Foo/users/%u/downloads", getuid ());
  g_autofree gchar *built_in_expected = g_strdup_printf ("/var/Applications/com.example.Bar/users/%u/downloads", getuid ());
  const gchar *download_dir = NULL;
  g_autofree gchar *platform_expected = NULL;

  download_dir = g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD);
  if (download_dir != NULL)
    {
      platform_expected = g_build_filename (download_dir, "canterbury", NULL);
    }
  else
    {
      platform_expected = g_build_filename (g_get_home_dir (), "Downloads", "canterbury",
                                            NULL);
    }

  g_assert_cmpstr (cby_process_info_get_downloads_path (store_app_bundle), ==, store_expected);
  g_assert_cmpstr (cby_process_info_get_downloads_path (built_in_app_bundle), ==, built_in_expected);
  g_assert_cmpstr (cby_process_info_get_downloads_path (platform_component), ==, platform_expected);
}

/*
 * Test data for test_invocation(). process-info-helper-built-in-app
 * is a subprocess that calls our Hello method (hello_cb()). process-info's
 * AppArmor profile assigns it an AppArmor profile that makes it look
 * like part of a built-in application bundle.
 */
static Config config_built_in_app =
{
  "process-info-helper-built-in-app",
  "/usr/Applications/org.apertis.CanterburyTests.ProcessInfo.BuiltInApp/bin/service",
  "org.apertis.CanterburyTests.ProcessInfo.BuiltInApp",
  CBY_PROCESS_TYPE_BUILT_IN_BUNDLE
};

/*
 * Test data for test_invocation(). process-info-helper-built-in-app
 * is a subprocess that calls our Hello method (hello_cb()). process-info's
 * AppArmor profile lets it inherit process-info's own AppArmor profile,
 * which happens to be part of the platform.
 */
static Config config_inherit =
{
  "process-info-helper-inherit",
  /* it inherits our profile */
  "${G_TEST_BUILDDIR}/process-info.t",
  NULL,
  CBY_PROCESS_TYPE_PLATFORM
};

/*
 * Test data for test_invocation(). process-info-helper-platform
 * is a subprocess that calls our Hello method (hello_cb()). process-info's
 * AppArmor profile assigns it an AppArmor profile that makes it look
 * like part of the platform.
 */
static Config config_platform =
{
  "process-info-helper-platform",
  "${G_TEST_BUILDDIR}/process-info-helper-platform",
  NULL,
  CBY_PROCESS_TYPE_PLATFORM
};

/*
 * Test data for test_invocation(). process-info-helper-platform
 * is a subprocess that calls our Hello method (hello_cb()). process-info's
 * AppArmor profile assigns it an AppArmor profile that makes it look
 * like part of a store application bundle.
 */
static Config config_store_app =
{
  "process-info-helper-store-app",
  "/Applications/org.apertis.CanterburyTests.ProcessInfo.StoreApp/bin/main",
  "org.apertis.CanterburyTests.ProcessInfo.StoreApp",
  CBY_PROCESS_TYPE_STORE_BUNDLE
};

/*
 * Test data for test_invocation(). process-info-helper-platform
 * is a subprocess that calls our Hello method (hello_cb()). process-info's
 * AppArmor profile results in it being run unconfined.
 */
static Config config_unconfined =
{
  "process-info-helper-unconfined",
  "unconfined",
  NULL,
  CBY_PROCESS_TYPE_PLATFORM
};

/* Called after fork in the child */
static void
child_setup (gpointer user_data)
{
  /* Write the child's stdout to the parent's stderr to avoid interfering
   * with TAP structured output. */
  if (dup2 (STDERR_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
    g_error ("dup2: %s", g_strerror (errno));
}

static void
child_exit_cb (GPid pid,
    gint status,
    gpointer user_data)
{
  Fixture *f = user_data;

  f->child_status = status;
  g_spawn_close_pid (f->child_pid);
  f->child_pid = 0;
}

static void
store_result_cb (GObject *source_object,
    GAsyncResult *result,
    gpointer user_data)
{
  Fixture *f = user_data;

  f->result = g_object_ref (result);
  g_test_queue_unref (f->result);
}

/*
 * Test that CbyProcessInfo can be constructed from a GDBusMethodInvocation.
 *
 * To achieve this, we run one of several subprocesses named
 * process-info-helper-whatever. The subprocess calls back into this
 * process (via the Hello method), and we use the resulting
 * GDBusMethodInvocation to get its process info.
 *
 * The AppArmor profile for process-info itself causes those subprocesses to
 * be run under several different AppArmor profiles:
 * - unconfined
 * - part of the platform (using the "natural" filename = profile name
 *   equivalence that production software would normally have)
 * - inheriting process-info's own profile
 * - pretending to be part of a store app-bundle in /Applications
 * - pretending to be part of a built-in app-bundle in /usr/Applications
 *
 * This test requires special AppArmor profiles, so it is only run as
 * an installed-test. It is skipped when we run as a unit test in "make check".
 */
static void
test_invocation (Fixture *f,
    gconstpointer user_data)
{
  const Config *config = user_data;
  const gchar *argv[] = { "<executable>", "<unique name>", NULL };
  CbyProcessInfo *them;
  const gchar *expected_label;

  if (!check_apparmor ())
    return;

  argv[0] = g_test_get_filename (G_TEST_BUILT, config->executable, NULL);
  argv[1] = g_dbus_connection_get_unique_name (f->conn);

  expected_label = config->expected_apparmor_label;

  if (g_str_has_prefix (expected_label, "${G_TEST_BUILDDIR}/"))
    expected_label = g_test_get_filename (G_TEST_BUILT,
        expected_label + strlen ("${G_TEST_BUILDDIR}/"), NULL);

  /* Run a subprocess to call a D-Bus method on us. */
  g_test_message ("running subprocess \"%s\"", argv[0]);
  g_spawn_async (NULL, (gchar **) argv, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
      child_setup, NULL, &f->child_pid, &f->error);

  g_assert_cmpint (f->child_pid, !=, 0);
  g_child_watch_add (f->child_pid, child_exit_cb, f);

  while (f->invocation == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_process_info_new_for_dbus_invocation_async (f->invocation,
      NULL, store_result_cb, f);

  while (f->result == NULL)
    g_main_context_iteration (NULL, TRUE);

  them = cby_process_info_new_for_dbus_invocation_finish (f->result,
      &f->error);
  g_assert_no_error (f->error);

  g_assert_true (CBY_IS_PROCESS_INFO (them));
  g_assert_cmpstr (cby_process_info_get_apparmor_label (them), ==,
      expected_label);
  g_assert_cmpstr (cby_process_info_get_bundle_id (them), ==,
      config->expected_bundle_id);
  g_assert_cmpint (cby_process_info_get_process_type (them), ==,
      config->expected_process_type);
  g_assert_cmpuint (cby_process_info_get_user_id (them), ==, getuid ());

  g_assert_cmpstr (f->claimed_apparmor_label, ==, expected_label);
  g_assert_cmpstr (f->claimed_bundle_id, ==,
      not_null (config->expected_bundle_id));
  g_assert_cmpint (f->claimed_process_type, ==,
      config->expected_process_type);
  g_assert_cmpuint (f->claimed_user_id, ==, getuid ());

  /* the child still hasn't died */
  g_assert_cmpint (f->child_pid, !=, 0);

  tests_process_info_complete_hello (TESTS_PROCESS_INFO (f->skel),
      f->invocation);

  /* now the child exits */
  while (f->child_pid != 0)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpint (f->child_status, ==, 0);
}

static void
teardown (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  /* cancel watchdog */
  alarm (0);
}

int
main (int argc,
    char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/process-info/myself", Fixture, NULL,
      setup, test_myself, teardown);
  g_test_add ("/process-info/apparmor", Fixture, NULL,
      setup, test_apparmor, teardown);
  g_test_add ("/process-info/path", Fixture, NULL, setup, test_path, teardown);
  g_test_add ("/process-info/inconsistent", Fixture, NULL, setup,
              test_inconsistent, teardown);
  g_test_add ("/process-info/persistence_path", Fixture, NULL, setup,
              test_persistence_path, teardown);
  g_test_add ("/process-info/invocation/built-in-app", Fixture,
      &config_built_in_app, setup, test_invocation, teardown);
  g_test_add ("/process-info/invocation/inherit", Fixture,
      &config_inherit, setup, test_invocation, teardown);
  g_test_add ("/process-info/invocation/platform", Fixture,
      &config_platform, setup, test_invocation, teardown);
  g_test_add ("/process-info/invocation/store-app", Fixture,
      &config_store_app, setup, test_invocation, teardown);
  g_test_add ("/process-info/invocation/unconfined", Fixture,
      &config_unconfined, setup, test_invocation, teardown);
  g_test_add ("/process-info/downloads_path", Fixture, NULL, setup,
              test_downloads_path, teardown);

  return g_test_run ();
}

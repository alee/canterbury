/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "common.h"

#include <errno.h>
#include <fcntl.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gio/gio.h>
#include <gio/gunixfdlist.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <sys/apparmor.h>

#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"
#include "canterbury/platform/component-index-internal.h"
#include "canterbury/platform/dbus.h"
#include "src/apparmor.h"
#include "src/util.h"

/**
 * Format a string as if via g_strdup_printf() and mark it to be freed at the
 * end of the test.
 */
const gchar *
tests_printf (const gchar *format,
              ...)
{
  va_list ap;
  gchar *string;

  va_start (ap, format);
  string = g_strdup_vprintf (format, ap);
  va_end (ap);

  g_test_queue_free (string);
  return string;
}

/*
 * Check that the current test is installed in the expected location
 * and labelled with the expected AppArmor profile.
 *
 * During installed testing, this is required to be true.
 *
 * During unit testing ("make check"), this is expected to be false,
 * and many tests need to be skipped.
 *
 * Returns: %TRUE if the AppArmor profile is in place
 */
gboolean
tests_require_apparmor_label (const gchar *expected_label)
{
  g_autofree gchar *label = NULL;
  /* part of label, do not free */
  char *mode;
  g_autofree gchar *message = NULL;

  if (aa_getcon (&label, &mode) < 0)
    {
      message = g_strdup_printf ("AppArmor not installed or not working? (%s)",
                                 g_strerror (errno));
      goto error;
    }

  if (g_strcmp0 (label, expected_label) != 0)
    {
      message =
          g_strdup_printf ("This test case is only useful when installed as %s",
                           expected_label);
      goto error;
    }

  return TRUE;

error:
  if (g_getenv ("CANTERBURY_TESTS_UNINSTALLED") != NULL)
    {
      g_test_skip (message);
    }
  else
    {
      /* incorrectly-installed */
      WARNING ("%s", message);
      g_test_fail ();
    }

  return FALSE;
}

void
tests_temp_component_index_update (TestsTempComponentIndex *self,
                                   TestsRunUpdateComponentIndexMode mode)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *update_component_index = NULL;
  int exit_status;
  const gchar *argv[] = { "<tmpdir/canterbury-update-component-index>",
                          "--root", "<tmpdir>", "--platform", NULL };

  update_component_index = g_build_filename (
      self->tmpdir, "canterbury-update-component-index", NULL);
  argv[0] = update_component_index;
  argv[2] = self->tmpdir;

  if (mode == TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM)
    argv[3] = "--platform";
  else
    argv[3] = NULL;

  g_spawn_sync (NULL, (gchar **) argv, NULL, /* envp */
                G_SPAWN_DEFAULT, NULL, NULL, /* child setup */
                NULL,                        /* stdout */
                NULL,                        /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

void
tests_temp_component_index_setup (TestsTempComponentIndex *self)
{
  const gchar *argv[] = { "cp", "-R", "<source>", "<tmpdir>", NULL };
  g_autoptr (GError) error = NULL;
  g_autofree gchar *extensions_apps_path = NULL;
  g_autofree gchar *update_component_index = NULL;
  g_autofree gchar *update_component_index_dir = NULL;
  g_autofree gchar *update_component_index_libs = NULL;
  g_autofree gchar *temp_update_component_index = NULL;
  int exit_status;

  self->tmpdir = g_dir_make_tmp ("canterbury.XXXXXX", &error);
  g_assert_no_error (error);
  g_assert_nonnull (self->tmpdir);

  g_test_message ("%s: tmpdir: %s; copying from: %s",
                  G_STRFUNC, self->tmpdir, g_test_get_dir (G_TEST_DIST));

  self->platform_cache =
      g_build_filename (self->tmpdir, CBY_APPSTREAM_CACHE_PLATFORM, NULL);
  self->store_cache = g_build_filename (
      self->tmpdir, CBY_APPSTREAM_CACHE_INSTALLED_STORE, NULL);

  extensions_apps_path =
      g_build_filename (self->tmpdir, CBY_PATH_SYSTEM_EXTENSIONS, "applications", NULL);
  g_mkdir_with_parents (extensions_apps_path, 0755);

  /* Copy test data to the temporary root */
  argv[2] = "tests/usr";
  argv[3] = self->tmpdir;

  g_spawn_sync (g_test_get_dir (G_TEST_DIST), (gchar **) argv, NULL, /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  argv[2] = "tests/Applications";
  argv[3] = self->tmpdir;

  g_spawn_sync (g_test_get_dir (G_TEST_DIST), (gchar **) argv, NULL, /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  /* We need to copy the executable so that it won't be confined by its
   * AppArmor profile, which would prevent it from reading the test
   * directory */
  update_component_index =
      g_find_program_in_path ("canterbury-update-component-index");
  g_assert (update_component_index != NULL);
  argv[2] = update_component_index;
  argv[3] = self->tmpdir;

  g_spawn_sync (g_test_get_dir (G_TEST_DIST), (gchar **) argv, NULL, /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  /* copy the actual binaries and not just the libtool wrapper when
   * running `make check` */
  update_component_index_dir = g_path_get_dirname(update_component_index);
  update_component_index_libs = g_build_filename(update_component_index_dir, ".libs", NULL);
  if (g_file_test (update_component_index_libs, G_FILE_TEST_EXISTS))
    {
      argv[2] = update_component_index_libs;
      argv[3] = self->tmpdir;


      g_spawn_sync (g_test_get_dir (G_TEST_DIST), (gchar **) argv, NULL, /* envp */
                    G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                    NULL,                            /* stdout */
                    NULL,                            /* stderr */
                    &exit_status, &error);
      g_assert_no_error (error);
      g_spawn_check_exit_status (exit_status, &error);
      g_assert_no_error (error);
    }

  /* Compile the cache files in the temporary root */
  tests_temp_component_index_update (
      self, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);
  tests_temp_component_index_update (
      self, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_STORE);
}

void
tests_temp_component_index_teardown (TestsTempComponentIndex *self)
{
  g_autoptr (GError) error = NULL;
  gint exit_status;

  if (self->tmpdir != NULL)
    {
      /* We need to chmod it in case it's a copy of the read-only data
       * during distcheck. */
      const gchar *argv[] = { "chmod", "-R", "u+w", "<tmpdir>", NULL };

      argv[3] = self->tmpdir;

      g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                    G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                    NULL,                            /* stdout */
                    NULL,                            /* stderr */
                    &exit_status, &error);
      g_assert_no_error (error);
      g_spawn_check_exit_status (exit_status, &error);
      g_assert_no_error (error);

      argv[0] = "rm";
      argv[1] = "-fr";
      argv[2] = self->tmpdir;
      argv[3] = NULL;

      g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                    G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                    NULL,                            /* stdout */
                    NULL,                            /* stderr */
                    &exit_status, &error);
      g_assert_no_error (error);
      g_spawn_check_exit_status (exit_status, &error);
      g_assert_no_error (error);
    }
}

/**
 * @source_object: unused
 * @result: an async result
 * @user_data: a `GAsyncResult **`
 *
 * A GAsyncReadyCallback that writes the @result through the
 * GAsyncResult ** @user_data.
 */
void
tests_store_result_cb (GObject *source_object G_GNUC_UNUSED,
    GAsyncResult *result,
    gpointer user_data)
{
  GAsyncResult **result_p = user_data;

  g_assert (*result_p == NULL);
  *result_p = g_object_ref (result);
}

/**
 * @ordinary_uid: the uid to use in bundle->persistence_path,
 *  or #CBY_PROCESS_INFO_NO_USER_ID to skip
 */
gboolean
tests_app_bundle_set_up (TestsAppBundle *bundle,
                         GDBusProxy *app_store_proxy,
                         GDBusProxy *bundle_manager1_proxy,
                         const gchar *bundle_id,
                         guint ordinary_uid,
                         const gchar * const *version_numbers,
                         gsize n_versions)
{
  g_autoptr (GDateTime) now = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *static_path = NULL;
  g_autofree gchar *date = NULL;
  gsize i;
  static int counter = 0;

  /* root is not a suitable uid */
  g_assert (ordinary_uid > 0);

  if (app_store_proxy != NULL)
    bundle->app_store_proxy = g_object_ref (app_store_proxy);

  bundle->bundle_manager1_proxy = g_object_ref (bundle_manager1_proxy);
  bundle->system_bus =
      g_object_ref (g_dbus_proxy_get_connection (bundle_manager1_proxy));
  bundle->ribchester_bus_name = g_dbus_proxy_get_name (bundle_manager1_proxy);

  if (bundle_id != NULL)
    {
      bundle->id = g_strdup (bundle_id);
    }
  else
    {
      /* These integration tests need to operate in shared storage, so we
       * need a unique bundle name.
       *
       * Use the date and time so it's easy to recognise and clear out old
       * tests; to get uniqueness, we use the monotonic time to avoid collision
       * between processes (we assume each one takes at least a microsecond),
       * combined with a per-process counter (in case a process generates
       * several IDs within the same microsecond). */
      now = g_date_time_new_now_local ();
      /* e.g. 20160119_191523 */
      date = g_date_time_format (now, "%Y%m%d_%H%M%S");
      bundle->id =
        g_strdup_printf ("com.example.CanterburyTests._%s_%" G_GINT64_FORMAT "_%d",
                         date, g_get_monotonic_time (), counter++);

      g_test_message ("Using app-bundle ID \"%s\"", bundle->id);
    }

  bundle->versions = g_new0 (TestsVersion, n_versions);
  bundle->n_versions = n_versions;

  for (i = 0; i < n_versions; i++)
    {
      const gchar *number = version_numbers[i];
      TestsVersion *version = &bundle->versions[i];

      version->number = g_strdup (number);
    }

  static_path = g_strdup_printf ("%s/%s", CBY_PATH_PREFIX_STORE_BUNDLE,
                                 bundle->id);
  bundle->process_info =
    cby_process_info_new_for_path_and_user (static_path, ordinary_uid);
  g_assert_cmpuint (cby_process_info_get_user_id (bundle->process_info), ==,
                    ordinary_uid);
  g_assert_cmpstr (cby_process_info_get_bundle_id (bundle->process_info), ==,
                   bundle->id);

  if (!tests_app_bundle_try_remove (bundle, &error))
    {
      if (g_error_matches (error, G_DBUS_ERROR, G_DBUS_ERROR_ACCESS_DENIED))
        {
          /* We can't do app-bundle-based tests as this user in this state
           * (most likely they are not considered to be actively logged-in
           * because we are using su to switch from root to an ordinary
           * user) */
          g_test_skip ("AccessDenied error while trying to remove app-bundle");
          return FALSE;
        }
      else
        {
          /* Other errors are fatal */
          g_test_message ("Unable to remove app-bundle: %s:%d: %s",
                          g_quark_to_string (error->domain), error->code,
                          error->message);
          g_test_fail ();
          return FALSE;
        }
    }

  return TRUE;
}

void
tests_app_bundle_tear_down (TestsAppBundle *self)
{
  gsize i;

  if (self->bundle_manager1_proxy != NULL)
    {
      g_autoptr (GError) error = NULL;

      /* Just ignore AccessDenied here: if we weren't allowed to remove it
       * then we wouldn't have been allowed to install it either */
      if (!tests_app_bundle_try_remove (self, &error) &&
          !g_error_matches (error, G_DBUS_ERROR, G_DBUS_ERROR_ACCESS_DENIED))
        {
          g_test_message ("Unable to remove app-bundle: %s:%d: %s",
                          g_quark_to_string (error->domain), error->code,
                          error->message);
          g_test_fail ();
        }
    }

  for (i = 0; i < self->n_versions; i++)
    {
      TestsVersion *version = &self->versions[i];

      g_clear_pointer (&version->number, g_free);
    }

  g_clear_pointer (&self->id, g_free);
  g_clear_object (&self->process_info);
  g_clear_object (&self->app_store_proxy);
  g_clear_object (&self->bundle_manager1_proxy);
  g_clear_object (&self->system_bus);
}

gboolean
tests_require_apparmor_active (void)
{
  if (!aa_is_enabled ())
   {
     int saved_errno = errno;

     g_test_message ("AppArmor not active: %s", g_strerror (saved_errno));
     g_test_skip ("This test requires AppArmor to be active");
     return FALSE;
   }

  return TRUE;
}

gboolean
tests_require_running_as_root (void)
{
  if (getuid () != 0)
   {
     g_test_skip ("This test requires root privileges");
     return FALSE;
   }

  return TRUE;
}

/**
 * If we are running as an ordinary user, put our own uid and gid
 * in @ordinary_uid and @ordinary_gid. If we are running as root (uid 0),
 * put the uid and gid of `user` there instead.
 *
 * Either way, put the uid and gid of an unused non-root user (`bin`)
 * in @unrelated_uid and @unrelated_gid.
 *
 * If we do not seem to be running on Apertis, mark the test skipped
 * and return %FALSE.
 */
gboolean
tests_require_ordinary_uid (guint *ordinary_uid,
                            guint *ordinary_gid,
                            guint *unrelated_uid,
                            guint *unrelated_gid)
{
  uid_t uid = getuid ();
  struct passwd *pwd;

  /* We make this assumption here, as does Canterbury */
  G_STATIC_ASSERT (sizeof (uid_t) <= sizeof (guint));

  if (unrelated_uid != NULL || unrelated_gid != NULL)
    {
      pwd = getpwnam ("bin");

      /* Apertis is a Debian derivative, and user 'bin' has uid 2
       * (this is centrally allocated in base-passwd). 'bin' is chosen to be
       * a harmless user that cannot actually log in. */
      if (pwd == NULL || pwd->pw_uid != 2)
        {
          g_test_skip ("This does not appear to be an Apertis environment");
          return FALSE;
        }

      g_assert_cmpint (pwd->pw_uid, >, 0);
      g_assert_cmpint (pwd->pw_uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (pwd->pw_gid, >, 0);

      if (unrelated_uid != NULL)
        *unrelated_uid = pwd->pw_uid;

      if (unrelated_gid != NULL)
        *unrelated_gid = pwd->pw_gid;
    }

  if (uid == 0)
    {
      pwd = getpwnam ("user");

      /* This is hard-coded in Apertis image creation. */
      if (pwd == NULL || pwd->pw_uid != 1000)
        {
          g_test_skip ("This does not appear to be an Apertis environment");
          return FALSE;
        }

      g_assert_cmpint (pwd->pw_uid, >, 0);
      g_assert_cmpint (pwd->pw_uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (pwd->pw_gid, >, 0);

      if (ordinary_uid != NULL)
        *ordinary_uid = (guint) pwd->pw_uid;

      if (ordinary_gid != NULL)
        *ordinary_gid = (guint) pwd->pw_gid;
    }
  else
    {
      gid_t gid = getgid ();

      g_assert_cmpint (uid, >, 0);
      g_assert_cmpint (uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (gid, >, 0);

      if (ordinary_uid != NULL)
        *ordinary_uid = (guint) uid;

      if (ordinary_gid != NULL)
        *ordinary_gid = (guint) gid;
    }

  return TRUE;
}

/* This is hard-coded here to avoid a circular dependency between canterbury
 * and ribchester */
#define RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED 50

static void
ribchester_appeared_cb (GDBusConnection *connection,
                        const gchar *name,
                        const gchar *owner,
                        gpointer user_data)
{
  gboolean *done = user_data;

  g_test_message ("Found Ribchester at \"%s\"", owner);
  *done = TRUE;
}

static void
ribchester_vanished_cb (GDBusConnection *connection,
                        const gchar *name,
                        gpointer user_data)
{
  gboolean *done = user_data;
  const gchar *message =
      "Ribchester is not running, try: "
      "sudo systemctl start ribchester.service";

  if (g_getenv ("CANTERBURY_TESTS_UNINSTALLED") != NULL)
    {
      g_test_skip (message);
    }
  else
    {
      /* incorrectly-installed */
      WARNING ("%s", message);
      g_test_fail ();
    }

  *done = TRUE;
}

/**
 * @bus_out: (out) (transfer none) (optional): the connection to the system bus
 * @app_store_proxy_out: (out) (transfer none) (optional): the AppStore
 *  interface proxy
 * @bundle_manager_proxy_out: (out) (transfer none) (optional): the
 *  BundleManager1 interface proxy
 */
gboolean
tests_require_ribchester (GDBusConnection **bus_out,
                          GDBusProxy **app_store_proxy_out,
                          GDBusProxy **bundle_manager1_proxy_out)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GDBusConnection) bus = NULL;
  g_autoptr (GDBusProxy) app_store_proxy = NULL;
  g_autoptr (GDBusProxy) bundle_manager1_proxy = NULL;
  guint watch_id;
  gboolean done = FALSE;

  bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);

  if (error != NULL && g_getenv ("CANTERBURY_TESTS_UNINSTALLED") != NULL)
    {
      g_test_message ("%s", error->message);
      g_test_skip ("System bus not available");
      return FALSE;
    }

  g_assert_no_error (error);
  g_assert_true (G_IS_DBUS_CONNECTION (bus));

  watch_id = g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                               CBY_SYSTEM_BUS_NAME_RIBCHESTER,
                               G_BUS_NAME_WATCHER_FLAGS_NONE,
                               ribchester_appeared_cb, ribchester_vanished_cb,
                               &done, NULL);

  while (!done)
    g_main_context_iteration (NULL, TRUE);

  g_bus_unwatch_name (watch_id);

  /* if we couldn't get the name, give up */
  if (g_test_failed ())
    return FALSE;

  g_dbus_proxy_new (bus, G_DBUS_PROXY_FLAGS_NONE, NULL,
                    CBY_SYSTEM_BUS_NAME_RIBCHESTER,
                    "/org/apertis/Ribchester/AppStore",
                    "org.apertis.Ribchester.AppStore",
                    NULL, tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  app_store_proxy = g_dbus_proxy_new_finish (result, &error);
  g_assert_no_error (error);
  g_assert_true (G_IS_DBUS_PROXY (app_store_proxy));
  g_clear_object (&result);

  g_dbus_proxy_new (bus, G_DBUS_PROXY_FLAGS_NONE, NULL,
                    CBY_SYSTEM_BUS_NAME_RIBCHESTER,
                    "/org/apertis/Ribchester/BundleManager1",
                    "org.apertis.Ribchester.BundleManager1",
                    NULL, tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  bundle_manager1_proxy = g_dbus_proxy_new_finish (result, &error);
  g_assert_no_error (error);
  g_assert_true (G_IS_DBUS_PROXY (bundle_manager1_proxy));
  g_clear_object (&result);

  if (bus_out != NULL)
    *bus_out = g_steal_pointer (&bus);

  if (app_store_proxy_out != NULL)
    *app_store_proxy_out = g_steal_pointer (&app_store_proxy);

  if (bundle_manager1_proxy_out != NULL)
    *bundle_manager1_proxy_out = g_steal_pointer (&bundle_manager1_proxy);

  return TRUE;
}

/*
 * @from_version: (nullable): the version we are upgrading from, or %NULL
 *  if this is an initial installation
 *
 * Returns: (transfer full):
 */
gchar *
tests_app_bundle_begin_install (TestsAppBundle *self,
                                const TestsVersion *from_version,
                                const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *unpack_to = NULL;

  g_return_val_if_fail (to_version != NULL, NULL);

  if (from_version != NULL)
    {
      g_test_message ("Beginning upgrade of \"%s\" from version \"%s\" to "
                      "\"%s\"...",
                      self->id, from_version->number, to_version->number);
    }
  else
    {
      g_test_message ("Beginning first installation of \"%s\", version "
                      "\"%s\"...",
                      self->id, to_version->number);
    }

  g_dbus_proxy_call (self->app_store_proxy, "BeginInstall",
                     g_variant_new ("(ssu)", self->id, to_version->number, 0),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "(s)");
  g_variant_get (tuple, "(s)", &unpack_to);
  g_assert_cmpstr (unpack_to, >, "");

  g_test_message ("... created snapshot, expecting new app in \"%s\"",
                  unpack_to);

  tests_assert_type_nofollow (unpack_to, S_IFDIR);

  return g_steal_pointer (&unpack_to);
}

void
tests_app_bundle_commit_install (TestsAppBundle *self,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;

  g_return_if_fail (to_version != NULL);
  g_return_if_fail (G_IS_DBUS_PROXY (self->app_store_proxy));

  g_test_message ("Committing installation of \"%s\" version \"%s\"...",
                  self->id, to_version->number);

  g_dbus_proxy_call (self->app_store_proxy, "CommitInstall",
                     g_variant_new ("(ss)", self->id, to_version->number),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "()");

  g_test_message ("... done");
}

void
tests_app_bundle_roll_back (TestsAppBundle *self,
                            TestsRollbackMode mode,
                            const TestsVersion *from_version,
                            const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *returned_to = NULL;

  g_return_if_fail (from_version != NULL);
  g_return_if_fail (G_IS_DBUS_PROXY (self->app_store_proxy));

  /* We can either roll back an installation that is in progress (the
   * temporary subvolume exists), or an upgrade that was already committed
   * (the rollback subvolumes exist) */
  switch (mode)
    {
    case TESTS_ROLLBACK_MODE_CANCEL_INSTALL:
      g_test_message ("Cancelling installation of \"%s\"...", self->id);
      break;

    case TESTS_ROLLBACK_MODE_ROLL_BACK:
      g_assert_true (to_version != NULL);

      g_test_message ("Rolling back \"%s\" to previous version...", self->id);
      break;

    default:
      g_return_if_reached ();
    }

  g_dbus_proxy_call (self->app_store_proxy, "RollBack",
                     g_variant_new ("(s)", self->id),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "(s)");
  g_variant_get (tuple, "(s)", &returned_to);
  g_assert_cmpstr (returned_to, !=, NULL);

  switch (mode)
    {
    case TESTS_ROLLBACK_MODE_CANCEL_INSTALL:

      if (to_version == NULL)
        {
          g_assert_cmpstr (returned_to, ==, "");
          g_test_message ("... aborted installation");
        }
      else
        {
          g_assert_cmpstr (returned_to, ==, to_version->number);
          g_test_message ("... aborted upgrade and returned to \"%s\"",
                          returned_to);
        }
      break;

    case TESTS_ROLLBACK_MODE_ROLL_BACK:
      g_test_message ("... restored snapshot version \"%s\"", returned_to);
      g_assert_cmpstr (returned_to, ==, to_version->number);
      break;

    default:
      g_return_if_reached ();
    }
}

void
tests_app_bundle_uninstall (TestsAppBundle *self,
                            const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;
  gboolean status;
  guint error_code;

  g_return_if_fail (G_IS_DBUS_PROXY (self->app_store_proxy));

  g_test_message ("Uninstalling \"%s\"...", self->id);

  g_dbus_proxy_call (self->app_store_proxy, "UninstallApp",
                     g_variant_new ("(s)", self->id),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "(bu)");
  g_variant_get (tuple, "(bu)", &status, &error_code);
  g_assert_cmpuint (error_code, ==, 0);
  g_assert_cmpint (status, ==, TRUE);
  g_test_message ("... done");
}

void
tests_app_bundle_reinstall (TestsAppBundle *self,
                            const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;
  gboolean status;
  guint error_code;

  g_return_if_fail (G_IS_DBUS_PROXY (self->app_store_proxy));

  g_test_message ("Reinstalling \"%s\"...", self->id);

  g_dbus_proxy_call (self->app_store_proxy, "ReInstallApp",
                     g_variant_new ("(s)", self->id),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "(bu)");
  g_variant_get (tuple, "(bu)", &status, &error_code);
  g_assert_cmpuint (error_code, ==, 0);
  g_assert_cmpint (status, ==, TRUE);
  g_test_message ("... done");
}

gboolean
tests_app_bundle_try_remove (TestsAppBundle *self,
                             GError **error)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) local_error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  g_test_message ("Permanently removing \"%s\"...", self->id);

  g_dbus_proxy_call (self->bundle_manager1_proxy, "RemoveBundle",
                     g_variant_new ("(s)", self->id),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->bundle_manager1_proxy, result,
                                    &local_error);

  if (local_error == NULL)
    {
      g_assert_nonnull (tuple);
      g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "()");

      g_test_message ("... done");
      return TRUE;
    }
  else
    {
      g_test_message ("... failed (%s:%d: %s)",
                      g_quark_to_string (local_error->domain),
                      local_error->code,
                      local_error->message);
      g_propagate_error (error, g_steal_pointer (&local_error));
      return FALSE;
    }
}

void
tests_app_bundle_remove (TestsAppBundle *self)
{
  g_autoptr (GError) error = NULL;
  gboolean ok;

  ok = tests_app_bundle_try_remove (self, &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
}

void
tests_app_bundle_delete_rollback_snapshot (TestsAppBundle *self,
                                           const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;

  g_return_if_fail (G_IS_DBUS_PROXY (self->app_store_proxy));

  g_test_message ("Removing ability to roll back \"%s\" to \"%s\"...",
                  self->id, version->number);

  g_dbus_proxy_call (self->app_store_proxy, "DeleteRollbackSnapshot",
                     g_variant_new ("(s)", self->id),
                     G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                     tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_proxy_call_finish (self->app_store_proxy, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (tuple);
  g_assert_cmpstr (g_variant_get_type_string (tuple), ==, "()");
  g_test_message ("... done");
}

gboolean
tests_remove_apparmor_profile (const gchar *name,
                               GError **error)
{
  g_autoptr (aa_kernel_interface) iface;

  if (aa_kernel_interface_new (&iface, NULL, NULL) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open kernel AppArmor interface: %s",
                   g_strerror (saved_errno));
      return FALSE;
    }

  if (aa_kernel_interface_remove_policy (iface, name) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open kernel AppArmor interface: %s",
                   g_strerror (saved_errno));
      return FALSE;
    }

  return TRUE;
}

/*
 * Assert that @path is a symbolic link to @expected_target.
 */
void
tests_assert_symlink_to (const gchar *path,
                         const gchar *expected_target)
{
  char buf[4096];
  ssize_t len;

  /* If this assertion fails, enlarge buf and recompile (this is just test
   * code so I'm not bothering to do a proper reallocation loop here). */
  g_return_if_fail (strlen (expected_target) < sizeof (buf));

  len = readlink (path, buf, sizeof (buf));

  if (len < 0)
    {
      int saved_errno = errno;

      g_warning ("readlink(\"%s\"): expected \"%s\", but got error: %s",
                 path, expected_target, g_strerror (saved_errno));
      g_test_fail ();
      return;
    }

  if (sizeof (buf) <= (size_t) len)
    {
      g_warning ("readlink(\"%s\"): expected \"%s\", but got %" G_GSSIZE_FORMAT
                 " bytes",
                 path, expected_target, len);
      g_test_fail ();
      return;
    }

  /* Yes, readlink really makes you do this yourself :-( */
  buf[len] = '\0';

  if (strcmp (buf, expected_target) != 0)
    {
      g_warning ("readlink(\"%s\"): expected \"%s\", but got \"%s\"",
                 path, expected_target, buf);
      g_test_fail ();
      return;
    }

  g_test_message ("readlink(\"%s\") returned \"%s\" as expected",
                  path, buf);
}

/*
 * Assert that @path does not exist. Unlike %G_FILE_TEST_EXISTS, this also
 * asserts that @path is not a dangling symlink.
 */
void
tests_assert_no_file (const gchar *path)
{
  GStatBuf stat_buf;
  int saved_errno;

  if (g_lstat (path, &stat_buf) == 0)
    {
      g_warning ("g_lstat(\"%s\") should not have succeeded", path);
      g_test_fail ();
      return;
    }

  saved_errno = errno;

  if (saved_errno != ENOENT)
    {
      g_warning ("Expected g_lstat(\"%s\") to fail with ENOENT, but got: %s",
                 path, g_strerror (saved_errno));
      g_test_fail ();
      return;
    }

  g_test_message ("g_lstat(\"%s\") failed with ENOENT as expected", path);
}

/*
 * Assert that @path's `st_mode & S_IFMT` is as desired, typically %S_IFDIR or
 * %S_IFREG. Unlike g_file_test(), this does not follow symlinks.
 */
void
tests_assert_type_nofollow (const gchar *path,
                            unsigned int s_ifmt)
{
  GStatBuf stat_buf;

  if (g_lstat (path, &stat_buf) != 0)
    {
      int saved_errno = errno;

      g_warning ("g_lstat(\"%s\") should have succeeded but got: %s",
                 path, g_strerror (saved_errno));
      g_test_fail ();
      return;
    }

  if ((stat_buf.st_mode & S_IFMT) != s_ifmt)
    {
      g_warning ("g_lstat(\"%s\").st_mode & S_IFMT: expected 0o%o, got 0o%o",
                 path, s_ifmt, (stat_buf.st_mode & S_IFMT));
      g_test_fail ();
      return;
    }

  g_test_message ("g_lstat(\"%s\").st_mode & S_IFMT: got 0o%o as expected",
                  path, s_ifmt);
}

static void
name_appeared_cb (GDBusConnection *bus,
                  const gchar *name,
                  const gchar *owner,
                  gpointer user_data)
{
  gchar **owner_out = user_data;

  g_free (*owner_out);
  *owner_out = g_strdup (owner);
}

static void
name_vanished_cb (GDBusConnection *bus,
                  const gchar *name,
                  gpointer user_data)
{
  gchar **owner_out = user_data;

  g_free (*owner_out);
  *owner_out = g_strdup ("");
}

void
tests_wait_for_no_name_owner (GDBusConnection *bus,
                              const gchar *name,
                              guint timeout_sec)
{
  g_auto (TestsNameWatch) watch = 0;
  g_auto (TestsScopedTimeout) timeout = 0;
  /* NULL until status is known, then "" or non-empty owner */
  g_autofree gchar *owner = NULL;

  if (timeout_sec != 0)
    tests_scoped_timeout_start (&timeout, timeout_sec);

  watch = g_bus_watch_name_on_connection (bus, name,
                                          G_BUS_NAME_WATCHER_FLAGS_NONE,
                                          name_appeared_cb,
                                          name_vanished_cb,
                                          &owner, NULL);

  /* Wait for the owner to be known to be "no owner" */
  while (owner == NULL || owner[0] != '\0')
    g_main_context_iteration (NULL, TRUE);
}

void
tests_wait_for_name_owner (GDBusConnection *bus,
                           const gchar *name,
                           guint timeout_sec,
                           gchar **name_owner_out)
{
  g_auto (TestsNameWatch) watch = 0;
  g_auto (TestsScopedTimeout) timeout = 0;
  /* NULL until status is known, then "" or non-empty owner */
  g_autofree gchar *owner = NULL;

  if (timeout_sec != 0)
    tests_scoped_timeout_start (&timeout, timeout_sec);

  watch = g_bus_watch_name_on_connection (bus, name,
                                          G_BUS_NAME_WATCHER_FLAGS_NONE,
                                          name_appeared_cb,
                                          name_vanished_cb,
                                          &owner, NULL);

  /* Wait for the owner to be known to be non-empty */
  while (owner == NULL || owner[0] == '\0')
    g_main_context_iteration (NULL, TRUE);

  if (name_owner_out != NULL)
    *name_owner_out = g_steal_pointer (&owner);
}

/*
 * tests_app_bundle_begin_install_bundle:
 * @self: The app-bundle
 * @from_version: (nullable): the version to upgrade from, or %NULL for a
 *  new installation
 * @to_version: the version to install or upgrade to
 *
 * Install the app-bundle from a .bundle file. The caller is expected to wait
 * for the resulting transaction to finish.
 *
 * Returns: the object path of the transaction
 */
static gchar *
tests_app_bundle_begin_install_bundle (TestsAppBundle *self,
                                       const TestsVersion *from_version,
                                       const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GUnixFDList) fd_list = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autofree gchar *basename =
    g_strconcat (self->id, "-", to_version->number, ".bundle", NULL);
  g_autofree gchar *bundle =
    g_test_build_filename (G_TEST_BUILT, "tests", basename, NULL);
  g_autofree gchar *path = NULL;
  g_auto (CbyFileDescriptor) fd = -1;
  gint handle;

  g_test_message ("Installing \"%s\" version \"%s\" from \"%s\"...",
                  self->id, to_version->number, bundle);

  fd = open (bundle, O_RDONLY | O_CLOEXEC);
  /* Using string comparison here so we get a better assertion message */
  g_assert_cmpstr (fd >= 0 ? NULL : g_strerror (errno), ==, NULL);

  fd_list = g_unix_fd_list_new ();
  handle = g_unix_fd_list_append (fd_list, fd, NULL);
  g_assert_cmpint (handle, ==, 0);

  /* We do this the hard way (without using gdbus-codegen) to avoid a
   * circular build-dependency between Ribchester and Canterbury. */
  tuple = g_dbus_proxy_call_with_unix_fd_list_sync (self->bundle_manager1_proxy,
                                                    "InstallBundle",
                                                    g_variant_new ("(h)",
                                                                   handle),
                                                    G_DBUS_CALL_FLAGS_NONE,
                                                    -1, fd_list, NULL, NULL,
                                                    &error);
  g_assert_no_error (error);
  g_variant_get (tuple, "(o)", &path);

  g_test_message ("Transaction object path: %s", path);

  return g_steal_pointer (&path);
}

/*
 * tests_app_bundle_install_bundle:
 * @self: The app-bundle
 * @from_version: (nullable): the version to upgrade from, or %NULL for a
 *  new installation
 * @to_version: the version to install or upgrade to
 *
 * Install the app-bundle from a .bundle file and wait for the resulting
 * transaction to succeed.
 */
void
tests_app_bundle_install_bundle (TestsAppBundle *self,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GVariant) bundle_id = NULL;
  g_autoptr (GVariant) got_version = NULL;
  g_autoptr (GDBusProxy) transaction = NULL;
  gint32 stage;

  path = tests_app_bundle_begin_install_bundle (self, from_version, to_version);
  g_assert (path != NULL);

  g_test_message ("Waiting for \"%s\" version \"%s\" transaction to finish...",
                  self->id, to_version->number);

  /* We do this the hard way (without using gdbus-codegen) to avoid a
   * circular build-dependency between Ribchester and Canterbury. */
  transaction =
      g_dbus_proxy_new_sync (self->system_bus,
                             G_DBUS_PROXY_FLAGS_GET_INVALIDATED_PROPERTIES,
                             NULL, self->ribchester_bus_name, path,
                             "org.apertis.Ribchester.AppStore.Transaction",
                             NULL, &error);
  g_assert_nonnull (transaction);
  g_assert_no_error (error);

  do
    {
      g_autoptr (GVariant) v = NULL;

      g_main_context_iteration (NULL, TRUE);

      v = g_dbus_proxy_get_cached_property (transaction, "Stage");
      g_assert_cmpstr (g_variant_get_type_string (v), ==, "i");
      stage = g_variant_get_int32 (v);

      g_test_message ("Installation stage: %d", stage);
    }
  while (stage >= 0 &&
         stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED);

  g_assert_cmpint (stage, ==,
                   RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED);

  bundle_id = g_dbus_proxy_get_cached_property (transaction, "BundleId");
  g_assert_cmpstr (g_variant_get_type_string (bundle_id), ==, "s");
  g_assert_cmpstr (g_variant_get_string (bundle_id, NULL), ==, self->id);

  got_version = g_dbus_proxy_get_cached_property (transaction, "BundleVersion");
  g_assert_cmpstr (g_variant_get_type_string (got_version), ==, "s");
  g_assert_cmpstr (g_variant_get_string (got_version, NULL), ==,
                   to_version->number);

  g_dbus_proxy_call_sync (transaction, "Release", NULL, G_DBUS_CALL_FLAGS_NONE,
                          -1, NULL, &error);
  g_assert_no_error (error);

  g_test_message ("... done");
}

/*
 * Like g_clear_object(), but for GDBus name-watching IDs.
 */
void
tests_name_watch_clear (TestsNameWatch *id_p)
{
  TestsNameWatch id = *id_p;

  *id_p = 0;

  if (id != 0)
    g_bus_unwatch_name (id);
}

/*
 * Like g_clear_object(), but for GDBus name-ownership IDs.
 */
void
tests_name_ownership_clear (TestsNameOwnership *id_p)
{
  TestsNameOwnership id = *id_p;

  *id_p = 0;

  if (id != 0)
    g_bus_unown_name (id);
}

void
tests_signal_subscription_init (TestsSignalSubscription *sub)
{
  sub->connection = NULL;
  sub->id = 0;
}

void
tests_signal_subscription_set (TestsSignalSubscription *sub,
                               GDBusConnection *connection,
                               guint id)
{
  sub->connection = g_object_ref (connection);
  sub->id = id;
}

/*
 * Like g_clear_object(), but for GDBus signal subscriptions.
 */
void
tests_signal_subscription_clear (TestsSignalSubscription *sub)
{
  TestsSignalSubscription tmp = *sub;

  /* ownership was transferred */
  sub->connection = NULL;
  sub->id = 0;

  if (tmp.id != 0)
    g_dbus_connection_signal_unsubscribe (tmp.connection, tmp.id);
}

gboolean
tests_require_real_session_bus (void)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *address = NULL;
  g_autofree gchar *expected_address = NULL;

  address = g_dbus_address_get_for_bus_sync (G_BUS_TYPE_SESSION,
                                             NULL, &error);
  g_assert_no_error (error);

  expected_address = g_strdup_printf ("unix:path=%s/bus",
                                      g_get_user_runtime_dir ());

  if (g_strcmp0 (address, expected_address) != 0)
    {
      g_autofree gchar *message =
          g_strdup_printf ("This test requires the real session bus and "
                           "systemd --user (expected address \"%s\", got "
                           "\"%s\")",
                           expected_address,
                           address);

      g_test_skip (message);
      return FALSE;
    }

  return TRUE;
}

static gboolean scoped_timeout_cb (gpointer p) G_GNUC_NORETURN;
static gboolean
scoped_timeout_cb (gpointer p)
{
  guint seconds = GPOINTER_TO_UINT (p);

  g_error ("Timed out after %u seconds", seconds);
}

void
tests_scoped_timeout_start (TestsScopedTimeout *id_p,
                            guint seconds)
{
  g_assert (id_p != NULL);
  *id_p = g_timeout_add (seconds * 1000, scoped_timeout_cb,
                         GUINT_TO_POINTER (seconds));
}

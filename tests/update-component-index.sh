#!/bin/sh

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -x

tmpdir="$(mktemp -d)"

here="$(cd "$(dirname "$0")" && pwd)"
: "${G_TEST_SRCDIR:="$here"}"
: "${G_TEST_BUILDDIR:="$here"}"

test_num=0

ok () {
    test_num="$(expr "$test_num" + 1)"
    echo "ok $test_num - $*"
}

skip () {
    test_num="$(expr "$test_num" + 1)"
    echo "ok $test_num # SKIP - $*"
}

not_ok () {
    test_num="$(expr "$test_num" + 1)"
    echo "not ok $test_num - $*"
}

# Copy the executable to the temporary directory so it isn't affected
# by the AppArmor profile, which would break the --root argument
mkdir -p "$tmpdir/var/lib/apertis_extensions/applications"
cp -R "$(command -v canterbury-update-component-index)" "$tmpdir"
# copy the actual binaries and not just the libtool wrapper when
# running `make check`
LIBS="$(dirname "$(command -v canterbury-update-component-index)")/.libs"
if [ -d "$LIBS" ]; then
	cp -R "$LIBS" "$tmpdir/"
fi
cp -R "${G_TEST_SRCDIR}/tests/Applications" "$tmpdir/"
cp -R "${G_TEST_SRCDIR}/tests/usr" "$tmpdir/"

export G_MESSAGES_DEBUG=Canterbury

echo "1..9"

"$tmpdir/canterbury-update-component-index" --root="$tmpdir" >&2
test -e "$tmpdir/var/cache/app-info/xmls/apertis-installed-store.xml.gz"
ok "updating store app-bundle cache successful"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-installed-store.xml.gz" | \
    grep -q "<id>com.example.MusicPlayer</id>"
ok "store app bundle was indexed"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-installed-store.xml.gz" | \
    grep -q "<id>com.example.Masked</id>"
ok "maskable app bundle was indexed"

"$tmpdir/canterbury-update-component-index" --root="$tmpdir" --platform >&2
test -e "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz"
ok "updating built-in bundle and platform cache successful"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz" | \
    grep -q "<id>net.example.wayneindustries.App</id>"
ok "built-in app bundle with only a .desktop file was indexed"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz" | \
    grep -q "<id>net.example.wayneindustries.Empty</id>"
ok "empty built-in app bundle was indexed"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz" | \
    grep -q "<id>net.example.wayneindustries.Theme</id>"
ok "theme with metadata was indexed"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz" | \
    grep -q "<id>com.example.Masked</id>"
ok "masking app bundle was indexed"

zcat "$tmpdir/var/cache/app-info/xmls/apertis-platform.xml.gz" | \
    grep -q "<id>example.desktop</id>"
ok "platform .desktop file was indexed"

# We need to chmod it in case it's a copy of the read-only data
# during distcheck
chmod -R u+w "${tmpdir}"
rm -fr "${tmpdir}"
